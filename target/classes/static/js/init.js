//判断当前浏览类型  

function myBrowser() {
	var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
	var isOpera = userAgent.indexOf("Opera") > -1; //判断是否Opera浏览器
	var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera; //判断是否IE浏览器
	var isFF = userAgent.indexOf("Firefox") > -1; //判断是否Firefox浏览器
	var isSafari = userAgent.indexOf("Safari") > -1; //判断是否Safari浏览器
	if (isIE) {
		var IE5 = IE55 = IE6 = IE7 = IE8 = false;
		var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
		reIE.test(userAgent);
		var fIEVersion = parseFloat(RegExp["$1"]);
		IE55 = fIEVersion == 5.5;
		IE6 = fIEVersion == 6.0;
		IE7 = fIEVersion == 7.0;
		IE8 = fIEVersion == 8.0;
		if (IE55) {
			return "IE55";
		}
		if (IE6) {
			return "IE6";
		}
		if (IE7) {
			return "IE7";
		}
		if (IE8) {
			return "IE8";
		}
	} //isIE end
	if (isFF) {
		return "FF";
	}
	if (isOpera) {
		return "Opera";
	}
} //myBrowser() end
//以下是调用上面的函数

function gotoDown() {
	setTimeout(function () {
		if (confirm('您当前使用的浏览器版本过低，请下载新版本浏览器，点击确定，下载最新的Firfox浏览器！')) {
			window.location.href = "/download/Firefox.exe";
		}
	}, 1000);
}
if (myBrowser() == "IE5" || myBrowser() == "IE6" || myBrowser() == "IE7" || myBrowser() == "IE8" || myBrowser() == "IE9") {
	gotoDown();
}
function getQueryString(name) {
	var url = location.href;
	var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
	var paraObj = {}
	for (i = 0; j = paraString[i]; i++) {
		paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
	}
	var returnValue = paraObj[name.toLowerCase()];
	if (typeof (returnValue) == "undefined") {
		return "";
	} else {
		return returnValue;
	}
}

window.onload = function () {
	var username = getQueryString('username');
	var password = getQueryString('password');
	if (username != '' && password != '') {
		document.getElementById("username").value = username;
		document.getElementById("password").value = password;
		document.getElementById("login_form").submit()
		// $('#username').val(username);
		// $('#password').val(password);
		// $('#login_form').submit();
	}
}