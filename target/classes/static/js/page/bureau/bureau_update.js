$(function() {
	
	$.ajax({
		type : "POST",
		url :"../../bureau/queryBureauById",
		data : {'id' :GetQueryString("row")},
		contentType : "application/x-www-form-urlencoded",
		success : function(data){
			$("#bureau_id").val(data.bureau.id);
			$("#bureau_name").val(data.bureau.bureau_name);
			$("#bureau_address").val(data.bureau.bureau_address);
			$("#contacts").val(data.bureau.contacts);
			$("#contacts_tel").val(data.bureau.contacts_tel);
			$("#create_time").val(data.bureau.create_time);
			$("#modifier").val(data.bureau.modifier);
			$("#modify_time").val(data.bureau.modify_time);
			$("#district_id").val(data.bureau.district_id);
			$("#district_name").val(data.bureau.district_name);
			$("#type").val(data.bureau.type);
		}
	});
	
	
	$("#saveOrUpdate").click(function() {
		var type =$("#type").val();
		if(check($("#type").val())){alert("请输入部门类型！");return;};
		var bureau_name=$("#bureau_name").val();
		var bureau_address=$("#bureau_address").val();
		var contacts=$("#contacts").val();
		var contacts_tel=$("#contacts_tel").val();
		$.ajax({
			data : {'bureau_id':GetQueryString("row")},
			url :'../../bureau/update?type='+type+"&bureau_name="+bureau_name+"&bureau_address="+bureau_address+"&contacts="+contacts+"&contacts_tel="+contacts_tel,
			success : function(data) {
				alert('操作成功');
				location.href="../../page/bureau/bureau_list.html";
				}	
			}		
		);
	});
});

function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}

function check(obj){
	var msg = false;
	if(obj==""){
		msg = true;
	}
	return msg;
}