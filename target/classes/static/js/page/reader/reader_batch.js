$(function() {
	$("#saveOrUpdate").click(function() {
		var startReaderNumber =$("#startReaderNumber").val();
		var length=$("#length").val();
		if(check($("#startReaderNumber").val())){alert("请输入识读器编码！");return;};
		if(check($("#length").val())){alert("请输入识读器个数！");return;};
		var batch=$("#batch").val();
		
		//根据批次号去识读器库查询是否存在
		//如果不存在提示
		$.ajax({
			type : "post",
			dataType : "json",
			url :'../../reader/query?batch='+batch,
			success : function(data) {
				if(data.noData!=0){
					alert('批次号已被占用');
					return ;
				}
		$.ajax({
			type : "post",
			dataType : "json",
			url :'../../reader/BatchAdd?startReaderNumber='+startReaderNumber+"&length="+length+"&batch="+batch,
			success : function(data) {
				
					alert('操作成功');
						location.href="../../page/reader/reader_list.html";
				
			}
		});
			}
		});
	});
});

function check(obj){
	var msg = false;
	if(obj==""){
		msg = true;
	}
	return msg;
}
