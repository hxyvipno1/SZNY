$(function() {
	$.ajax({
		type : "POST",
		url :"../../reader/queryReaderById",
		data : {'id' :GetQueryString("id")},
		contentType : "application/x-www-form-urlencoded",
		success : function(data){
			$("#id").val(data.reader.id);
			$("#readerCode").val(data.reader.readerCode);
			$("#customerId").val(data.reader.customerId);
			$("#sim").val(data.reader.sim);
			$("#batch").val(data.reader.batch);
			$("#status").val(data.reader.status);
		}
	});
		
	
	$("#saveOrUpdate").click(function() {
		var sim=$("#sim").val();
		//alert('id'+GetQueryString("id"));
		$.ajax({
			type : "post",
			dataType : "json",
			data : {'id':GetQueryString("id")},
			
			url :'../../reader/update?sim='+sim,
			success : function(data) {
					alert('操作成功');
						location.href="../../page/reader/reader_list.html";
				
			}
		});
		
	});
});


function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}
