function __RUN_ACT(url,data,_callback){
	$.ajax({
		url: url,type: "POST",
		contentType: "application/json; charset=utf-8", 
		 data:JSON.stringify(data) ,
		 dataType: "json",
		 success: function(r){
			 var d=r.response.detail;
			 _callback.call(null,d);
		 }
	});
}

function __RUN_STEP(step,_callback){
	__RUN_ACT("/reader/query",{step:step},_callback);
}