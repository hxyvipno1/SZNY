$(function() {
	s();
	//查询
	$("#query").click(function() {
		//获取查询条件
		var readerCode = $("#readerCode").val();

		var batch = $("#batch").val();
		//var companyNameval = encodeURIComponent($("#comanyName").val());
		var status = $("#status").val();

		var postJson = {
			readerCode : readerCode,
			batch : batch,
			status : status
		};
		//传入查询条件参数  
		$("#list").jqGrid("setGridParam", {
			postData : postJson
		});
		//每次提出新的查询都转到第一页  
		$("#list").jqGrid("setGridParam", {
			page : 1
		});
		//提交post并刷新表格  
		$("#list").jqGrid("setGridParam", {
			url : $("#searchForm").attr("action")
		}).trigger("reloadGrid");
	});
});

function s() {
	$("#list").jqGrid({
		url : '../../reader/list',// 请求数据的地址
		datatype : "json",

		colNames : [ 
		             '识读器编码', /*'农户id',*/'SIM', '批次',
		'识读器状态', '日期' ],
		// jqgrid主要通过下面的索引信息与后台传过来的值对应

		colModel : [
		 {
			name : 'readerCode',index : 'invdate',width : 290
		},/*{	
			name : 'customerId',index : 'name',width : 500
		},*/{
			name : 'sim',index : 'sim',width : 500
		}, {
			name : 'batch',index : 'batch',width : 500
		}, {
			name : 'status',index : 'status',width : 500,
			formatter: function(value, options, row){
				if(value ==='0'){
					return '<option value="0">初始</option>'  
				}else if(value ==='1'){
					return'<option value="1">已领取</option>'
				}else if(value ==='2'){
					return'<option value="2">使用中</option>'
				}else if(value ==='3'){
					return'<option value="3">损坏</option>'
				}
				}
		}, {
			name : 'datestr',index : 'datestr',width : 500
		} ],

		height : 385,
		rownumbers : true,
		rownumWidth : 25,
		autowidth : true,
		multiselect : true,

		rowNum : 10,
		rowList : [ 10, 20, 30 ],

		sortname : 'id',
		recordpos : 'right',
		viewrecords : true,
		pager : '#pager',
		jsonReader : {
			root : "page.list",
			page : "page.currPage",
			total : "page.totalPage",
			records : "page.totalCount"
		},
		prmNames : {
			page : "page.currPage",
			rows : "limit",
			order : "order"
		},
		gridComplete : function() {
			// 隐藏grid底部滚动条
			$("#jqGrid").closest(".ui-jqgrid-bdiv").css({
				"overflow-x" : "hidden"
			});
		}
	});
	//修改
	$("#update").click(function() {
		var purchaseIds = getSelectedRows();
		if (purchaseIds == null) {
			return;
		}
		var ids = "";
		for (var i = 0; i < purchaseIds.length; i++) {
			ids += eval(purchaseIds[i]) + ",";
		}
		location.href = "../../page/reader/reader_detail.html?id="+ids;
}
	)}



//选择多条记录
function getSelectedRows() {
    var grid = $("#list");
    var rowKey = grid.getGridParam("selrow");
    if(!rowKey){
    	alert("请至少选择一条记录");
    	return ;
    }
    return grid.getGridParam("selarrrow");
}