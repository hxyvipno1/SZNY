$(function() {
	// 查询
	$("#query").click(function() {
		var page = $("#jqGrid").jqGrid('getGridParam', 'page');
		$("#jqGrid").jqGrid('setGridParam', {
			page : page,
			mtype: 'get',
			postData : toObject("myform")
		}).trigger("reloadGrid");
	});
	
	
	// 修改
	$("#update").click(function() {
		var ids = $("#jqgrid").jqGrid('getGridParam','selarrrow');
		if(ids.length<1){
			alert("请选择一条记录！");
			return ;
		}
		if (ids.length>1) {
			alert("你只能选择一条记录进行修改！");
			return;
		}
		location.href = "../../page/earTag/earTag_detail.html?operate=update&id="+ids;
	});

	// 删除
	$("#del").click(function() {		
		var ids = $("#jqgrid").jqGrid('getGridParam','selarrrow');
		
		if (confirm("确认删除吗")) {
			$.ajax({
				type : "POST",
				url : "../../earTag/delete?earTagIds="+ids,
				contentType : "application/x-www-form-urlencoded",
				dataType : "json",
				cache : false,
				success : function(r) {
					if(r.code==1){
						alert(r.message);	
						var len = ids.length;
						for(var i=0;i<len;i++)
						{
						    $("#jqgrid").jqGrid('delRowData',ids[0]);
						}
						return ;
					}
					alert(r.message);
						
				}
		});
	}

		
   });
});



