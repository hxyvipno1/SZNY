//获得form里的数据
function __GET_FROM_VALUES(id){
	var result={};
	var form=$("#"+id);
	form.find('input').each(function(index,item){
		var v=$(item).val();
		result[$(item).attr('name')]=v;
	});

	form.find('textarea').each(function(index,item){
		var v=$(item).val();
		result[$(item).attr('name')]=v;
	});

	form.find('select').each(function(index,item){
		var v=$(item).val();
		result[$(item).attr('name')]=v;
	});

	return result;
}

//提交数据
function __RUN_ACT(url,data,_callback){
	$.ajax({
		url: url,type: "POST",
		contentType: "application/json; charset=utf-8", 
		 data:JSON.stringify(data) ,
		 dataType: "json",
		 success: function(r){
			 var d=r;
			 _callback.call(null,d);
		 }
	});
}


//初始化
function DETAIL__RUN_STEP(step,_callback){
	__RUN_ACT("/step",{step:step},_callback);
}
//查询
function SELECT__RUN_STEP(step,_callback){
	__RUN_ACT("/earTag/list",{step:step},_callback);
}

//增加
function ADD__RUN_STEP(step,_callback){
	__RUN_ACT("/step",{step:step},_callback);
}

//修改
function UPDATE__RUN_STEP(step,_callback){
	__RUN_ACT("/earTag/update",{step:step},_callback);
}

//删除
function DEL__RUN_STEP(step,_callback){
	__RUN_ACT("/earTag/delete",{step:step},_callback);
}

function statusChange() {
	// 客户名称的选择
	var options = $("#statusSelect option:selected");
	var status = options.text();
	$("#status").val(status);
}


function customerNameChange() {
	//客户名称的选择
	var options = $("#customerSeclect option:selected");
    var customerName = options.text();
    $("#customerName").val(customerName);
}

