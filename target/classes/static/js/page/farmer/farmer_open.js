$(function() {
	
	//初始化省份
	if(GetQueryString("row")==null){
		$.ajax({
			type : "POST",
			url :"../../farmer/detail",
			contentType : "application/x-www-form-urlencoded",
			success : function(data) {
			for(var i=0;i<data.provinceList.length;i++){
				$("#province").append("<option>"+data.provinceList[i].name+"</option>");
				}
			}});
	};
	
	$.ajax({
		type : "POST",
		url :"../../farmer/detail",
		data : {'id' :GetQueryString("id")},
		contentType : "application/x-www-form-urlencoded",
		success : function(data) {
			$("#id").val(data.farmer.id);
			$("#name").val(data.farmer.name);
			$("#idNo").val(data.farmer.idNo);
			$("#birthday").val(data.farmer.birthday);
			$("#sex").val(data.farmer.sex);
			$("#address").val(data.farmer.address);
			$("#linkMobile").val(data.farmer.linkMobile);
			$("#email").val(data.farmer.email);
			$("#companyName").val(data.farmer.companyName);
			$("#coCreditCode").val(data.farmer.coCreditCode);
			$("#coType").val(data.farmer.coType);
			$("#busiLicRegNum").val(data.farmer.busiLicRegNum);
			$("#taxRegIdNum").val(data.farmer.taxRegIdNum);
			$("#organCode").val(data.farmer.organCode);
			$("#coAddress").val(data.farmer.coAddress);
			$("#coRegisteredCapital").val(data.farmer.coRegisteredCapital);
			$("#coCreateDate").val(data.farmer.coCreateDate);
			$("#coBusinessTerm").val(data.farmer.coBusinessTerm);
			$("#coBusinessScope").val(data.farmer.coBusinessScope);
			$("#coRegistrationAuthority").val(data.farmer.coRegistrationAuthority);
			$("#coRegistrationDate").val(data.farmer.coRegistrationDate);
			$("#coZipCode").val(data.farmer.coZipCode);
			$("#coFixedTelephone").val(data.farmer.coFixedTelephone);
			$("#coEmail").val(data.farmer.coEmail);
			$("#coInbox").val(data.farmer.coInbox);
			$("#nationality").val(data.farmer.nationality);
			$("#maritalStatus").val(data.farmer.maritalStatus);
			$("#presentAddress").val(data.farmer.presentAddress);
			for(var i=0;i<data.provinceList.length;i++){
				if(data.provinceList[i].name==data.farmer.province){
					$("#province").append("<option selected = 'selected'>"+data.provinceList[i].name+"</option>");
				}else{
					$("#province").append("<option>"+data.provinceList[i].name+"</option>");
				}
				}
			for(var i=0;i<data.cityList.length;i++){
				if(data.cityList[i].name==data.farmer.city){
					$("#city").append("<option selected = 'selected'>"+data.farmer.city+"</option>");
				}else{
					$("#city").append("<option>"+data.cityList[i].name+"</option>");
				}
				}
			for(var i=0;i<data.areaList.length;i++){
				if(data.areaList[i].name==data.farmer.area){
					$("#area").append("<option selected = 'selected'>"+data.farmer.area+"</option>");
				}else{
					$("#area").append("<option>"+data.areaList[i].name+"</option>");
				}
				}
			for(var i=0;i<data.townList.length;i++){
				if(data.townList[i].name==data.farmer.town){
					$("#town").append("<option selected = 'selected'>"+data.farmer.town+"</option>");
				}else{
					$("#town").append("<option>"+data.townList[i].name+"</option>");
				}
				}
			for(var i=0;i<data.villageList.length;i++){
				if(data.villageList[i].name==data.farmer.village){
					$("#village").append("<option selected = 'selected'>"+data.farmer.village+"</option>");
				}else{
					$("#village").append("<option>"+data.villageList[i].name+"</option>");
				}
			}
			//耳标列表
			for(var i=0;i<data.earList.length;i++){
				$("#earList").append("<tr style='text-align:center'><td>"+data.earList[i]+"</td><td>"+data.earList[i]+"</td></tr>");
			}
			//保险列表
			for(var i=0;i<data.readList.length;i++){
				$("#readList").append("<tr style='text-align:center'><td>"+data.readList[i]+"</td><td>"+""+"</td></tr>");
			}
			//保险列表
			for(var i=0;i<data.insuranceMealList.length;i++){
				$("#insuranceMealList").append("<tr style='text-align:center'>" +
						"<td>"+data.insuranceMealList[i].insuranceMealName+"</td>" +
						"<td>"+data.insuranceMealList[i].num+"</td>" +
						"<td>"+data.insuranceMealList[i].insuranceReceiptNo+"</td></tr>");
			}
			//运营商列表
			for(var i=0;i<data.mobileMealList.length;i++){
				var parentMobileNo = data.mobileMealList[i].parentMobileNo;
				if(parentMobileNo==null){
					parentMobileNo = "";
				}
				var cardType = data.mobileMealList[i].cardType;
				if(cardType=="MAIN"){
					cardType = "主卡";
				}else{
					cardType = "副卡";
				}
				
				$("#mobileMealList").append("<tr style='text-align:center'>" +
						"<td>"+cardType+"</td>" +
						"<td>"+data.mobileMealList[i].mobileNo+"</td>" +
						"<td>"+data.mobileMealList[i].mobileMealName+"</td>" +
						"<td>"+parentMobileNo+"</td></tr>");
			}
			
			var check = data.farmer.checkPass;
			if(check=="Y"){
				$("#pass").attr("checked","checked");
			}else if(check=="N"){
				$("#noPass").attr("checked","checked");
			}
			var strs= new Array(); //定义一数组
			strs=data.farmer.busiId.split(",");
			
			$("input[name='animal']").each(function(){
				var ani = $(this).val();
				for (var i=0;i<strs.length ;i++ )
				{
					if(ani==strs[i]){
						$(this).attr("checked","true"); 
					}
				}
			});
		}
	});
});


function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}