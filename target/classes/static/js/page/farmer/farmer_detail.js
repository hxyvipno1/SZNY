$(function() {
	$("body").undelegate();
	$("body").delegate(".tt", "change",filechange );

	var id = GetQueryString("row");
	$("#idd").val(id);
	$("#id").val(id);

		//明细信息
		$.ajax({
			type : "POST",
			url :"../../farmer/detail",
			data : {'id' :GetQueryString("row")},
			contentType : "application/x-www-form-urlencoded",
			success : function(data) {
				$("#id").val(data.farmer.id);
				$("#name").val(data.farmer.name);
				$("#idNo").val(data.farmer.idNo);
				$("#birthday").val(data.farmer.birthday);
				$("#sex").val(data.farmer.sex);
				$("#address").val(data.farmer.address);
				$("#linkMobile").val(data.farmer.linkMobile);
				$("#email").val(data.farmer.email);
				$("#companyName").val(data.farmer.companyName);
				$("#coCreditCode").val(data.farmer.coCreditCode);
				$("#coType").val(data.farmer.coType);
				$("#busiLicRegNum").val(data.farmer.busiLicRegNum);
				$("#taxRegIdNum").val(data.farmer.taxRegIdNum);
				$("#organCode").val(data.farmer.organCode);
				$("#coAddress").val(data.farmer.coAddress);
				$("#coRegisteredCapital").val(data.farmer.coRegisteredCapital);
				$("#coCreateDate").val(data.farmer.coCreateDate);
				$("#coBusinessTerm").val(data.farmer.coBusinessTerm);
				$("#coBusinessScope").val(data.farmer.coBusinessScope);
				$("#coRegistrationAuthority").val(data.farmer.coRegistrationAuthority);
				$("#coRegistrationDate").val(data.farmer.coRegistrationDate);
				$("#coZipCode").val(data.farmer.coZipCode);
				$("#coFixedTelephone").val(data.farmer.coFixedTelephone);
				$("#coEmail").val(data.farmer.coEmail);
				$("#coInbox").val(data.farmer.coInbox);
				$("#nationality").val(data.farmer.nationality);
				$("#maritalStatus").val(data.farmer.maritalStatus);
				$("#presentAddress").val(data.farmer.presentAddress);
				for(var i=0;i<data.provinceList.length;i++){
					if(data.provinceList[i].name==data.farmer.province){
						$("#province").append("<option selected = 'selected' id="+data.provinceList[i].id+">"+data.provinceList[i].name+"</option>");
					}else{
						$("#province").append("<option id="+data.provinceList[i].id+">"+data.provinceList[i].name+"</option>");
					}
					}
				for(var i=0;i<data.cityList.length;i++){
					if(data.cityList[i].name==data.farmer.city){
						$("#city").append("<option selected = 'selected' id="+data.cityList[i].id+">"+data.farmer.city+"</option>");
					}else{
						$("#city").append("<option id="+data.cityList[i].id+">"+data.cityList[i].name+"</option>");
					}
					}
				for(var i=0;i<data.areaList.length;i++){
					if(data.areaList[i].name==data.farmer.area){
						$("#area").append("<option selected = 'selected' id="+data.areaList[i].id+">"+data.farmer.area+"</option>");
					}else{
						$("#area").append("<option id="+data.areaList[i].id+">"+data.areaList[i].name+"</option>");
					}
					}
				for(var i=0;i<data.townList.length;i++){
					if(data.townList[i].name==data.farmer.town){
						$("#town").append("<option selected = 'selected' id="+data.townList[i].id+">"+data.farmer.town+"</option>");
					}else{
						$("#town").append("<option id="+data.townList[i].id+">"+data.townList[i].name+"</option>");
					}
					}
				for(var i=0;i<data.villageList.length;i++){
					if(data.villageList[i].name==data.farmer.village){
						$("#village").append("<option selected = 'selected' id="+data.villageList[i].id+">"+data.farmer.village+"</option>");
					}else{
						$("#village").append("<option id="+data.villageList[i].id+">"+data.villageList[i].name+"</option>");
					}
					}
				
				
				var check = data.farmer.checkPass;
				if(check=="Y"){
					$("#pass").attr("checked","checked");
				}else if(check=="N"){
					$("#noPass").attr("checked","checked");
				}
				var strs= new Array(); //定义一数组
				strs=data.farmer.busiId.split(",");
				$("#aniList").append("<h2 style='color:#00ced1'>牲畜类型</h2>");
				for(var i=0;i<data.animalList.length;i++){
					$("#aniList").append("<div style='height:50px;width:30%;float: left;text-align: center;'><input type='checkbox' id ='pig' name ='animal' class='ani' value="+data.animalList[i].id+" onchange='choiseType()'/>&nbsp;&nbsp;"+data.animalList[i].busiName+"</div>");
					}
				$("input[name='animal']").each(function(){
					var ani = $(this).val();
					for (var i=0;i<strs.length ;i++ )
					{
						if(ani==strs[i]){
							$(this).attr("checked","true"); 
						}
					}
				});
			}
		});
	
		
		$("#subupload").change(function (){
			
			$("#pigForm").submit();
		});
	
	//保存
	$("#saveOrUpdate").click(function() {
		if(check($("#name").val())){alert("请输入姓名！");return;};
		if(check($("#idNo").val())){alert("请输入身份证号！");return;};
		if(check($("#birthday").val())){alert("请输入年龄！");return;};
		if(check($("#sex").val())){alert("请输入性别！");return;};
		if(check($("#email").val())){alert("请输入邮箱！");return;};
		if(check($("#adress").val())){alert("请输入地址！");return;};
		if(check($("#companyName").val())){alert("请输入公司名称！");return;};
		if(check($("#linkMobile").val())){alert("请输入联系电话！");return;};
		var checkPass = $('input:radio[name="check"]:checked').val();
		$("#checkPass").val(checkPass);
		if($("#town").val()=="请选择街道"){
			$("#town").val("");
		}
		if($("#village").val()=="请选择村"){
			$("#village").val("");
		}
		if($("#area").val()=="请选择区"){
			$("#area").val("");
		}
		if($("#city").val()=="请选择市"){
			$("#city").val("");
		}
		if($("#province").val()=="请选择省份"){
			$("#province").val("");
		}
		var $form = $("#form");
	    var spCodesTemp = "";
	      $('input:checkbox[name=animal]:checked').each(function(i){
	       if(0==i){
	        spCodesTemp = $(this).val();
	       }else{
	        spCodesTemp += (","+$(this).val());
	       }
	      });
	      
	      $("#animals").val(spCodesTemp);
	     
			
		$.ajax({
			type : "post",
			dataType : "json",
			url : "../../farmer/add",
			data :$form.serialize(),
			contentType : "application/x-www-form-urlencoded",
			success : function(data) {
				 
							alert(data.message);
							location.href="../../page/farmer/farmer.html";
					
			}
		});
		
		
	});	
	
	
	$("#dropz").dropzone({
		url: "../../farmer/file?id="+$("#idd").val(), //必须填写
        method:"post",  
        paramName:"file", //默认为file
        addRemoveLinks : true,
        uploadMultiple: true,
        maxFiles:10,//一次性上传的文件数量上限
        maxFilesize: 20, //MB
        acceptedFiles: ".jpg,.gif,.png", //上传的类型
        init:function(){
            this.on("removedfile",function(file){
                alert("cheng");
                 });
        }

});
	
	$("#pig").live("onchange", function(){
		$("#file").append("<form style='width:190px;height:200px;float:left;margin-left:5px;'  action='../../farmer/file' class='dropzone' id='my-awesome-dropzone' enctype='multipart/form-data' ><div id='dropz' class="+data.crads[i].id+" ></div></form>");
	});
});


function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}

function check(obj){
	var msg = false;
	if(obj==""){
		msg = true;
	}
	return msg;
}

//省市区切换
function choiceNext(obj){
	var oid = 0;
	var id = $(obj).attr("id");
	if(id=='province'){
		oid = $("#province option:selected").attr("id");
	}else if(id=='city'){
		oid = $("#city option:selected").attr("id");
	}else if(id=='area'){
		oid = $("#area option:selected").attr("id");
	}else if(id=='town'){
		oid = $("#town option:selected").attr("id");
	}else{
		oid = $("#village option:selected").attr("id");
	}
	$.ajax({
		type : "POST",
		url :"../../farmer/queryDistrict",
		data:{"id":oid},
		contentType : "application/x-www-form-urlencoded",
		success : function(data) {
		if(id=='province'){
			$("#city").empty();
			$("#city").append("<option>请选择市</option>");
			$("#area").empty();
			$("#area").append("<option>请选择区</option>");
			$("#town").empty();
			$("#town").append("<option>请选择街道</option>");
			$("#village").empty();
			$("#village").append("<option>请选择村</option>");
			for(var i=0;i<data.districtList.length;i++){
				$("#city").append("<option id="+data.districtList[i].id+">"+data.districtList[i].name+"</option>");
			}
		}else if(id=='city'){
			$("#area").empty();
			$("#area").append("<option>请选择区</option>");
			$("#town").empty();
			$("#town").append("<option>请选择街道</option>");
			$("#village").empty();
			$("#village").append("<option>请选择村</option>");
			for(var i=0;i<data.districtList.length;i++){
				$("#area").append("<option id="+data.districtList[i].id+">"+data.districtList[i].name+"</option>");
			}
		}else if(id=='area'){
			$("#town").empty();
			$("#town").append("<option>请选择街道</option>");
			$("#village").empty();
			$("#village").append("<option>请选择村</option>");
			for(var i=0;i<data.districtList.length;i++){
				$("#town").append("<option id="+data.districtList[i].id+">"+data.districtList[i].name+"</option>");
			}
		}else if(id=='town'){
			$("#village").empty();
			$("#village").append("<option>请选择村</option>");
			for(var i=0;i<data.districtList.length;i++){
				$("#village").append("<option id="+data.districtList[i].id+">"+data.districtList[i].name+"</option>");
			}
		}
		
		
		}});
	
}
function choiseType(){
	var busiType = "";
	$('input:checkbox[name=animal]:checked').each(function(i){
	       if(0==i){
	    	   busiType = $(this).val();
	       }else{
	    	   busiType += (","+$(this).val());
	       }
	      });
	$.ajax({
		type : "POST",
		url :"../../farmer/queryCredentials?busiType="+busiType,
		contentType : "application/x-www-form-urlencoded",
		success : function(data) {
			$("#files").children().remove();
			for (var i=0;i<data.crads.length ;i++ ){
				$("#files").append("<form method='post' style='width:190px;height:200px;float:left;margin-left:5px;'  " +
						"action='../../farmer/file' class='tt' id='my-awesome-dropzone"+i+"' " +
								"enctype='multipart/form-data' ><input type='file' id='file' name='file' value='upload' " +
								"onchange='filechange('q')';/>" +
								"<input type='text' id='credentialsName' name='credentialsName' value='"+data.crads[i].credentialsName+"' />" +
								"<input type='text' id='id' name='id' value='"+data.crads[i].id+"' />" +
								"" +
								"</form>");
				//$("#files").append("<input type='file' name='file' value='upload'/>");
				//$("#files").append("<input type='button' class='ddd' value='ss' onclick=ss();/>");
			}
			if(busiType==""){
				$("#files").children().remove();
			}
			
		}});
}
function filechange(bb){
	alert('sdfdfdf'+JSON.stringify(bb));
	console.log("file upload");
	$("#"+myform).submit();
}
