$(function() {
	
	$("#jqGrid").jqGrid({
        url:"../../login/list",
        datatype:"json", //数据来源，本地数据
        height:420,//高度，表格高度。可为数值、百分比或'auto'
        height:'auto',//高度，表格高度。可为数值、百分比或'auto'
        width:1000,//这个宽度不能为百分比
        autowidth:true,//自动宽
        colNames:['用户编码','用户名称','邮箱', '手机号', '状态 '/*0：禁用   1：正常*/,'创建时间'],
        colModel:[			
	      			{  name: 'id',index:'id', width: 45, key: true,hidden:true},
	    			{  name: 'userName',index:'userName', width: 75 },
	    			{  name: 'email',index:'email', width: 75 },
	    			{  name: 'mobile',index:'mobile', width: 75},
	    			{  name: 'status',index:'status', width: 75,
	    				formatter: function(value, options, row){
	    					return value === '1' ? 
	    							'<span >正常</span>': 
	    								'<span >禁用</span>' ;
	    				}
	    			},
	    			{  name: 'createTime',index:'createTime', width: 75}
                 ],
        viewrecords: true,
        
        rowNum: 10,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        jsonReader : {
        	root: "list",
            page: "pageNum",
            total: "pages",
            records: "navigatePages"
        },
        prmNames : {
        	page : "pageNum",
			rows : "limit",
			order : "order"
		},
		pager: "#gridPager",
		gridComplete : function() {
			// 隐藏grid底部滚动条
			$("#jqGrid").closest(".ui-jqgrid-bdiv").css({
				"overflow-x" : "hidden"
			});}
    });
	
	$("#add").click(function() {
		location.href = "../../page/user/user_detail.html";
	});
	
	$("#update").click(function() {
		var row = getSelectedRow();
		if (row == null) {
			return;
		}
		location.href ="../../page/user/user_update.html?row="+row;
	});
	$("#del").click(function() {
		var farmerIds = getSelectedRows();
		if (farmerIds == null) {
			return;
		}
		var ids = "";
		for (var i = 0; i < farmerIds.length; i++) {
			ids += eval(farmerIds[i]) + ",";
		}
		confirm('确定要删除选中的记录？', function() {
			$.ajax({
				type : "POST",
				url :"../../login/delete",
				data : {
					'Ids' : ids.substring(0, ids.length - 1)
				},
				contentType : "application/x-www-form-urlencoded",
				success : function(data) {
					alert(data.message);
					$("#jqGrid").trigger("reloadGrid");
				}
			});
		});
	});
})
	//选择一条记录
function getSelectedRow() {
    var grid = $("#jqGrid");
    var rowKey = grid.getGridParam("selrow");
    if(!rowKey){
    	alert("请选择一条记录");
    	return ;
    }
    var selectedIDs = grid.getGridParam("selarrrow");
    if(selectedIDs.length > 1){
    	alert("只能选择一条记录");
    	return ;
    }
    return selectedIDs[0];
}

//选择多条记录
function getSelectedRows() {
    var grid = $("#jqGrid");
    var rowKey = grid.getGridParam("selrow");
    if(!rowKey){
    	alert("请选择一条记录");
    	return ;
    }
    return grid.getGridParam("selarrrow");
}
//重置按钮事件  
$(document).ready(function(){  
    $("#reset").off().on("click",function(){  
         $('#myform')[0].reset();
    });  
});  
