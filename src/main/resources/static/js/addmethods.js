//身份证
jQuery.validator.addMethod("idCard", function (value, element) {
    var isIDCard1=/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/;//(15位)
    var isIDCard2=/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;//(18位)

    return this.optional(element) || (isIDCard1.test(value)) || (isIDCard2.test(value));
}, "身份证格式不对");

//社会统一信用代码验证
jQuery.validator.addMethod("isSocialcredit", function (value, element) {
    var isSocialcredit=/[A-Z0-9]{18}/g;
    return this.optional(element) || (isSocialcredit.test(value));
}, "社会统一信用代码格式不对,例如：'Z11111111111111111'");

//汉字
jQuery.validator.addMethod("chinese", function (value, element) {
    var chinese = /^[\u4E00-\u9FFF]+$/;
    return this.optional(element) || (chinese.test(value));
}, "请填写汉字");

//营业执照验证
jQuery.validator.addMethod("isRegistration", function (value, element) {
    var isRegistration = /[0-9]{15}/g;
    return this.optional(element) || (isRegistration.test(value));
}, "营业执照格式不对");

//组织机构代码验证
jQuery.validator.addMethod("isOrganization_code", function (value, element) {
    var isOrganization_code = /[A-Za-z0-9]{8}-[A-Za-z0-9]/g;
    return this.optional(element) || (isOrganization_code.test(value));
}, "组织机构代码格式不对,例如：'a1234567-1'");

jQuery.validator.addMethod("isMobile", function(value, element) {
var length = value.length;
var mobile = /^1(3|4|5|7|8)\d{9}$/;
return this.optional(element) || (length == 11 && mobile.test(value));
}, "请正确填写您的手机号码");


jQuery.validator.addMethod("isNum", function(value, element) {
var num= /^(-?\d+)(\.\d+)?$/ ;
return this.optional(element) || ( isNum.test(value));
}, "请正确填写您的手机号码");