$(function() {

	$.ajax({
			type : "POST",
			url :"../../login/queryUserById",
			data : {'id' :GetQueryString("row")},
			contentType : "application/x-www-form-urlencoded",
			success : function(data){
				$("#id").val(data.user.id);
				$("#userName").val(data.user.userName);
				$("#email").val(data.user.email);
				$("#mobile").val(data.user.mobile);
			}
		});


	$("#saveOrUpdate").click(function() {
		var userName=$("#userName").val();
		var password=$("#password").val();
		var email=$("#email").val();
		var mobile=$("#mobile").val();
		var newPassword=$("#newPassword").val();
		//根据用户名和密码去查询是否存在
		//如果不存在提示则跳出
		$.ajax({
			type : "post",
			dataType : "json",
			url :'../../login/query?userName='+userName+'&password='+password,
			success : function(data) {
				if(data.noData==0){
					alert('你输入的原密码有误！请和核实后输入');
					return ;
				}
			//否则入库
				$.ajax({
					type : "post",
					dataType : "json",
					data : {'id':GetQueryString("row")},
					url :'../../login/update?&newPassword='+newPassword+'&userName='+userName+"&password="+password+"&email="+email+"&mobile="+mobile,
					success : function(data) {
							alert('修改成功');
							location.href="../../page/user/user_list.html";
					}
				});
			}
		});
	});	
})
function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}

function check(obj){
	var msg = false;
	if(obj==""){
		msg = true;
	}
	return msg;
}