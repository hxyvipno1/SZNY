
function getBodySize(){
	return {
		w:$(window).width(),
		h:$(document).height()
	}
}
var simGrid=null;
var lastsel;
var customerId;
$(function() {
	var size=getBodySize();
	$("#Customer_Grid").jqGrid({
        url:"../../farmer/list",
        datatype:"json", //数据来源，本地数据
        height:(size.h-80),//高度，表格高度。可为数值、百分比或'auto'
        width:300,//这个宽度不能为百分比
        colNames:['id','农户名称','养殖场'],
        colModel:[			
      			{  name: 'id',index:'id', width: 45, key: true,hidden:true},
    			{  name: 'name',index:'name', width: 75 },
    			{  name: 'companyName', index:'companyName',width: 75 }
        ],
        viewrecords: true,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        jsonReader : {
            root: "list",
            total: "total"
        },
        
        onSelectRow: function(id){
        	customerId=id;
        	$("#Sim_Grid").jqGrid('clearGridData');
        	var url="../../sim/list/"+customerId;
        	$.ajax({ 
        		url:url, 
        		success: function(data,success){
        			var list=data;
        			for(var i=0;i<list.length;i++){
        				$("#Sim_Grid").jqGrid('addRowData',i+1,list[i]);
        			}
        		}
        	});
        }
		
        
    });
	
	
	simGrid=$("#Sim_Grid").jqGrid({
		datatype: "local",
        height:'auto',//高度，表格高度。可为数值、百分比或'auto'
        autowidth:true,//自动宽
        colNames:['id','customerId','SIM NO','备注'],
        colModel:[			
      			{  name: 'id',index:'id', width: 45, key: true,hidden:true, editable:true},
      			{  name: 'customerId',index:'customerId', width: 45,hidden:true, editable:true},
    			{  name: 'simNo',index:'simNo', width: 75 , editable:true},
    			{  name: 'remark', index:'remark',width: 75 , editable:true}
        ],
        viewrecords: true,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        editurl: "../../sim/update",
        onSelectRow: function(id){
        	if(id){
    			//jQuery('#Sim_Grid').jqGrid('restoreRow',lastsel);
    			jQuery('#Sim_Grid').jqGrid('editRow',id,true);
    			lastsel=id;
    		}
        }
    });
	
	$('#new_btn').click(function(){
		if(!customerId) return;
    	$.ajax({ 
    		url:"../../sim/add/"+customerId, 
    		success: function(data,success){
    			$("#Sim_Grid").jqGrid('addRowData',1,data);
    		}
    	});
		
	});
});

function addPageInit() {
	

}

function customerNameChange() {

}