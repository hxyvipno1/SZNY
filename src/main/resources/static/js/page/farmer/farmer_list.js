$(function() {
	// 新增
	$("#add").click(function() {
		window.location.href = "../../#page/farmer/farmer_detail.html";
	});
	 
	// 修改
	$("#update").click(function() {
		var row = getSelectedRow();
		if (row == null) {
			return;
		}
		location.href = "../../#page/farmer/farmer_detail.html?row="+row;
	});
	//领取识读器
	$("#receive").click(function() {
		var row = getSelectedRow();
		if (row == null) {
			return;
		}
		location.href = "zny/page/receive/receive_reader.html?row="+row;
	});
	
	//SIM卡记录
	$("#simCard").click(function() {
		var row = getSelectedRow();
		if (row == null) {
			return;
		}
		location.href = "zny/page/sim/customer_sim_record.html?row="+row;
	});
	
	//	查询
	$("#query").click(function() {
		var $form = $("#myform");
		$("#jqGrid").jqGrid('setGridParam', {
			page : 1,
			url:"../../farmer/query", 
			mtype: 'POST',
	        postData:$form.serialize(),
		}).trigger("reloadGrid");
	});

	
	
	// 删除
	$("#del").click(function() {
		var farmerIds = getSelectedRows();
		if (farmerIds == null) {
			return;
		}
		alert(farmerIds);
		var ids = "";
		for (var i = 0; i < farmerIds.length; i++) {
			ids += eval(farmerIds[i]) + ",";
		}
		confirm('确定要删除选中的记录？', function() {
			$.ajax({
				type : "POST",
				url :"../../farmer/delete",
				data : {
					'farmerIds' : ids.substring(0, ids.length - 1)
				},
				contentType : "application/x-www-form-urlencoded",
				success : function(data) {
					alert(data.message);
					$("#jqGrid").trigger("reloadGrid");
				}
			});
		});
	});
	
	//耳标的领取
	$("#fetch").click(function() {
		var gr = $("#jqGrid").getGridParam('selrow');
		var customerName = $("#jqGrid").getCell(gr,"name");
		var row = getSelectedRow();
		if (row == null) {
			return;
		}
		
		
		//alert(encodeURIComponent(customerName));
		location.href = "zny/page/earTag/farmerAddEarTag_detail.html?customerName="+encodeURI(encodeURI(customerName))+"&id=" + eval(row);
	});
});
//jqGrid的配置信息
$.jgrid.defaults.width = 1000;
$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

//重写alert
window.alert = function(msg, callback){
	parent.layer.alert(msg, function(index){
		parent.layer.close(index);
		if(typeof(callback) === "function"){
			callback("ok");
		}
	});
};

//重写confirm式样框
window.confirm = function(msg, callback){
	parent.layer.confirm(msg, {btn: ['确定','取消']},
	function(){//确定事件
		if(typeof(callback) === "function"){
			callback("ok");
		}
	});
};

//选择一条记录
function getSelectedRow() {
    var grid = $("#jqgrid");
    var rowKey = grid.getGridParam("selrow");
    if(!rowKey){
    	alert("请选择一条记录");
    	return ;
    }
    var selectedIDs = grid.getGridParam("selarrrow");
    if(selectedIDs.length > 1){
    	alert("只能选择一条记录");
    	return ;
    }
    return selectedIDs[0];
}

//选择多条记录
function getSelectedRows() {
    var grid = $("#jqgrid");
    var rowKey = grid.getGridParam("selrow");
    if(!rowKey){
    	alert("请选择一条记录");
    	return ;
    }
    
    return grid.getGridParam("selarrrow");
}
//重置按钮事件  
$(document).ready(function(){  
    $("#reset").off().on("click",function(){  
         $('#myform')[0].reset();
    });  
});  

