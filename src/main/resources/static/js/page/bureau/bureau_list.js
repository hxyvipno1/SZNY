$(function() {
	
	$("#jqGrid").jqGrid({
        url:"../../bureau/list",
        datatype:"json", //数据来源，本地数据
        height:420,//高度，表格高度。可为数值、百分比或'auto'
        height:'auto',//高度，表格高度。可为数值、百分比或'auto'
        width:1000,//这个宽度不能为百分比
        autowidth:true,//自动宽
        colNames:['单位编码','单位名称','地址', '联系人', '联系电话'],
        colModel:[			
	      			{  name: 'bureau_id',index:'bureau_id', width: 45, key: true,hidden:true},
	    			{  name: 'bureau_name',index:'bureau_name', width: 75 },
	    			{  name: 'bureau_address', index:'bureau_address',width: 75 },			
	    			{  name: 'contacts',index:'contacts', width: 75 },
	    			{  name: 'contacts_tel',index:'contacts_tel', width: 75},
                 ],
        viewrecords: true,
        
        rowNum: 10,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        jsonReader : {
        	root: "list",
            page: "pageNum",
            total: "pages",
            records: "navigatePages"
        },
        prmNames : {
        	page : "pageNum",
			rows : "limit",
			order : "order"
		},
		pager: "#gridPager",
		gridComplete : function() {
			// 隐藏grid底部滚动条
			$("#jqGrid").closest(".ui-jqgrid-bdiv").css({
				"overflow-x" : "hidden"
			});}
    });
	
	$("#add").click(function() {
		location.href = "../../page/bureau/bureau_detail.html";
	});
	
	$("#update").click(function() {
		var row = getSelectedRow();
		if (row == null) {
			return;
		}
		location.href ="../../page/bureau/bureau_update.html?row="+row;
	});
	$("#del").click(function() {
		var farmerIds = getSelectedRows();
		if (farmerIds == null) {
			return;
		}
		var ids = "";
		for (var i = 0; i < farmerIds.length; i++) {
			ids += eval(farmerIds[i]) + ",";
		}
		confirm('确定要删除选中的记录？', function() {
			$.ajax({
				type : "POST",
				url :"../../bureau/delete",
				data : {
					'Ids' : ids.substring(0, ids.length - 1)
				},
				contentType : "application/x-www-form-urlencoded",
				success : function(data) {
					alert(data.message);
					$("#jqGrid").trigger("reloadGrid");
				}
			});
		});
	})
})
	//选择一条记录
function getSelectedRow() {
    var grid = $("#jqGrid");
    var rowKey = grid.getGridParam("selrow");
    if(!rowKey){
    	alert("请选择一条记录");
    	return ;
    }
    var selectedIDs = grid.getGridParam("selarrrow");
    if(selectedIDs.length > 1){
    	alert("只能选择一条记录");
    	return ;
    }
    return selectedIDs[0];
}

//选择多条记录
function getSelectedRows() {
    var grid = $("#jqGrid");
    var rowKey = grid.getGridParam("selrow");
    if(!rowKey){
    	alert("请选择一条记录");
    	return ;
    }
    return grid.getGridParam("selarrrow");
}
//重置按钮事件  
$(document).ready(function(){  
    $("#reset").off().on("click",function(){  
         $('#myform')[0].reset();
    });  
});  

	