$(function() {
//批量领取
	$("#saveOrUpdate").click(
			
			function() {
				$("#unitOprater").val("");
				
				var $form = $(".form-horizontal");
				$.ajax({
					type : "post",
					dataType : "json",
					url : $form.attr("action"),
					data : $form.serialize(),
					contentType : "application/x-www-form-urlencoded",
					success : function(data) {
						if ("领取成功！"==data.message) {
							alert(data.message);
							/*//置空
							$("#startRtagNumber").val("");
							$("#number").val("");*/
							location.href = "../../page/farmer/farmer.html";
						} else {
							alert(data.message);
						}
					}
				});
			});
	
	$("#batchFetch").click(
			function() {
			$("#batchNumberDiv").show();
			$("#batchNumDiv").show();
			$("#unitNumberDiv").hide();
			
			$("#rfTagNumber").val("");
			$("#oprater").val("");
			
		});
	
	$("#unitFetch").click(
			function() {
				$("#batchNumberDiv").hide();
				$("#batchNumDiv").hide();
				$("#unitNumberDiv").show();

				$("#startRtagNumber").val("");
				$("#number").val("");
				$("#oprater").val("unit");
			});
//单个领取
	addPageInit();

});


function addPageInit() {
	$("#id").val(GetQueryString("id"));
	var s = decodeURI(GetQueryString("customerName"));
	$("#name").val(s);
	
	$("#customerId").val(GetQueryString("id"));
	$("#customerName").val(s);
}

function customerNameChange() {
	// 客户名称的选择
	var options = $("#customerSeclect option:selected");
	var customerName = options.text();
	$("#customerName").val(customerName);
}

function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}
//对要领用的起始射频号进行验证
function startNumberChecked(){
	var rTagNumber = $("#startRtagNumber").val();
	var rfTagNumber = $("#rfTagNumber").val();
	var str = null;
	if(rTagNumber !=""){
		str = rTagNumber;
	}
	if(rfTagNumber !=""){
		str = rfTagNumber;
	}
	var reg=/^[1-9]{1}[0-9]{14}$/;
	if(reg.test(str)){
		$.ajax({
			type : "post",
			dataType : "json",
			url :"../../earTag/checkedRfTagNumber",
			data :{"rfTagNumber":str},
			contentType : "application/x-www-form-urlencoded",
			success : function(data) {
				if (""==data.message) {
					return ;
				} else {
					alert(data.message);
					$("#startRtagNumber").val("");
					return ;
				}
			}
		});
	}else{
		alert("此号码是由15位数字组成，请核对！");
		return ;
	}	
	
}