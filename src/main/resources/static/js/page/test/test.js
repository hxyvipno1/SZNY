$(function() {
	//加载页面列表数据
	pageInit();
	//搜索
	$("#searchButton").click(search);
	//新增页面
	$("#addButton").click(addPage);

});
function addPage() {
	//页面需要初始数据，如下拉框，跳转到对应页面通过AJAX方式调用控制器的初始化方法
	location.href="../../page/test/test_add.html";
}
function search() {
	//获取查询条件
	var idval = encodeURIComponent($("#id").val());
	var nameval = encodeURIComponent($("#name").val());
	//var companyNameval = encodeURIComponent($("#comanyName").val());
	var companyNameval = $("#companyName").val();

	var postJson = {
		id : idval,
		name : nameval,
		companyName : companyNameval
	};
	//传入查询条件参数  
	$("#list").jqGrid("setGridParam", {
		postData : postJson
	});
	//每次提出新的查询都转到第一页  
	$("#list").jqGrid("setGridParam", {
		page : 1
	});
	//提交post并刷新表格  
	$("#list").jqGrid("setGridParam", {
		url : $("#searchForm").attr("action")
	}).trigger("reloadGrid");
}
function pageInit() {
	$("#list").jqGrid({
		url : '../../test/t',// 请求数据的地址
		datatype : "json",

		colNames : [ 'Id', '姓名', '年龄' ],
		// jqgrid主要通过下面的索引信息与后台传过来的值对应
		colModel : [ {
			name : 'id',
			index : 'id',
			width : 255
		}, {
			name : 'name',
			index : 'invdate',
			width : 290
		}, {
			name : 'companyName',
			index : 'companyName',
			width : 500
		} ],

		height : 385,
		rownumbers : true,
		rownumWidth : 25,
		autowidth : true,
		multiselect : true,

		rowNum : 10,
		rowList : [ 10, 20, 30 ],

		sortname : 'id',
		recordpos : 'right',
		viewrecords : true,
		pager : '#pager',
		jsonReader : {
			root : "page.list",
			page : "page.currPage",
			total : "page.totalPage",
			records : "page.totalCount"
		},
		prmNames : {
			page : "page",
			rows : "limit",
			order : "order"
		},
		gridComplete : function() {
			// 隐藏grid底部滚动条
			$("#jqGrid").closest(".ui-jqgrid-bdiv").css({
				"overflow-x" : "hidden"
			});
		}
	});
}