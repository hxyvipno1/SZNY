$(function() {
	// 新增
	$("#add").click(function() {
		location.href = "../../page/farmer/farmer_detail.html";
	});
	 $("#jqGrid").jqGrid({
	        url:"../../quhua/list",
	        datatype:"json", //数据来源，本地数据
	        height:420,//高度，表格高度。可为数值、百分比或'auto'
	        width:1000,//这个宽度不能为百分比
	        autowidth:true,//自动宽
	        colNames:['区域ID','区域全称', '区域简称', '父类ID','区域代码','区域类型','区域源类型 ','大区域','builder code','删除状态','区域详情类型','是否标准','管理县'],
	        colModel:[
	            {name:'id',index:'id', width:'50', key: true ,hidden:true },
	            {name:'fullname',index:'fullname', width:'50',align:'center'},
	            {name:'shortname',index:'shortname', width:'50',align:'center'},
	            {name:'parentid',index:'parentid', width:'50', align:"center"},
	            {name:'regioncode',index:'regioncode', width:'50', align:"left", sortable:false},
	            {name:'regiontype',index:'regiontype', width:'50',align:"center", sortable:false},
	            {name:'regionsourcetype',index:'regionsourcetype', width:'50',align:"center", sortable:false},
	            {name:'bigregion',index:'bigregion', width:'50',align:"center", sortable:false},
	            {name:'buildercode',index:'buildercode', width:'50',align:"center", sortable:false},
	            {name:'isdelete',index:'isdelete', width:'50',align:"center", sortable:false},
	            {name:'regiondetailtype',index:'regiondetailtype', width:'50',align:"center", sortable:false},
	            {name:'isnonstandard',index:'isnonstandard', width:'50',align:"center", sortable:false},
	            {name:'isprovincemanagecounty',index:'isprovincemanagecounty', width:'50',align:"center", sortable:false},
	        ],
	        viewrecords: true,
	        height: 385,
	        rowNum: 10,
	        rowList : [10,30,50],
	        rownumbers: true,
	        rownumWidth: 25,
	        autowidth:true,
	        multiselect: true,
	        jsonReader : {
	            root: "page.list",
	            page: "page.pageNum",
	            total: "page.pages",
	            records: "page.navigatePages"
	        },
	        prmNames : {
				page : "pageNum",
				rows : "limit",
				order : "order"
			},
			pager: "#gridPager",
			ondblClickRow:function(id){
				 layer.open({
					  type: 2,
					  title: '查看信息',
					  maxmin: true,
					  shadeClose: true, //点击遮罩关闭层
					  area : ['45%' , '60%'],
					  content: '../../page/farmer/farmer_open.html?id='+id,
					  });
			},
			gridComplete : function() {
				// 隐藏grid底部滚动条
				$("#jqGrid").closest(".ui-jqgrid-bdiv").css({
					"overflow-x" : "hidden"
				});}
	        
	    });
	// 修改
	$("#update").click(function() {
		var row = getSelectedRow();
		if (row == null) {
			return;
		}
		location.href = "../../page/farmer/farmer_detail.html?row="+row;
	});
	//领取识读器
	$("#receive").click(function() {
		var row = getSelectedRow();
		if (row == null) {
			return;
		}
		location.href = "../../page/receive/receive_reader.html?row="+row;
	});
	
	//SIM卡记录
	$("#simCard").click(function() {
		var row = getSelectedRow();
		if (row == null) {
			return;
		}
		location.href = "../../page/sim/customer_sim_record.html?row="+row;
	});
	
	//	查询
	$("#query").click(function() {
		var $form = $("#myform");
		$("#jqGrid").jqGrid('setGridParam', {
			page : 1,
			url:"../../farmer/query", 
			mtype: 'POST',
	        postData:$form.serialize(),
		}).trigger("reloadGrid");
	});

	
	
	// 删除
	$("#del").click(function() {
		var farmerIds = getSelectedRows();
		if (farmerIds == null) {
			return;
		}
		var ids = "";
		for (var i = 0; i < farmerIds.length; i++) {
			ids += eval(farmerIds[i]) + ",";
		}
		confirm('确定要删除选中的记录？', function() {
			$.ajax({
				type : "POST",
				url :"../../farmer/delete",
				data : {
					'farmerIds' : ids.substring(0, ids.length - 1)
				},
				contentType : "application/x-www-form-urlencoded",
				success : function(data) {
					alert(data.message);
					$("#jqGrid").trigger("reloadGrid");
				}
			});
		});
	});
	
	//耳标的领取
	$("#fetch").click(function() {
		var gr = $("#jqGrid").getGridParam('selrow');
		var customerName = $("#jqGrid").getCell(gr,"name");
		var row = getSelectedRow();
		if (row == null) {
			return;
		}
		
		
		//alert(encodeURIComponent(customerName));
		location.href = "../../page/earTag/farmerAddEarTag_detail.html?customerName="+encodeURI(encodeURI(customerName))+"&id=" + eval(row);
	});
});
//jqGrid的配置信息
$.jgrid.defaults.width = 1000;
$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

//重写alert
window.alert = function(msg, callback){
	parent.layer.alert(msg, function(index){
		parent.layer.close(index);
		if(typeof(callback) === "function"){
			callback("ok");
		}
	});
};

//重写confirm式样框
window.confirm = function(msg, callback){
	parent.layer.confirm(msg, {btn: ['确定','取消']},
	function(){//确定事件
		if(typeof(callback) === "function"){
			callback("ok");
		}
	});
};

//选择一条记录
function getSelectedRow() {
    var grid = $("#jqGrid");
    var rowKey = grid.getGridParam("selrow");
    if(!rowKey){
    	alert("请选择一条记录");
    	return ;
    }
    var selectedIDs = grid.getGridParam("selarrrow");
    if(selectedIDs.length > 1){
    	alert("只能选择一条记录");
    	return ;
    }
    return selectedIDs[0];
}

//选择多条记录
function getSelectedRows() {
    var grid = $("#jqGrid");
    var rowKey = grid.getGridParam("selrow");
    if(!rowKey){
    	alert("请选择一条记录");
    	return ;
    }
    return grid.getGridParam("selarrrow");
}
//重置按钮事件  
$(document).ready(function(){  
    $("#reset").off().on("click",function(){  
         $('#myform')[0].reset();
    });  
});  

