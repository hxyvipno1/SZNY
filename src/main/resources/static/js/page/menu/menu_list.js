function __RUN_ACT(url,data,_callback){
	$.ajax({
		url: url,type: "POST",
		contentType: "application/json; charset=utf-8", 
		 data:JSON.stringify(data) ,
		 dataType: "json",
		 success: function(r){
			 var d=r;
			 _callback.call(null,d);
		 }
	});
}

function __RUN_STEP(step,_callback){
	__RUN_ACT("/menu/query",{step:step},_callback);
}

function __RUN_STEP_INSERT(step,_callback){
	__RUN_ACT("/step",{step:step},_callback);
}

function __GET_FROM_VALUES(id){
	var result={};
	var form=$("#"+id);
	form.find('input').each(function(index,item){
		var v=$(item).val();
		result[$(item).attr('name')]=v;
	});

	return result;
}