/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.model;

import java.util.Date;

/**
 * 
 * @author liyuran
 * @version $Id: Test.java, v 0.1 2017年7月11日 上午11:17:18 liyuran Exp $
 */
public class Business {

    private String idx;
    private String busi_name;
    private String pid;
    private String sort_no;
    private String level;
    private String remark;
    private String   create_time;

    /**
     * Getter method for property <tt>idx</tt>.
     * 
     * @return property value of idx
     */
    public String getIdx() {
        return idx;
    }

    /**
     * Setter method for property <tt>idx</tt>.
     * 
     * @param idx value to be assigned to property idx
     */
    public void setIdx(String idx) {
        this.idx = idx;
    }

    /**
     * Getter method for property <tt>busi_name</tt>.
     * 
     * @return property value of busi_name
     */
    public String getBusi_name() {
        return busi_name;
    }

    /**
     * Setter method for property <tt>busi_name</tt>.
     * 
     * @param busi_name value to be assigned to property busi_name
     */
    public void setBusi_name(String busi_name) {
        this.busi_name = busi_name;
    }

    /**
     * Getter method for property <tt>pid</tt>.
     * 
     * @return property value of pid
     */
    public String getPid() {
        return pid;
    }

    /**
     * Setter method for property <tt>pid</tt>.
     * 
     * @param pid value to be assigned to property pid
     */
    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * Getter method for property <tt>sort_no</tt>.
     * 
     * @return property value of sort_no
     */
    public String getSort_no() {
        return sort_no;
    }

    /**
     * Setter method for property <tt>sort_no</tt>.
     * 
     * @param sort_no value to be assigned to property sort_no
     */
    public void setSort_no(String sort_no) {
        this.sort_no = sort_no;
    }

    /**
     * Getter method for property <tt>level</tt>.
     * 
     * @return property value of level
     */
    public String getLevel() {
        return level;
    }

    /**
     * Setter method for property <tt>level</tt>.
     * 
     * @param level value to be assigned to property level
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /**
     * Getter method for property <tt>remark</tt>.
     * 
     * @return property value of remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Setter method for property <tt>remark</tt>.
     * 
     * @param remark value to be assigned to property remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * Getter method for property <tt>create_time</tt>.
     * 
     * @return property value of create_time
     */
    public String getCreate_time() {
        return create_time;
    }

    /**
     * Setter method for property <tt>create_time</tt>.
     * 
     * @param create_time value to be assigned to property create_time
     */
    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

  

}
