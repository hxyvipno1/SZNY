/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
/**
 * 
 * @author liyuran
 * @version $Id: package-info.java, v 0.1 2017年7月11日 下午2:52:44 liyuran Exp $
 */
package com.differ.microservice.model;

import java.util.Date;

public class User {
    
    public User(){}
	
	private String idx;
	private String user_name;
	private String password;
	private String email;
	private String mobile;
	private String status;
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
    
	
}