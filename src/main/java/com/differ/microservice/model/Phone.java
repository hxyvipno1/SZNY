/**
 * 
 */
package com.differ.microservice.model;

import java.util.Date;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: Phone.java, v 0.1 2017年7月18日 上午10:25:35 zhenshan.xu Exp $
 */
public class Phone {
	
	/**idx*/
	private String idx;
	/**手机品牌*/
	private String brand;
	/**手机型号*/
	private String model;
	/**数量*/
	private String number;
	/**单价*/
	private String price;
	/**时间*/
	private Date create_time;
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	

}
