/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.model;

/**
 * 
 * @author liyuran
 * @version $Id: Test.java, v 0.1 2017年7月11日 上午11:17:18 liyuran Exp $
 */
public class Test {
    public Test(){}
    private String idx;
    private String company_name;
    private String company_address;
    private String company_contact;
    private String company_tel;
    private String company_logo;
    private String create_time;
    private Integer company_status;
    /**
     * Getter method for property <tt>idx</tt>.
     * 
     * @return property value of idx
     */
    public String getIdx() {
        return idx;
    }
    /**
     * Setter method for property <tt>idx</tt>.
     * 
     * @param idx value to be assigned to property idx
     */
    public void setIdx(String idx) {
        this.idx = idx;
    }
    /**
     * Getter method for property <tt>company_name</tt>.
     * 
     * @return property value of company_name
     */
    public String getCompany_name() {
        return company_name;
    }
    /**
     * Setter method for property <tt>company_name</tt>.
     * 
     * @param company_name value to be assigned to property company_name
     */
    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }
    /**
     * Getter method for property <tt>company_address</tt>.
     * 
     * @return property value of company_address
     */
    public String getCompany_address() {
        return company_address;
    }
    /**
     * Setter method for property <tt>company_address</tt>.
     * 
     * @param company_address value to be assigned to property company_address
     */
    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }
    /**
     * Getter method for property <tt>company_contact</tt>.
     * 
     * @return property value of company_contact
     */
    public String getCompany_contact() {
        return company_contact;
    }
    /**
     * Setter method for property <tt>company_contact</tt>.
     * 
     * @param company_contact value to be assigned to property company_contact
     */
    public void setCompany_contact(String company_contact) {
        this.company_contact = company_contact;
    }
    /**
     * Getter method for property <tt>company_tel</tt>.
     * 
     * @return property value of company_tel
     */
    public String getCompany_tel() {
        return company_tel;
    }
    /**
     * Setter method for property <tt>company_tel</tt>.
     * 
     * @param company_tel value to be assigned to property company_tel
     */
    public void setCompany_tel(String company_tel) {
        this.company_tel = company_tel;
    }
    /**
     * Getter method for property <tt>company_logo</tt>.
     * 
     * @return property value of company_logo
     */
    public String getCompany_logo() {
        return company_logo;
    }
    /**
     * Setter method for property <tt>company_logo</tt>.
     * 
     * @param company_logo value to be assigned to property company_logo
     */
    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }
    /**
     * Getter method for property <tt>create_time</tt>.
     * 
     * @return property value of create_time
     */
    public String getCreate_time() {
        return create_time;
    }
    /**
     * Setter method for property <tt>create_time</tt>.
     * 
     * @param create_time value to be assigned to property create_time
     */
    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
    /**
     * Getter method for property <tt>company_status</tt>.
     * 
     * @return property value of company_status
     */
    public Integer getCompany_status() {
        return company_status;
    }
    /**
     * Setter method for property <tt>company_status</tt>.
     * 
     * @param company_status value to be assigned to property company_status
     */
    public void setCompany_status(Integer company_status) {
        this.company_status = company_status;
    }
    

}
