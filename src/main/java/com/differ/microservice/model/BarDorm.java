package com.differ.microservice.model;

import java.math.BigDecimal;
import java.util.Date;

public class BarDorm {
	
	public BarDorm() {
	}

	/**
	 * 主键
	 */
	private String idx;
	
	/**
	 * 父类ID（舍id）
	 */
	private String parent_id;
	
	/**
	 * （栏舍负责人）农户ID
	 */
	private String customer_idx;
	/**
	 * 栏号
	 */
	private String bar_no;
	/**
	 * 品种
	 */
	private String varieties;
	/**
	 * 射频rfid编号
	 */
	private BigDecimal rfid_no;
	/**
	 * 生物编号
	 */
	private String biont_no;
	/**
	 * 栏舍负责人联系方式
	 */
	private String cutomer_phone;
	/**
	 * 信息员
	 */
	private String messenger;
	/**
	 * 信息员联系方式
	 */
	private String messenger_phone;
	/**
	 * 创建时间
	 */
	private String create_time;
	/**
	 * 修改时间
	 */
	private String update_time;
	
	
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	
	public String getCustomer_idx() {
		return customer_idx;
	}
	public void setCustomer_idx(String customer_idx) {
		this.customer_idx = customer_idx;
	}
	public String getBar_no() {
		return bar_no;
	}
	public void setBar_no(String bar_no) {
		this.bar_no = bar_no;
	}
	public String getVarieties() {
		return varieties;
	}
	public void setVarieties(String varieties) {
		this.varieties = varieties;
	}
	public BigDecimal getRfid_no() {
		return rfid_no;
	}
	public void setRfid_no(BigDecimal rfid_no) {
		this.rfid_no = rfid_no;
	}
	public String getBiont_no() {
		return biont_no;
	}
	public void setBiont_no(String biont_no) {
		this.biont_no = biont_no;
	}
	public String getCutomer_phone() {
		return cutomer_phone;
	}
	public void setCutomer_phone(String cutomer_phone) {
		this.cutomer_phone = cutomer_phone;
	}
	public String getMessenger() {
		return messenger;
	}
	public void setMessenger(String messenger) {
		this.messenger = messenger;
	}
	public String getMessenger_phone() {
		return messenger_phone;
	}
	public void setMessenger_phone(String messenger_phone) {
		this.messenger_phone = messenger_phone;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	

}
