/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.model;

import java.util.Date;

/**
 * 
 * @author yue.li
 * @version $Id: Ear_tag.java, v 0.1 2017年7月14日 上午11:26:15 yue.li Exp $
 */
public class EarTag {

    private String idx;
    private long   rfid_no;
    private long   qr_no;
    private int    status;
    private String customer_id;
    private String customer_name;
    private String remark;
    private Date   create_time;
    private String use_time;
    private int    id;
    private String batch_idx;

    /**
     * Getter method for property <tt>idx</tt>.
     * 
     * @return property value of idx
     */
    public String getIdx() {
        return idx;
    }

    /**
     * Setter method for property <tt>idx</tt>.
     * 
     * @param idx value to be assigned to property idx
     */
    public void setIdx(String idx) {
        this.idx = idx;
    }

    /**
     * Getter method for property <tt>rfid_no</tt>.
     * 
     * @return property value of rfid_no
     */
    public long getRfid_no() {
        return rfid_no;
    }

    /**
     * Setter method for property <tt>rfid_no</tt>.
     * 
     * @param rfid_no value to be assigned to property rfid_no
     */
    public void setRfid_no(long rfid_no) {
        this.rfid_no = rfid_no;
    }

    /**
     * Getter method for property <tt>qr_no</tt>.
     * 
     * @return property value of qr_no
     */
    public long getQr_no() {
        return qr_no;
    }

    /**
     * Setter method for property <tt>qr_no</tt>.
     * 
     * @param qr_no value to be assigned to property qr_no
     */
    public void setQr_no(long qr_no) {
        this.qr_no = qr_no;
    }

    /**
     * Getter method for property <tt>status</tt>.
     * 
     * @return property value of status
     */
    public int getStatus() {
        return status;
    }

    /**
     * Setter method for property <tt>status</tt>.
     * 
     * @param status value to be assigned to property status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * Getter method for property <tt>customer_id</tt>.
     * 
     * @return property value of customer_id
     */
    public String getCustomer_id() {
        return customer_id;
    }

    /**
     * Setter method for property <tt>customer_id</tt>.
     * 
     * @param customer_id value to be assigned to property customer_id
     */
    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    /**
     * Getter method for property <tt>customer_name</tt>.
     * 
     * @return property value of customer_name
     */
    public String getCustomer_name() {
        return customer_name;
    }

    /**
     * Setter method for property <tt>customer_name</tt>.
     * 
     * @param customer_name value to be assigned to property customer_name
     */
    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    /**
     * Getter method for property <tt>remark</tt>.
     * 
     * @return property value of remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Setter method for property <tt>remark</tt>.
     * 
     * @param remark value to be assigned to property remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * Getter method for property <tt>create_time</tt>.
     * 
     * @return property value of create_time
     */
    public Date getCreate_time() {
        return create_time;
    }

    /**
     * Setter method for property <tt>create_time</tt>.
     * 
     * @param create_time value to be assigned to property create_time
     */
    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    /**
     * Getter method for property <tt>use_time</tt>.
     * 
     * @return property value of use_time
     */
    public String getUse_time() {
        return use_time;
    }

    /**
     * Setter method for property <tt>use_time</tt>.
     * 
     * @param use_time value to be assigned to property use_time
     */
    public void setUse_time(String use_time) {
        this.use_time = use_time;
    }

    /**
     * Getter method for property <tt>id</tt>.
     * 
     * @return property value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for property <tt>id</tt>.
     * 
     * @param id value to be assigned to property id
     */
    public void setId(int id) {
        this.id = id;
    }

}
