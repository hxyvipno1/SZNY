/**
 * 
 */
package com.differ.microservice.model;

import java.util.Date;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: Reader.java, v 0.1 2017年7月18日 上午10:25:35 zhenshan.xu Exp $
 */
public class Reader {
	
	/**idx*/
	private String idx;
	/**识读器编号*/
	private String reader_code;
	/**农户id*/
	private String customer_id;
	/**农户姓名*/
	private String customer_name;
	/**识读器状态*/
	private String status;
	/**SIM*/
	private String sim;
	/**批次*/
	private String batch;
	/**型号*/
	private String model;
	/**供应商*/
	private String supplier;
	/**时间*/
	private Date create_time;
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getReader_code() {
		return reader_code;
	}
	public void setReader_code(String reader_code) {
		this.reader_code = reader_code;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSim() {
		return sim;
	}
	public void setSim(String sim) {
		this.sim = sim;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	
	

}
