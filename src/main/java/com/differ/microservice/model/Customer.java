/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.model;

import java.sql.Date;

/**
 * 散户实体
 * @author dunfang.zhou
 * @version $Id: Customer.java, v 0.1 2017年7月25日 下午2:21:17 dunfang.zhou Exp $
 */
public class Customer {
    
    /**  */
    private String  idx;
    /**  */
    private String  name;
    /**  */
    private String  identity_no;
    /**  */
    private int     sex;
    /**  */
    private String  nation;
    /**  */
    private Date    birthdate;
    /**  */
    private String  certificate_site;
    /**  */
    private String  phone;
    /**  */
    private String  email;
    /**  */
    private String  residential_site;
    /**  */
    private int     marital_status;
    /**  */
    private String  public_check;
    /**  */
    private String  portrait_name; 
    /**  */
    private String  portrait_path;
    /**  */
    private String  district_idx;
    /**  */
    private int     breeding_stock;
    /**  */
    private String  create_time;
    /**  */
    private String  update_time;
    /**
     * Getter method for property <tt>idx</tt>.
     * 
     * @return property value of idx
     */
    public String getIdx() {
        return idx;
    }
    /**
     * Setter method for property <tt>idx</tt>.
     * 
     * @param idx value to be assigned to property idx
     */
    public void setIdx(String idx) {
        this.idx = idx;
    }
    /**
     * Getter method for property <tt>name</tt>.
     * 
     * @return property value of name
     */
    public String getName() {
        return name;
    }
    /**
     * Setter method for property <tt>name</tt>.
     * 
     * @param name value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * Getter method for property <tt>identity_no</tt>.
     * 
     * @return property value of identity_no
     */
    public String getIdentity_no() {
        return identity_no;
    }
    /**
     * Setter method for property <tt>identity_no</tt>.
     * 
     * @param identity_no value to be assigned to property identity_no
     */
    public void setIdentity_no(String identity_no) {
        this.identity_no = identity_no;
    }
    /**
     * Getter method for property <tt>sex</tt>.
     * 
     * @return property value of sex
     */
    public int getSex() {
        return sex;
    }
    /**
     * Setter method for property <tt>sex</tt>.
     * 
     * @param sex value to be assigned to property sex
     */
    public void setSex(int sex) {
        this.sex = sex;
    }
    /**
     * Getter method for property <tt>nation</tt>.
     * 
     * @return property value of nation
     */
    public String getNation() {
        return nation;
    }
    /**
     * Setter method for property <tt>nation</tt>.
     * 
     * @param nation value to be assigned to property nation
     */
    public void setNation(String nation) {
        this.nation = nation;
    }
    /**
     * Getter method for property <tt>birthdate</tt>.
     * 
     * @return property value of birthdate
     */
    public Date getBirthdate() {
        return birthdate;
    }
    /**
     * Setter method for property <tt>birthdate</tt>.
     * 
     * @param birthdate value to be assigned to property birthdate
     */
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
    /**
     * Getter method for property <tt>certificate_site</tt>.
     * 
     * @return property value of certificate_site
     */
    public String getCertificate_site() {
        return certificate_site;
    }
    /**
     * Setter method for property <tt>certificate_site</tt>.
     * 
     * @param certificate_site value to be assigned to property certificate_site
     */
    public void setCertificate_site(String certificate_site) {
        this.certificate_site = certificate_site;
    }
    /**
     * Getter method for property <tt>phone</tt>.
     * 
     * @return property value of phone
     */
    public String getPhone() {
        return phone;
    }
    /**
     * Setter method for property <tt>phone</tt>.
     * 
     * @param phone value to be assigned to property phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /**
     * Getter method for property <tt>email</tt>.
     * 
     * @return property value of email
     */
    public String getEmail() {
        return email;
    }
    /**
     * Setter method for property <tt>email</tt>.
     * 
     * @param email value to be assigned to property email
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * Getter method for property <tt>residential_site</tt>.
     * 
     * @return property value of residential_site
     */
    public String getResidential_site() {
        return residential_site;
    }
    /**
     * Setter method for property <tt>residential_site</tt>.
     * 
     * @param residential_site value to be assigned to property residential_site
     */
    public void setResidential_site(String residential_site) {
        this.residential_site = residential_site;
    }
    /**
     * Getter method for property <tt>marital_status</tt>.
     * 
     * @return property value of marital_status
     */
    public int getMarital_status() {
        return marital_status;
    }
    /**
     * Setter method for property <tt>marital_status</tt>.
     * 
     * @param marital_status value to be assigned to property marital_status
     */
    public void setMarital_status(int marital_status) {
        this.marital_status = marital_status;
    }
    /**
     * Getter method for property <tt>public_check</tt>.
     * 
     * @return property value of public_check
     */
    public String getPublic_check() {
        return public_check;
    }
    /**
     * Setter method for property <tt>public_check</tt>.
     * 
     * @param public_check value to be assigned to property public_check
     */
    public void setPublic_check(String public_check) {
        this.public_check = public_check;
    }
    /**
     * Getter method for property <tt>portrait_name</tt>.
     * 
     * @return property value of portrait_name
     */
    public String getPortrait_name() {
        return portrait_name;
    }
    /**
     * Setter method for property <tt>portrait_name</tt>.
     * 
     * @param portrait_name value to be assigned to property portrait_name
     */
    public void setPortrait_name(String portrait_name) {
        this.portrait_name = portrait_name;
    }
    /**
     * Getter method for property <tt>portrait_path</tt>.
     * 
     * @return property value of portrait_path
     */
    public String getPortrait_path() {
        return portrait_path;
    }
    /**
     * Setter method for property <tt>portrait_path</tt>.
     * 
     * @param portrait_path value to be assigned to property portrait_path
     */
    public void setPortrait_path(String portrait_path) {
        this.portrait_path = portrait_path;
    }
    /**
     * Getter method for property <tt>district_idx</tt>.
     * 
     * @return property value of district_idx
     */
    public String getDistrict_idx() {
        return district_idx;
    }
    /**
     * Setter method for property <tt>district_idx</tt>.
     * 
     * @param district_idx value to be assigned to property district_idx
     */
    public void setDistrict_idx(String district_idx) {
        this.district_idx = district_idx;
    }
    /**
     * Getter method for property <tt>breeding_stock</tt>.
     * 
     * @return property value of breeding_stock
     */
    public int getBreeding_stock() {
        return breeding_stock;
    }
    /**
     * Setter method for property <tt>breeding_stock</tt>.
     * 
     * @param breeding_stock value to be assigned to property breeding_stock
     */
    public void setBreeding_stock(int breeding_stock) {
        this.breeding_stock = breeding_stock;
    }
    /**
     * Getter method for property <tt>create_time</tt>.
     * 
     * @return property value of create_time
     */
    public String getCreate_time() {
        return create_time;
    }
    /**
     * Setter method for property <tt>create_time</tt>.
     * 
     * @param create_time value to be assigned to property create_time
     */
    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
    /**
     * Getter method for property <tt>update_time</tt>.
     * 
     * @return property value of update_time
     */
    public String getUpdate_time() {
        return update_time;
    }
    /**
     * Setter method for property <tt>update_time</tt>.
     * 
     * @param update_time value to be assigned to property update_time
     */
    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }
    
}
