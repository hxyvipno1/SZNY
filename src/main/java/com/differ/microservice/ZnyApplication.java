package com.differ.microservice;

import com.differ.microservice.core.spring.UxInitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * Created by cailm on 2017-07-04.
 */

@SpringBootApplication
public class ZnyApplication {
    public static void main(String[] args) {
        //SpringApplication.run(OnlineDesignerApplication.class,args);
        SpringApplication app = new SpringApplication(ZnyApplication.class);
        app.addInitializers(new UxInitializingBean());
        app.run(args);
    }
}
