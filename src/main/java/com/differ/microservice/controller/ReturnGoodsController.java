/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.ReturnGoodsService;

/**
 * 物品退货
 * @author zhenshan.xu
 * @version $Id: ReturnGoodsController.java, v 0.1 2017年11月18日 下午2:27:29
 *          zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/returngoods")
public class ReturnGoodsController extends BaseSupport {
	@Autowired
	private ReturnGoodsService returngoods;
	
	/**
	 * 物资退货
	 * @param req
	 * @return
	 */
	@RequestMapping("/refund")
	@ResponseBody
	public String storage(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(returngoods.storage(param));

	}
	

}
