/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.ProductManagementService;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: ProductManagementController.java, v 0.1 2017年10月15日 上午9:18:23 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/product")
public class ProductManagementController extends BaseSupport {
	
	@Autowired
	private ProductManagementService productService;


	@RequestMapping("/init")
	@ResponseBody
	public String List(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(productService.findAll(param));

	}
	
	@RequestMapping("/queryByidx")
	@ResponseBody
	public String queryByIdx(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(productService.queryByIdx(param));

	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	public String Add(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(productService.addProduct(param));

	}
	@RequestMapping("update")
	@ResponseBody
	public String Update(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(productService.updateProduct(param));

	}
	
	/**
	 * 产品单据号及发生日期
	 * @param req
	 * @return
	 */
	@RequestMapping("/documentNum")
	@ResponseBody
	public String documentNum(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(productService.document(param));

	}
	
	/**
	 * 通过idx查询节点
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryByIdx")
    @ResponseBody
    public String getDictionaryByIdx(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(productService.findByIdx(param));
    }
 
    
    @RequestMapping("/queryByPid")
    @ResponseBody
    public String getDictionaryByPid(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(productService.findByPid(param));
    }
    
    @RequestMapping("/queryByModel")
    @ResponseBody
    public String getByType(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(productService.queryByModel(param));
    }
	/**
	 * 根据idx删除所选物资及其子节点
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(HttpServletRequest req) {

		Map param = getInputJSON(req);
		return output(productService.delete(param));

	}
	/**
	 * 产品下拉
	 * @param req
	 * @return
	 */
	@RequestMapping("/goods")
	@ResponseBody
	public String gools(HttpServletRequest req) {

		Map param = getInputJSON(req);
		return output(productService.queryByGoods(param));

	}
}
