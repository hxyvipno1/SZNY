/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.SuppliersService;

/**
 * 供应商管理
 * @author zhenshan.xu
 * @version $Id: SuppliersController.java, v 0.1 2017年8月23日 下午7:00:27 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/suppliers")
public class SuppliersController extends BaseSupport{
	
	@Autowired
	private SuppliersService suppliersService;

	/**
	 * 初始化供应商列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/init")
	@ResponseBody
	public String List(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(suppliersService.findAll());

	}
	
	/**
	 * 来往单位
	 * @param req
	 * @return
	 */
	@RequestMapping("/btype")
	@ResponseBody
	public String getBtype(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(suppliersService.btype(param));

	}
	
	/**
	 * 来往单位
	 * @param req
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public String update(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(suppliersService.update(param));

	}
	/**
	 * 
	 * 查询所有节点
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryByPid")
	@ResponseBody
	public String getNodeByPid(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(suppliersService.findByPid(param));
	}
	/**
	 * 根据idx查询供应商
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryByIdx")
	@ResponseBody
	public String getListByIdx(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(suppliersService.queryByIdx(param));
	}
	/**
	 * 查询供应商名称
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryName")
    @ResponseBody
    public String queryName(HttpServletRequest req){
        
    Map param=getInputJSON(req);
    return output(suppliersService.queryName(param));
    
    }
	/**
	 * 删除
	 * @param req
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(HttpServletRequest req) {

		Map param = getInputJSON(req);
		return output(suppliersService.delete(param));
	}
	
	 /**
     * 列表区域下拉框联动查询
     * @param req
     * @return
     */
    @RequestMapping("/queryCity")
    @ResponseBody
    public String getDistrictList(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(suppliersService.query(param));
    }
    
    /**
	 * 供应商树拖拽
	 * @param req
	 * @return
	 */
	@RequestMapping("/updateTree")
	@ResponseBody
	public String updateTree(HttpServletRequest req){
		Map param = getInputJSON(req);
		return output(suppliersService.updateTree(param));
	}
    
}
