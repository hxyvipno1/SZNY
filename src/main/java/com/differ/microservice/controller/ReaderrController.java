package com.differ.microservice.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.model.Reader;
import com.differ.microservice.service.FarmerService;
import com.differ.microservice.service.ReaderService;

/**
 * 用户控controller类
 * 注意：注解是@RestController，与平时用的@Controller稍有不同
 */

@RestController
@RequestMapping("/reader")
public class ReaderrController  extends BaseSupport{

    @Autowired
    private ReaderService service;
    @Autowired
    private FarmerService FarmerService;
    /**
	 * 根据idx查询信息
	 * @param idx
	 * @return
	 */
    @RequestMapping("/query/{idx}")
    @ResponseBody
    public String getListByIdx( @PathVariable String idx){
        return output(service.queryByIdx(idx));
    }
    /**
     * 查询所有信息
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public String getList(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
        return output(service.query(param));
    }
    /**
     * 添加识读器信息，默认状态为“0”初始
     * @param reader_code
     * @param customer_id
     * @param sim
     * @param batch
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public String add(HttpServletRequest req){
    	Map param = new HashMap();
		return output(service.add(param));
    }
    
    @RequestMapping("/batchadd")
    @ResponseBody
    public String bachadd(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
		return output(service.batchadd(param));
    }
    /**
     * 根据id
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public String deltet(HttpServletRequest id){
    	Map param=this.getInputJSON(id);
    	return output(service.deldete(param));
    }
   /**
    * 修改
    * @param req
    * @return
    */
    @RequestMapping("/update")
    @ResponseBody
    public String update(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
        return output(service.update(param));
    }
    
    @RequestMapping(value = "/checksn")
    @ResponseBody
    public String checksn(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
		return output(service.checksn(param));
    }
    
    @RequestMapping(value = "/grant")
    @ResponseBody
    public String grant(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
		return output(service.grant(param));
    }

}
