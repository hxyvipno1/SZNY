/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.CustomerService;
import com.differ.microservice.service.DistrictService;
import com.differ.microservice.service.FarmerService;
import com.differ.microservice.service.UserService;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: FarmerController.java, v 0.1 2017年7月10日 下午5:03:57 dunfang.zhou Exp $
 */

@RestController
@RequestMapping("/farmer")
public class CustomerController extends BaseSupport{
    
    @Autowired
    private CustomerService service;
    @Autowired
    private DistrictService districtService;
    /**
     * 列表查询
     * @param req
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public String getCustomer(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(service.findAll(param));
    }
    /**
     * 列表区域下拉框联动查询
     * @param req
     * @return
     */
    @RequestMapping("/queryCity")
    @ResponseBody
    public String getDistrictList(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(districtService.query(param));
    }

    /**
     * 散户、规模场详情信息
     * @param req
     * @return
     */
    @RequestMapping("/querydetails")
    @ResponseBody
    public String getCustomerDetails(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(service.findDetails(param));
    }
    /**
     * 新增技术员信息
     * @param req
     * @return
     */
    @RequestMapping("/technicianAdd")
    @ResponseBody
    public String CustomerTechnicianAdd(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(service.customerTechnicianAdd(param));
    }
    /**
     * 删除技术员信息
     * @param req
     * @return
     */
    @RequestMapping("/technicianDelete")
    @ResponseBody
    public String CustomerTechnicianDelete(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(service.customerTechnicianDelete(param));
    }
    /**
     * 散户、规模场详情更新
     * @param req
     * @return
     */
    @RequestMapping("/detailsUpdate")
    @ResponseBody
    public String CustomerDetailsUpdate(HttpServletRequest req){
    	Map param = getInputJSON(req);
    	return output(service.cusomerUpdate(param));
    }
    /**
     * 新增散户、规模场信息
     * @param req
     * @return
     */
    @RequestMapping("/detailsAdd")
    @ResponseBody
    public String CustomerDetailsAdd(HttpServletRequest req){
    	Map param = getInputJSON(req);
    	return output(service.customerAdd(param));
    }
    /**
     * 查询user_code（phone）是否唯一
     * @param req
     * @return
     */
    @RequestMapping("/usercodeSole")
    @ResponseBody
    public String UserCodeSole(HttpServletRequest req){
    	Map param = getInputJSON(req);
    	return output(service.usercodeSole(param));
    }
    /**
     * 删除散户、规模场中附件中间表数据
     * @param req
     * @return
     */
    @RequestMapping("/deleteFile")
    @ResponseBody
    public String DeleteFile(HttpServletRequest req){
    	Map param = getInputJSON(req);
    	return output(service.deleteFiel(param));
    }
    
    /**
     * 查询各地区规模场数量
     * @param req
     * @return
     */
    @RequestMapping("/queryCount")
    @ResponseBody
    public String queryCount(HttpServletRequest req){
        Map param = getInputJSON(req);
        return output(service.queryCount(param));
    }
    
    /**
     * 查询各地区规模场数量
     * @param req
     * @return
     */
    @RequestMapping("/queryCus")
    @ResponseBody
    public String queryCus(HttpServletRequest req){
        Map map = getInputJSON(req);
        return output(service.queryCus(map));
    }
    
    /**
     * 查询养殖场明细(地图)
     * @param req
     * @return
     */
   /* @RequestMapping("/queryDetail")
    @ResponseBody
    public String queryDetail(HttpServletRequest req){
        Map param = getInputJSON(req);
        return output(service.queryDetail(param));
    }*/
    
    /**
     * 规模场地址跳转后获取数据
     * @param req
     * @return
     */
    @RequestMapping("/customerSkip")
    @ResponseBody
    public String customerSkip(HttpServletRequest req){
        Map param = getInputJSON(req);
        return output(service.customerSkip(param));
    }
    
}
