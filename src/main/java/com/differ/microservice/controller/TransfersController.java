/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.TransfersService;

/**
 * 物资调拨
 * @author zhenshan.xu
 * @version $Id: TransfersController.java, v 0.1 2017年11月18日 上午11:02:17 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/transfers")
public class TransfersController extends BaseSupport{
	
	@Autowired
	private TransfersService transfersService;

	/**
	 * 查询调出机构
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public String query(HttpServletRequest req) {

		Map param = getInputJSON(req);

		return output(transfersService.query(param));

	}
	/**
	 * 查询调出机构
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/query1")
	@ResponseBody
	public String query1(HttpServletRequest req) {

		Map param = getInputJSON(req);

		return output(transfersService.query(param));

	}

	/**
	 * 物资调拨
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/adjust")
	@ResponseBody
	public String storage(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(transfersService.storage(param));

	}
		
}
