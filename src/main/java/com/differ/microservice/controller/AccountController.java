/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.AccountService;

/**
 * 饲料销售，原料采购
 * @author dunfang.zhou
 * @version $Id: AccountController.java, v 0.1 2017年9月16日 下午2:22:29 dunfang.zhou Exp $
 */
@RestController
@RequestMapping("/account")
public class AccountController extends BaseSupport{
    @Autowired
    private AccountService accountService;

    @RequestMapping("/findAllFeed")
    @ResponseBody
    public String findAllFeed(HttpServletRequest req) {

        Map param = getInputJSON(req);
        
        return output(accountService.findAllFeed(param));

    }
    
    @RequestMapping("/findAllPurchase")
    @ResponseBody
    public String findAllPurchase(HttpServletRequest req) {

        Map param = getInputJSON(req);
        
        return output(accountService.findAllPurchase(param));

    }
}
