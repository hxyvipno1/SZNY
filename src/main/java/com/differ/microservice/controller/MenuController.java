package com.differ.microservice.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.Menuservice;
import com.differ.microservice.service.UserService;

/**
 * 用户控controller类
 * 注意：注解是@RestController，与平时用的@Controller稍有不同
 */

@RestController
@RequestMapping("/menu")
public class MenuController  extends BaseSupport{

    @Autowired
    private Menuservice service;

    /**
     * 根据 主键查询
     * @param idx
     * @return
     */
    @RequestMapping("/query/{idx}")
    @ResponseBody
    public String getUserListByIdx( @PathVariable String idx){
        return output(service.queryByIdx(idx));
    }
    
    /**
     * 全部查询 无参数
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public String getUserList(){
        return output(service.query());
    }
    
    @RequestMapping("/query/{parent}")
    @ResponseBody
    public String getUserListByPid(@PathVariable Map param){
    	return output(service.queryParentId(param));
    }
  
}
