package com.differ.microservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.model.User;
import com.differ.microservice.service.UserService;

/**
 * 用户控controller类
 * 注意：注解是@RestController，与平时用的@Controller稍有不同
 */

@RestController
@RequestMapping("/user")
public class UserController  extends BaseSupport{

    @Autowired
    private UserService service;
    


//    @RequestMapping("/query/{idx}")
//    @ResponseBody
//    public String getUserListByIdx( @PathVariable String idx){
//        return output(service.queryByIdx(idx));
//    }
//    @RequestMapping("/query")
//    @ResponseBody
//    public String getUserList(){
//        return output(service.query());
//    }
//
    @RequestMapping("/login")
    @ResponseBody
    public String UserLogin(String user_name,String password ){
       List<User> users =service.queryByField(user_name, password);
       if(users.size()!=1){
          return  output(-2,"用户名或密码错误");
       }
       
       return output(1);
    }
 
}
