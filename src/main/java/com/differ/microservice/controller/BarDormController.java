package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.model.BarDorm;
import com.differ.microservice.service.BarDormService;

/**
 * 栏舍Controller类
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/bardorm")
public class BarDormController extends BaseSupport {
	
	@Autowired
	private BarDormService barDormService;
	
	/**
	 * 无参集合
	 */
	@RequestMapping("/list")
	@ResponseBody
	public String List(HttpServletRequest req){
		
		Map param=getInputJSON(req);
		return output(barDormService.findAll(param));
	}
	
	/**
	 * 根据idx查询
	 */
	@RequestMapping("/listidx/{idx}")
	public String ListByidx(@PathVariable String idx){
		return output(barDormService.findByKey(idx));
		
	}
	
	/**
     * 无参集合
     */
    @RequestMapping("/columnList")
    @ResponseBody
    public String ColumnList(HttpServletRequest req){
        
        Map param=getInputJSON(req);
        return output(barDormService.findColumnList(param));
    }
	/**
	 * 条件查询
	 */
	@RequestMapping("/queryDorms")
	public String queryDorms(HttpServletRequest req){
	    Map param=getInputJSON(req);
		return output(barDormService.findDorms(param));
	}
	
	/**
     * 条件查询
     */
    @RequestMapping("/queryByName")
    public String queryByName(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(barDormService.queryByName(param));
    }

	/**
     * 条件查询
     */
	@RequestMapping("/peopleList")
    @ResponseBody
    public String getPeopleList(HttpServletRequest req){
        
        Map param=getInputJSON(req);
        return output(barDormService.findPeopleList(param));
    }
	/**
	 * 新增  
	 */
	@RequestMapping("/add")
	public String add(BarDorm bardorm) {

        // 可以自己定义提示
        //int x=service.add(t);
        //return output(x,"添加成功");
        // 也可以使用默认提示

		if(bardorm.getParent_id() == null || bardorm.getParent_id().equals("")){
			bardorm.setParent_id("0");
		}
		
        int x = barDormService.add(bardorm);
        return output(x);
    }
	/**
	 * 修改
	 */
    @RequestMapping("/update")
    public String update(BarDorm bardorm) {
        //返回方式跟添加类似
        return output(barDormService.update(bardorm));
    }
	  
    /**
	   * 删除
	   * @param idx
	   * @return
	   */
    @RequestMapping("/delete")
    @ResponseBody
    public Map delete(HttpServletRequest req) {
        Map param=getInputJSON(req);
        Map del = barDormService.delete(param);
      return del;
    }
    
}



























