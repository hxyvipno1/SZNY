/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.ApplyForService;

/**
 * 物资申报
 * @author zhenshan.xu
 * @version $Id: ApplyForController.java, v 0.1 2017年9月16日 上午10:22:57 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/applyFor")
public class ApplyForController extends BaseSupport{
	
	@Autowired
	private ApplyForService applyFor;
	
	
	/**
	 * 申请人信息
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryPeople")
	@ResponseBody
	public String getPeople(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(applyFor.query(param));

	}
	/**
	 * 物资申报
	 * @param req
	 * @return
	 */
	@RequestMapping("/report")
	@ResponseBody
	public String report(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(applyFor.report(param));

	}
	
	@RequestMapping("/examines")
	@ResponseBody
	public String examines(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(applyFor.examines(param));

	}
	
}
