/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.BankService;

/**
 * 
 * 银行管理
 * 
 * @author zhenshan.xu
 * @version $Id: BankController.java, v 0.1 2017年8月20日 上午8:33:16 zhenshan.xu Exp
 *          $
 */
@RestController
@RequestMapping("/bank")
public class BankController extends BaseSupport {

	@Autowired
	private BankService bankService;

	/**
	 * 查询所有银行
	 * @param req
	 * @return
	 */
	@RequestMapping("/init")
	@ResponseBody
	public String List(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(bankService.findAll(param));

	}

	@RequestMapping("/queryByIdx")
	@ResponseBody
	public String getListByIdx(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(bankService.queryByIdx(param));
	}
	
	/**
	 * 查询银行名称
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryName")
    @ResponseBody
    public String queryName(HttpServletRequest req){
        
    Map param=getInputJSON(req);
    return output(bankService.queryName(param));
    
    }

	@RequestMapping("/queryNames")
    @ResponseBody
    public String queryNames(HttpServletRequest req){
        
    Map param=getInputJSON(req);
    return output(bankService.queryNames(param));
    
    }
	
	/**
	 * 查询能繁母猪与生猪的头数
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryCount")
    @ResponseBody
    public String queryCount(HttpServletRequest req){
        
    Map param=getInputJSON(req);
    return output(bankService.queryCount(param));
    
    }
	
	/**
	 * 删除
	 * @param req
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(HttpServletRequest req) {

		Map param = getInputJSON(req);
		return output(bankService.delete(param));

	}
	
	/**
	 * 银行树拖拽
	 * @param req
	 * @return
	 */
	@RequestMapping("/updateTree")
	@ResponseBody
	public String updateTree(HttpServletRequest req){
		Map param = getInputJSON(req);
		return output(bankService.updateTree(param));
	}
}
