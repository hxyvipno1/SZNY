/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.AnimalValuationService;

/**
 * 活体动物估值
 * @author zhenshan.xu
 * @version $Id: AnimalValuation.java, v 0.1 2017年9月10日 上午10:48:45 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/valuation")
public class AnimalValuation extends BaseSupport{
	
	@Autowired
	private AnimalValuationService valuation;
	
	@RequestMapping("/query")
	@ResponseBody
	public String List(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(valuation.find(param));

	}
	   /**
     * 规模场估值
     * @param req
     * @return
     */
    @RequestMapping("/queryDetails")
    @ResponseBody
    public String getCustomerDetails(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(valuation.findDetails(param));
    }
}
