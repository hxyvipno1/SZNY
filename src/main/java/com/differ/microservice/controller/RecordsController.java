/**
 * 
 */
package com.differ.microservice.controller;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.RecordsService;

/**
 * 查看记录，出入库单
 * 
 * @author zhenshan.xu
 * @version $Id: RecordsController.java, v 0.1 2017年7月29日 上午9:23:30 zhenshan.xu
 *          Exp $
 */
@RestController
@RequestMapping("/records")
public class RecordsController extends BaseSupport {

	@Autowired
	private RecordsService recordsService;

	/**
	 * 初始化发放记录信息
	 * 
	 * @param req
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping("/init")
	@ResponseBody
	public String getCustomer(HttpServletRequest req) throws ParseException {
		Map param = getInputJSON(req);
		Map pg = new HashMap<>(); 
		pg = (Map) param.get("pg");
		param = (Map) param.get("param");
		return output(recordsService.findAll(param,pg));
	}
	
	/**
	 * 查询详情
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryByIdx")
	@ResponseBody
	public String getByIdx(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(recordsService.queryByIdx(param));
	}

	
	/**
	 * 物资修改
	 * @param req
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public String update(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(recordsService.update(param));

	}
	
	@RequestMapping("/toView")
	@ResponseBody
	public String getToview(HttpServletRequest req) throws ParseException{
		Map param = getInputJSON(req);
		Map pg = new HashMap<>(); 
		pg = (Map) param.get("pg");
		param = (Map) param.get("param");
		return output(recordsService.toView(param,pg));
	}
}
