package com.differ.microservice.controller;

import java.util.Map;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.SubsidyService;

@RestController
@RequestMapping("/subsidy")
public class SubsidyController extends BaseSupport {

	@Autowired
	private SubsidyService subService;
	
	/**
	 * 新增或更新政府补贴详情信息
	 * @param req
	 * @return
	 */
	@RequestMapping("/save")
	@ResponseBody
	public String setUpdateSubsidy(HttpServletRequest req){
		Map param = getInputJSON(req);
		return output(subService.saveSubsidy(param));
	}
}
