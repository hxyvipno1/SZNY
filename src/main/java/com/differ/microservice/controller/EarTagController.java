/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.EarTagService;
import com.differ.microservice.service.FarmerService;

/** 
 * 耳标信息Controller层
 * @author yue.li
 * @version $Id: EarTagController.java, v 0.1 2017年5月16日 上午11:18:34 yue.li Exp $
 */
@RestController
@RequestMapping("/earTag")
public class EarTagController extends BaseSupport {
    
    @Autowired
    private EarTagService earTagService;
    
    /**
	 * 根据idx查询信息
	 * @param idx
	 * @return
	 */
    @RequestMapping("/query/{id}")
    @ResponseBody
    public String getListByIdx( @PathVariable String id){
        return output(earTagService.queryById(id));
    }
    /**
     * 查询所有信息
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public String getList(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
        return output(earTagService.query(param));
    }
    
    @RequestMapping("/batch")
    @ResponseBody
    public String List(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
        return output(earTagService.bacth(param));
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @ResponseBody
    public String update(Map earTagEntity) {
		return output(earTagService.update(earTagEntity));
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public Map delete(HttpServletRequest req) {
    	Map param=this.getInputJSON(req);
    	return earTagService.delete(param);
	 
    }
    
    @RequestMapping(value = "/add")
    @ResponseBody
    public String add(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
		return output(earTagService.add(param));
    }
  
    @RequestMapping(value = "/addbatch")
    @ResponseBody
    public String bachadd(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
		return output(earTagService.bachadd(param));
    }
    
    
    @RequestMapping(value = "/checksn")
    @ResponseBody
    public String checksn(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
		return output(earTagService.checksn(param));
    }
    
    @RequestMapping(value = "/grant")
    @ResponseBody
    public String grant(HttpServletRequest req){
    	Map param=this.getInputJSON(req);
		return output(earTagService.grant(param));
    }
    
  
}
