/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.OperatorsService;

/**
 * 运营商管理
 * @author zhenshan.xu
 * @version $Id: OperatorsController.java, v 0.1 2017年8月23日 下午7:00:27 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/operators")
public class OperatorsController extends BaseSupport{
	
	@Autowired
	private OperatorsService operatorsService;

	/**
	 * 初始运营商列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/init")
	@ResponseBody
	public String List(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(operatorsService.findAll(param));

	}
	/**
	 * 根据idx查询运营商
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryByIdx")
	@ResponseBody
	public String getListByIdx(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(operatorsService.queryByIdx(param));
	}
	/**
	 * 查询运营商名称
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryName")
    @ResponseBody
    public String queryName(HttpServletRequest req){
        
    Map param=getInputJSON(req);
    return output(operatorsService.queryName(param));
    
    }
	/**
	 * 删除
	 * @param req
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(HttpServletRequest req) {

		Map param = getInputJSON(req);
		return output(operatorsService.delete(param));

	}
	
	/**
	 * 运营商树拖拽
	 * @param req
	 * @return
	 */
	@RequestMapping("/updateTree")
	@ResponseBody
	public String updateTree(HttpServletRequest req){
		Map param = getInputJSON(req);
		return output(operatorsService.updateTree(param));
	}
}
