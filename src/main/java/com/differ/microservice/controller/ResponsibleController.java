/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.ResponsibleService;

/**
 * 专管人员
 * 
 * @author zhenshan.xu
 * @version $Id: ResponsibleController.java, v 0.1 2017年9月3日 上午9:30:51
 *          zhenshan.xu Exp $
 */

@RestController
@RequestMapping("/administer")
public class ResponsibleController extends BaseSupport {

	@Autowired
	private ResponsibleService responsibleService;

	/**
	 * 初始化专管人员列表
	 * @param req
	 * @return
	 */

	@RequestMapping("/init")
	@ResponseBody
	public String getAllStaff(HttpServletRequest req) {
		
	Map param = getInputJSON(req);
	return output(responsibleService.findAll(param));
	}
	@RequestMapping("/add")
	@ResponseBody
	public String getAddStaff(HttpServletRequest req) {
		
	Map param = getInputJSON(req);
	return output(responsibleService.add(param));
	}
	/**
	 * 查询专管人员
	 * @param req
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public String getStaff(HttpServletRequest req) {
		
	Map param = getInputJSON(req);
	return output(responsibleService.findAll(param));
	}
	
}
