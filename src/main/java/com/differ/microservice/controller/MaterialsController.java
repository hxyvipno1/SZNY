/**
 * 
 */
package com.differ.microservice.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.MaterialsService;

/**
 * 物质管理
 * @author zhenshan.xu
 * @version $Id: MaterialsController.java, v 0.1 2017年8月29日 下午7:24:15 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/materials")
public class MaterialsController extends BaseSupport{
	
	@Autowired
	private MaterialsService materialsService;
	
	
	  @RequestMapping("/query")
	    @ResponseBody
	    public String getAllNode(){
	        return output(materialsService.findAll());
	    }
	    
	    @RequestMapping("/queryByIdx")
	    @ResponseBody
	    public String getNodeByIdx(HttpServletRequest req){
	        Map param=getInputJSON(req);
	        return output(materialsService.findByIdx(param));
	    }
	 
	    
	    @RequestMapping("/queryByPid")
	    @ResponseBody
	    public String getNodeByPid(HttpServletRequest req){
	        Map param=getInputJSON(req);
	        return output(materialsService.findByPid(param));
	    }
	@RequestMapping("/queryByCode")
	@ResponseBody
	    public Map getNodeByCode(HttpServletRequest req){
	        String nodeCode = req.getParameter("code");
	        Map param= new HashMap<>();
	        param.put("node_code", nodeCode);
	        return materialsService.findByCode(param);
	    }
	/**
	 *根据idx删除所选物资及其子节点
	 * @param req
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(HttpServletRequest req) {

		Map param = getInputJSON(req);
		return output(materialsService.delete(param));

	}
	
}
