/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.WrapService;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: WrapController.java, v 0.1 2017年8月19日 下午4:59:56 dunfang.zhou Exp $
 */
@RestController
@RequestMapping("/wrap")
public class WrapController extends BaseSupport{
    @Autowired
    private WrapService  wrapService;
    
    /**
     * 
     * 查询所有兽医
     * @param req
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public String getAllWrap(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(wrapService.findAll(param));
    }
    /**
     * 
     * 通过idx查询
     * @param req
     * @return
     */
    @RequestMapping("/queryByIdx")
    @ResponseBody
    public String getWrapByIdx(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(wrapService.findByIdx(param));
    }
    
    /**
     * 
     * 删除
     * @param req
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Map delete(HttpServletRequest req){
        Map param=getInputJSON(req);
        return wrapService.delete(param);
    }
}
