package com.differ.microservice.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.differ.microservice.core.dto.ResponseInfo;
import com.differ.microservice.core.spring.mvc.SingleThreadInstance;

@Controller
//@RequestMapping("/testlist")
public class TestController {

	@RequestMapping("/testlist")
	public ModelAndView list() {
		System.out.println(
				SingleThreadInstance.getInstance().get("uidx")
				);
		List<Map> messages =new ArrayList();
		Map row=new HashMap();
		row.put("code", "22222");
		row.put("message", "dddd");
			List list=new ArrayList();
			Map cell=new HashMap();
			cell.put("idx", "2222");
			
			list.add(cell);
			list.add(cell);
			list.add(cell);
		row.put("list", list);
		messages.add(row);
		//messages.isEmpty()
		return new ModelAndView("list", "messages", messages);
	}
}
