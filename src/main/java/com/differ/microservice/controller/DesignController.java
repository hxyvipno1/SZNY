package com.differ.microservice.controller;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.DesignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by cailm on 2017-07-04.
 */

@RestController
@RequestMapping("/dsgn")
public class DesignController  extends BaseSupport{

    @Autowired
    private DesignService service;

    @RequestMapping("/db/{idx}")
    @ResponseBody
    public String dbconnect( @PathVariable String idx){
        Boolean flg=service.dbconnect(idx);
        return output(flg);
    }

    @RequestMapping("/tbl/{idx}")
    @ResponseBody
    public String tblist( @PathVariable String idx){
        return output(service.tblist(idx));
    }

    @RequestMapping("/fld/{idx}/{tname}")
    @ResponseBody
    public String fldlist( @PathVariable String idx,@PathVariable String tname){
        return output(service.fldlist(idx,tname));
    }

    @RequestMapping("/dst/{idx}")
    @ResponseBody
    public String dstlist( @PathVariable String idx){
        return output(service.dstlist(idx));
    }

}
