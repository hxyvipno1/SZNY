package com.differ.microservice.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.CONSTANTS;
import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.core.service.StepService;
import com.differ.microservice.core.spring.SpringUtil;
import com.differ.microservice.core.util.DateUtil;
import com.differ.microservice.core.util.JSONParser;
import com.differ.microservice.service.CmmService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class CmmController extends BaseSupport {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CmmService service;
	
	@Autowired
    private StepService step;
	
	@RequestMapping("/pages/therm")
	@ResponseBody
	public String thermometer(
			@RequestParam("serialno") String serialno,
			@RequestParam("rule") String rule,
			@RequestParam("pno") String pno,
			@RequestParam("max") String max,
			@RequestParam("min") String min,
			@RequestParam("avg") String avg,
			@RequestParam("diff") String diff
		){
		
		Map param=new HashMap();
		param.put("serialno", serialno);
		param.put("rule", rule);
		param.put("pno", pno);
		param.put("max", max);
		param.put("min", min);
		param.put("avg", avg);
		param.put("diff", diff);
		
		service.thermometer(param);	
		return "success";
	}
	
	@RequestMapping("/pages/thermfile")
	@ResponseBody
	public String thermfile(
			@RequestParam("serialno") String serialno,
			HttpServletRequest request){		
		String path=SpringUtil.getProp("spring.application.upload_path")+"/therm/";
		String name=serialno+DateUtil.format(new Date(), "yyMMddHHmm")+".jpg";
		
		//FileUtils.copyInputStreamToFile(request.getInputStream(), new File(path,name));
		Map pmp=new HashMap();
		String sql="select * from t_file where file_name=:name";
		pmp.put("name", name);
		Map row=step.getDao().loadRowByMap(sql, pmp);
		if(row!=null)
			return "exist";
		
		int bytesRead=0;
        int bytesWritten = 0;
		try {
            InputStream in = request.getInputStream();
            byte[] bytes = new byte[2048];
            

            OutputStream  outputStream = new FileOutputStream(path+"/"+name);
            while ((bytesRead = in.read(bytes) ) != -1 ) {
            	 outputStream.write(bytes, 0, bytesRead);
            	 outputStream.flush();
            	 bytesWritten+=bytesRead;
            }	            
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
			
		Map entity=new HashMap();
		entity.put("file_name", name);
    	entity.put("file_path", "/therm/");
    	entity.put("upload_type", "zny");
    	entity.put("file_type", "image");
    	entity.put("file_size", bytesWritten);
    	step.addEntity("t_file", entity);
    	
		Map param=new HashMap();
		param.put("serialno", serialno);
		param.put("attach_idx", entity.get("idx"));
		service.thermfile(param);	
		return "success";
	}
	
	
	@RequestMapping("/deviceinfo")
	@ResponseBody
	public String deviceinfo(HttpServletRequest req){
		Map pmp=this.getInputJSON(req);
		return output(service.deviceinfo(pmp));
	}
	
	
	//根据识读器传过来的用户id返回县级的id和所在县以乡镇为节点的区域树 
	@RequestMapping("/pages/beast/comm!getXzTreeByUserId.action")
	@ResponseBody
	public String xztree(
			@RequestParam("id") String info_code,
			@RequestParam("resType") String type){
		
		
		//return "{'xjId':102790, 'tree':'[{'id':136960,'text':衢州市辖区规模场组,xx},{'id':136978,'text':东港开发区,end}], 'defaultZ':136960}";
		return JSONParser.obj2Json(service.xztree(info_code));
	}
	
	//AT+DNT8  id=1;2;3;4;5	下载资源数据
	@RequestMapping("/pages/beast/comm!DNT8.action")
	@ResponseBody
	public String t8(
			@RequestParam("id") String id){
logger.debug(id);		
		String rtn=JSONParser.obj2Json(service.t8(id));
logger.debug(rtn);
		return rtn;
	}
	
	//AT+DNT9		下载首页信息
	@RequestMapping("/pages/beast/comm!DNT9.action")
	@ResponseBody
	public String t9(
			@RequestParam("id") String id,
			@RequestParam("resType") String type){
logger.debug(id);		
logger.debug("resType", type);
//		String rtn=JSONParser.obj2Json(service.t9(id));

//		logger.info(rtn);
//		规模场无需返回数据
		if(true) return "{'total':0,'length':0,'content':''}";
		return "{'total':1,'length':128,'content':'000000M段志辉　　　　　00000000000000000000000000000000000000100311胡小军　                                                     '}";
	}
	
	//AT+DND0 id=496	规模场统计信息数据库  id 区域id
	@RequestMapping("/pages/beast/comm!DND0.action")
	@ResponseBody
	public String d0(@RequestParam("id") String id){
logger.debug(id);		
		//if(true)return "{'total':1,'length':128,'content':'123                                                                                                                         '}";
		String rtn=JSONParser.obj2Json(service.d0(id));
logger.debug(rtn);
		
		return rtn;
	}
	
	//AT+DND5 id=26171 传入选择的镇，下载该镇下兽医管理村的表格
	@RequestMapping("/pages/beast/comm!DND1.action")
	@ResponseBody
	public String d1(@RequestParam("id") String id){
		//if(true)return "{'total':1,'length':128,'content':'000000M段志辉　　　　　00000000000000000000000000000000000000100311胡小军　                                                     '}";
		if(true) return "{'total':0,'length':0,'content':''}";
logger.debug("1:"+id);		
		return "{'total':1,'length':128,'content':'   678                                                                                                                      '}";
	}
	//AT+DND2 	id=125119,12409	传入选择的村，下载核查使用的散户信息数据库
	@RequestMapping("/pages/beast/comm!DND2.action")
	@ResponseBody
	public String d2(@RequestParam("id") String id){
		//if(true)return "{'total':1,'length':128,'content':'000000M段志辉　　　　　00000000000000000000000000000000000000100311胡小军　                                                     '}";
		if(true) return "{'total':0,'length':0,'content':''}";
logger.debug("2:"+id);		
		return "{'total':1,'length':128,'content':'   678                                                                                                                      '}";
	}
	
	//村下的散户信息(B8)
	@RequestMapping("/pages/beast/comm!DND3.action")
	@ResponseBody
	public String d3(@RequestParam("id") String id){
		//if(true)return "{'total':1,'length':128,'content':'000000M段志辉　　　　　00000000000000000000000000000000000000100311胡小军　                                                     '}";
		if(true) return "{'total':0,'length':0,'content':''}";
logger.debug("3:"+id);		
		return "{'total':1,'length':128,'content':'   678                                                                                                                      '}";

	}
	//县级单位下的授精站、公猪信息(B10)
	@RequestMapping("/pages/beast/comm!DND4.action")
	@ResponseBody
	public String d4(@RequestParam("id") String id){
		//if(true)return "{'total':1,'length':128,'content':'000000M段志辉　　　　　00000000000000000000000000000000000000100311胡小军　                                                     '}";
		if(true) return "{'total':0,'length':0,'content':''}";
logger.debug("4:"+id);		
		return "{'total':1,'length':128,'content':'   678                                                                                                                      '}";

	}
	
	//母猪详细信息数据库，参数如 id=11540,33,42322
	@RequestMapping("/pages/beast/comm!DNL1.action")
	@ResponseBody
	public String dnl1(@RequestParam("id") String id){
		if(false)return "{'total':0,'length':0,'content':''}";
logger.debug("l1:"+id);		
String rtn=JSONParser.obj2Json(service.dnl1(id));
logger.debug(rtn);
		return rtn;
		// return "{'total':1,'length':128,'content':'   678                                                                                                                      '}";

	}
}
