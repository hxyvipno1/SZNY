/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.VeterinaryDrugsService;

/**
 * 兽药企业信息管理
 * @author zhenshan.xu
 * @version $Id: VeterinaryDrugsController.java, v 0.1 2017年8月26日 上午10:22:59 zhenshan.xu Exp $
 */

@RestController
@RequestMapping("/drugs")
public class VeterinaryDrugsController extends BaseSupport{
	
	@Autowired
	private VeterinaryDrugsService drugsService;
	/**
	 * 初始化兽药企业列表
	 * @param req
	 * @return
	 */
	@RequestMapping("/init")
	@ResponseBody
	public String List(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(drugsService.findAll(param));

	}
	
	@RequestMapping("/query")
	@ResponseBody
	public String query(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(drugsService.find(param));

	}
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(drugsService.delete(param));

	}

	   /**
     * 列表区域下拉框联动查询
     * @param req
     * @return
     */
    @RequestMapping("/queryCity")
    @ResponseBody
    public String getDistrictList(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(drugsService.query(param));
    }
}
