/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.InsuranceService;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: Insurance.java, v 0.1 2017年8月20日 下午3:28:30 dunfang.zhou Exp $
 */
@RestController
@RequestMapping("/insurance")
public class InsuranceController extends BaseSupport{
    
    @Autowired
    private InsuranceService insuranceService;
    
    /**
     * 查询所有银行
     * @param req
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public String List(HttpServletRequest req){
        
    Map param=getInputJSON(req);
    return output(insuranceService.findAll(param));
    
    }
    
    @RequestMapping("/queryByIdx")
    @ResponseBody
    public String queryByIdx(HttpServletRequest req){
        
    Map param=getInputJSON(req);
    return output(insuranceService.findByIdx(param));
    
    }
    
    @RequestMapping("/queryName")
    @ResponseBody
    public String queryName(HttpServletRequest req){
        
    Map param=getInputJSON(req);
    return output(insuranceService.queryName(param));
    
    }
    @RequestMapping("/delete")
    @ResponseBody
    public Map delete(HttpServletRequest req){
        
    Map param=getInputJSON(req);
    return insuranceService.delete(param);
    
    }
    
	/**
	 * 保险树拖拽
	 * @param req
	 * @return
	 */
	@RequestMapping("/updateTree")
	@ResponseBody
	public String updateTree(HttpServletRequest req){
		Map param = getInputJSON(req);
		return output(insuranceService.updateTree(param));
	}

}
