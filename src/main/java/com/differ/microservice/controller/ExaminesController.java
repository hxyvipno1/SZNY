/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.ExaminesService;

/**
 * 审核
 * @author zhenshan.xu
 * @version $Id: ExaminesController.java, v 0.1 2017年10月30日 上午10:08:11 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/examines")
public class ExaminesController extends BaseSupport{
	
	@Autowired
	private ExaminesService examinesService;
	/**
	 * 加载审核人
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryPeople")
	@ResponseBody
	public String getPeople(HttpServletRequest req) {
		
		Map param = getInputJSON(req);
		
		return output(examinesService.people(param));
	}
	
	/**
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public String getQuery(HttpServletRequest req) {
		
		Map param = getInputJSON(req);
		
		return output(examinesService.query(param));
	}
	
	/**
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/reply")
	@ResponseBody
	public String getReply(HttpServletRequest req) {
		
		Map param = getInputJSON(req);
		
		return output(examinesService.reply(param));
	}
	
}
