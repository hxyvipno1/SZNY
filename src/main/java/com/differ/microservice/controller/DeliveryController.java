/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.DeliveryService;

/**
 * 投递
 * @author zhenshan.xu
 * @version $Id: DeliveryController.java, v 0.1 2017年10月27日 上午10:30:42 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/deliver")
public class DeliveryController extends BaseSupport{
	@Autowired
	private	DeliveryService	deliveryService;
	
	//物资投递
	@RequestMapping("/send")
	@ResponseBody
	public String getSend(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(deliveryService.send(param));
	}
}
