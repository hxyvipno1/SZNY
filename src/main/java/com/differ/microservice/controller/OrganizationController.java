/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.NodeService;
import com.differ.microservice.service.OrganizationService;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: OrganizationController.java, v 0.1 2017年8月2日 上午8:59:52 dunfang.zhou Exp $
 */
@RestController
@RequestMapping("/organization")
public class OrganizationController extends BaseSupport{
    @Autowired
    private OrganizationService  organizationService;
    
    /**
     * 
     * 查询所有节点
     * @param req
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public String getAllOrganization(){
        return output(organizationService.findAll());
    }
    
    @RequestMapping("/queryByIdx")
    @ResponseBody
    public String getOrganizationByIdx(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(organizationService.findByIdx(param));
    }
 
    
    @RequestMapping("/queryByPid")
    @ResponseBody
    public String getOrganizationByPid(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(organizationService.findByPid(param));
    }
    @RequestMapping("/queryByPpid")
    @ResponseBody
    public String queryByPpid(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(organizationService.queryByPpid(param));
    }
    /**
     * 删除
     * @param idx
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
  public Map delete(HttpServletRequest req) {
      Map param=getInputJSON(req);
    return organizationService.delete(param);
      
  }
    
    
    /**
     * 删除
     * @param idx
     * @return
     */
    @RequestMapping("/deletePeople")
    @ResponseBody
  public void deletePeople(HttpServletRequest req) {
      Map param=getInputJSON(req);
     organizationService.deletePeople(param);
      
  }
}
