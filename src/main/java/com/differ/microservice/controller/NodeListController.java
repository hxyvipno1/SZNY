/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.controller;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.NodeListService;

/**
 * 业务字典
 * @author dunfang.zhou
 * @version $Id: NodeController.java, v 0.1 2017年7月28日 上午9:19:09 dunfang.zhou Exp $
 */
@RestController
@RequestMapping("/nodeList")
public class NodeListController extends BaseSupport{
 
    @Autowired
    private NodeListService  nodeListService;
    
    /**
     * 
     * 查询所有节点
     * @param req
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public String getAllNode(){
        return output(nodeListService.findAll());
    }
    
    @RequestMapping("/queryByIdx")
    @ResponseBody
    public String getNodeByIdx(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(nodeListService.findByIdx(param));
    }
 
    
    @RequestMapping("/queryByCode")
    @ResponseBody
    public Map getNodeByCode(HttpServletRequest req){
        String nodeCode = req.getParameter("node_code");
        Map param= new HashMap<>();
        param.put("node_code", nodeCode);
        return nodeListService.findByCode(param);
    }
    @RequestMapping("/queryByPid")
    @ResponseBody
    public String getNodeByPid(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(nodeListService.findByPid(param));
    }
    
    /**
     * 删除
     * @param idx
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
  public Map delete(HttpServletRequest req) {
      Map param=getInputJSON(req);
      Map del = nodeListService.delete(param);
    return del;
      
  }
    
    /**
     * 查询附件
     * @param req
     * @return
     */
    @RequestMapping("/queryChild")
    @ResponseBody
    public String getChild(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(nodeListService.findChild(param));
    }
}
