/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.IssueService;

/**
 * 发放物资
 * @author zhenshan.xu
 * @version $Id: IssueController.java, v 0.1 2017年10月22日 上午10:40:57 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/Issue")
public class IssueController extends BaseSupport{
	
	@Autowired
	private IssueService issueService;
	/**
	 * 搜索负责人
	 * @param req
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public String getCustomer(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(issueService.findAll(param));
	}
	
	/**
	 * 搜索负责人详情
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryDetail")
	@ResponseBody
	public String getDetail(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(issueService.detail(param));
	}
	
	/**
	 * 物资发放
	 * @param req
	 * @return
	 */
	@RequestMapping("/distribute")
	@ResponseBody
	public String getDistribute(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(issueService.distribute(param));
	}

	/**
	 * 物资发放
	 * @param req
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public String getUpdate(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(issueService.update(param));
	}
}
