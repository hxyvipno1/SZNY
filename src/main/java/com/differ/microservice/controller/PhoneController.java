/**
 * 
 */
package com.differ.microservice.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.model.Phone;
import com.differ.microservice.model.Reader;
import com.differ.microservice.service.PhoneService;
import com.differ.microservice.service.ReaderService;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: MobilePhoneController.java, v 0.1 2017年7月18日 下午2:52:52 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/phone")
public class PhoneController extends BaseSupport{
	
	 @Autowired
	 private PhoneService phoneService;
	 
	 /**
		 * 根据idx查询信息
		 * @param idx
		 * @return
		 */
	    @RequestMapping("/query/{idx}")
	    @ResponseBody
	    public String getListByIdx( @PathVariable String idx){
	        return output(phoneService.queryByIdx(idx));
	    }
	    /**
	     * 查询所有信息
	     * @return
	     */
	    @RequestMapping("/query")
	    @ResponseBody
	    public String getList(HttpServletRequest req){
	    	Map param=this.getInputJSON(req);
	        return output(phoneService.query(param));
	    }
	    /**
	     * 修改
	     * @param req
	     * @return
	     */
	    @RequestMapping("/update")
	    @ResponseBody
	    public String update(HttpServletRequest req){
	    	Map param=this.getInputJSON(req);
			return output(phoneService.update(param));
	    	
	    }
	    /**
	     * 添加手机信息
	     * @param brand
	     * @param model
	     * @param number
	     * @param price
	     * @return
	     */
	    @RequestMapping("/add")
	    @ResponseBody
	    public String add(HttpServletRequest req){
	    	Map param=this.getInputJSON(req);
			return output(phoneService.add(param));
	    }
	    /**
	     *领取
	     * @return
	     */
	    @RequestMapping("/grant")
	    @ResponseBody
	    public String receive(HttpServletRequest req){
	    	
	    	Map param=this.getInputJSON(req);
	    	
	    	return output(phoneService.receive(param));
	    }
}
