/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.controller;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.DictionaryService;

/**
 * 业务字典
 * @author dunfang.zhou
 * @version $Id: NodeController.java, v 0.1 2017年7月28日 上午9:19:09 dunfang.zhou Exp $
 */
@RestController
@RequestMapping("/dictionary")
public class DictionaryController extends BaseSupport{
 
    @Autowired
    private DictionaryService  dictionaryService;
    
    /**
     * 
     * 查询所有节点
     * @param req
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public String getAllNode(){
        return output(dictionaryService.findAll());
    }
    
    @RequestMapping("/queryByIdx")
    @ResponseBody
    public String getDictionaryByIdx(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(dictionaryService.findByIdx(param));
    }
 
    
    @RequestMapping("/queryByPid")
    @ResponseBody
    public String getDictionaryByPid(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(dictionaryService.findByPid(param));
    }
    
    @RequestMapping("/queryByType")
    @ResponseBody
    public String getByType(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(dictionaryService.findByType(param));
    }
    
    @RequestMapping("/queryByCode")
    @ResponseBody
    public Map getDictionaryByCode(HttpServletRequest req){
        String nodeCode = req.getParameter("code");
        String pid = req.getParameter("pid");
        Map param= new HashMap<>();
        param.put("code", nodeCode);
        param.put("pid", pid);
        return dictionaryService.findByCode(param);
    }
    
    @RequestMapping("/queryByName")
    @ResponseBody
    public Map getDictionaryByName(HttpServletRequest req){
        String nodeName = req.getParameter("name");
        String pid = req.getParameter("pid");
        Map param= new HashMap<>();
        param.put("name", nodeName);
        param.put("pid", pid);
        return dictionaryService.findByName(param);
    }
    
    @RequestMapping("/queryCode")
    @ResponseBody
    public String getByCode(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(dictionaryService.findByCode1(param));
    }
    /**
     * 删除
     * @param idx
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
  public Map delete(HttpServletRequest req) {
      Map param=getInputJSON(req);
      Map del = dictionaryService.delete(param);
    return del;
      
  }
    
    /**
     * 查询附件
     * @param req
     * @return
     */
    @RequestMapping("/queryChild")
    @ResponseBody
    public String getChild(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(dictionaryService.findChild(param));
    }
}
