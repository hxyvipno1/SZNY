/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.DistrictService;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: DistrictController.java, v 0.1 2017年8月15日 下午2:54:26 dunfang.zhou Exp $
 */
@RestController
@RequestMapping("/district")
public class DistrictController extends BaseSupport{

    @Autowired
    private DistrictService districtService;
    
    /**
     * 列表区域下拉框联动查询
     * @param req
     * @return
     */
    @RequestMapping("/queryCity")
    @ResponseBody
    public String getDistrictList(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(districtService.query(param));
    }
    
    
    @RequestMapping("/load")
    @ResponseBody
    public String load(@RequestParam(value = "idx", required = true)String idx){
        return output(districtService.load(idx));
    }
    
    /**
     * 区域查询上一级信息
     * @param req
     * @return
     */
    @RequestMapping("/queryCityPid")
    @ResponseBody
    public String getDistrictPidList(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(districtService.queryPid(param));
    }
    /**
     * 获取区划详情信息
     * @param req
     * @return
     */
    @RequestMapping("/queryDetails")
    @ResponseBody
    public String getDistrictDetails(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(districtService.queryDetails(param));
    }
    
    /**
     * 区域查询上一级信息
     * @param req
     * @return
     */
    @RequestMapping("/queryLeaf")
    @ResponseBody
    public String queryLeaf(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(districtService.queryLeaf(param));
    }
    
    /**
     * 区域查询上一级信息
     * @param req
     * @return
     */
    @RequestMapping("/queryByIdx")
    @ResponseBody
    public String queryByIdx(HttpServletRequest req){
        Map param=getInputJSON(req);
        return output(districtService.queryByIdx(param));
    }
    @RequestMapping("/queryPid")
    @ResponseBody
    public String queryByPid(HttpServletRequest req){
    	Map param = getInputJSON(req);
    	return output(districtService.queryDistrictPid(param));
    }
    /**
     * 获取区划详情信息(返回汉字)
     * @param req
     * @return
     */
    @RequestMapping("/queryDistrict")
    @ResponseBody
    public String getDistrictName(HttpServletRequest req){
        Map param=getInputJSON(req);
        String idx = param.get("district_idx").toString();
        return districtService.queryDistrict(idx);
    }
    
    /**
     * 查询浙江两级
     * @param req
     * @return
     */
    @RequestMapping("/queryZheJiang")
    @ResponseBody
    public String queryZheJiang(){
        return output(districtService.queryZheJiang());
    }
}
