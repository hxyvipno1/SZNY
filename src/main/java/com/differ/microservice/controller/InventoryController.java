/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.InventoryService;

/**
 * 库存记录,入库单
 * 
 * @author zhenshan.xu
 * @version $Id: InventoryController.java, v 0.1 2017年9月3日 下午2:54:22 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/inventory")
public class InventoryController extends BaseSupport {

	@Autowired
	private InventoryService inventory;

	/**
	 *  查询物资
	 * @param req
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public String query(HttpServletRequest req) {

		Map param = getInputJSON(req);

		return output(inventory.query(param));

	}

	/**
	 * 物资入库
	 * @param req
	 * @return
	 */
	@RequestMapping("/storage")
	@ResponseBody
	public String storage(HttpServletRequest req) {
		Map param = getInputJSON(req);
		return output(inventory.storage(param));

	}
	
}
