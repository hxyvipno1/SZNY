/**
 * 
 */
package com.differ.microservice.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.controller.BaseSupport;
import com.differ.microservice.service.CommunicationsService;

/**
 * 通讯人员
 * @author zhenshan.xu
 * @version $Id: CommunicationsController.java, v 0.1 2017年12月4日 上午11:04:17 zhenshan.xu Exp $
 */
@RestController
@RequestMapping("/communications")
public class CommunicationsController extends BaseSupport{
	

	@Autowired
	private CommunicationsService communicationsservice;
	
	/**
	 * 查询所有通讯人员 
	 * @param req
	 * @return
	 */
	@RequestMapping("/init")
	@ResponseBody
	public String List(HttpServletRequest req) {

		Map param = getInputJSON(req);
		
		return output(communicationsservice.findAll(param));

	}
}
