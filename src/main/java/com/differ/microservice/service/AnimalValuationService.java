/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: AnimalValuationService.java, v 0.1 2017年9月10日 上午10:51:34 zhenshan.xu Exp $
 */
@Service
public class AnimalValuationService {
	
	@Autowired
	private StepDao dao;
	@Autowired
	private DistrictService dis;
	
	public List find(Map param) {
		Map phone = new HashMap<>();
		phone.put("phone", param.get("code"));
		return dao.loadListByCode("LD_CHARGE_PEOPLE", phone);
	}
	 /**
     * 规模场估值
     * @param param
     * @return
     */
    public Map findDetails(Map param){
    	Map details =new HashMap<>();
    	details.put("details", 	dao.loadListByCode("LD_VALUATION", param));//企业工商登记信息、法人信息、基本信息
    	List list =  dao.loadListByCode("LD_ANIMAL_UNIT", param);
		List resultList = new ArrayList<>();
		for (Object object : list) {
			Map map = new HashMap<>();
			map = (Map) object;
			String disname =  dis.queryDistrict(map.get("district_idx").toString());
			map.put("district_name", disname);
			resultList.add(map);	
		}
		details.put("resultList",  resultList);//动物个体
		return  details;
	}
	
}
