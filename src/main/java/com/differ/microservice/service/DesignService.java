
package com.differ.microservice.service;

import com.differ.microservice.core.dao.DynamicDao;
import com.differ.microservice.core.dao.StepDao;
import com.differ.microservice.core.spring.bean.DataSourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cailm on 2017-07-04.
 */

@Service
public class DesignService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    
    
    @Autowired
    private StepDao dao;

    public Boolean dbconnect(String idx){
        Map row=dao.loadRT("sys_db_info",idx);
        
        return true;
    }

    public List tblist(String dbidx){
        Map row=dao.loadRT("sys_db_info",dbidx);
        
        return null;
    }

    public List fldlist(String dbidx,String tname){
        Map row=dao.loadRT("sys_db_info",dbidx);
        
        return null;
    }

    public List dstlist(String dbidx){
        Map param=new HashMap(); param.put("db_idx",dbidx);
          return dao.loadListByCode ("LD_DATASET",param);
    }

}
