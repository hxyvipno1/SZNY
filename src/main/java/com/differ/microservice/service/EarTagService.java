/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 耳标信息服务的实现
 * 
 * @author yue.li
 * @version $Id: EarTagServiceImpl.java, v 0.1 2017年5月16日 上午10:43:47 yue.li Exp $
 */
@Service
public class EarTagService {
    private final Logger  logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private StepDao       dao;

    @Autowired
    private FarmerService farmerService;
    
    public List<String> queryById(String id) {
        Map row = dao.loadRT("t_ear_tag", id);
        return new ArrayList<String>(row.values());
    }
    /**
     * 查询所有耳标信息
     * @param param
     * @return
     */
   public List query(Map param) {
        return dao.loadListByCode("LD_EAR_TAG", param);
    }
   public List bacth(Map param) {
       return dao.loadListByCode("LD_EAR_BATCH", param);
   }
   /**
    * 查询库存
    * @param param
    * @return
    */
   public List queryStock(Map param) {
       return dao.loadListByCode("LD_EAR_STOCK", param);
   }
   
    public Map add(Map param) {
        return dao.add("t_ear_tag", param);

    }
    /**
     * 批量添加耳标记录（入库）
     * @param param
     * @return
     */
    
    public Map bachadd(Map param) {
    	String str =  param.get("no_size").toString();
    	Integer count=Integer.valueOf(str);
    	String start_rfid_no=(String)param.get("start_rfid_no");
    	long end=new Long(start_rfid_no).longValue() + count.longValue()-1;
    	String end_rfid_no= new String(end+"");
    	
    	/*String start_qr_no=(String)param.get("start_qr_no");
    	long endno=new Long(start_qr_no).longValue() + count.longValue()-1;
    	String end_qr_no= new String(endno+"");*/
    	
    	Map map = new HashMap();
    	map.put("supplier", param.get("supplier"));
    	map.put("purchaser", param.get("purchaser"));
    	map.put("start_rfid_no", param.get("start_rfid_no"));
    	map.put("start_qr_no", param.get("start_qr_no"));
    	map.put("no_size",param.get("no_size"));
    	map.put("end_rfid_no", end_rfid_no);
    	/*map.put("end_qr_no", end_qr_no);*/
        return dao.add("t_ear_batch", map);

    }

    public Map update(Map entity) {
        return dao.update("t_ear_tag", entity);

    }
/**
 * 删除耳标发放记录及发放详情，同时修改耳标为未发放状态
 * @param param
 * @return
 */
    public Map delete(Map param) {
    	
    	String idx = (String)param.get("idx");
    	
    	Map ear=dao.excuteByCode("RM_ISSUE_RECORD", param);
    	
    	String terminal_no=(String) param.get("terminal_no");
    	String[] code = terminal_no.split("-");
    	String startno=code[0];
    	String endno=code[1];
    	Map mp = new HashMap();
    	mp.put("people_id", ""); 
    	mp.put("people_name", "");
    	mp.put("identity_no", "");
    	mp.put("startno", startno);
    	mp.put("endno", endno);
    	mp.put("status", 1);
    	dao.excuteByCode("UP_EAR_UPDATE", mp);
    	
    	Map detail = new HashMap();
    	detail.put("startno", startno);
    	detail.put("endno", endno);
    	detail.put("terminal_type", 1);
    	dao.excuteByCode("RM_TERMINAL", detail);
    	return ear;
    }
    
    public List checksn(Map param){
    	String str =  param.get("quantity").toString();
    	Integer count=Integer.valueOf(str)-1;
    	String startno=(String)param.get("terminal_no");
    	long end=new Long(startno).longValue() + count.longValue();
    	String endno= new String(end+"");
    	Map map = new HashMap();
    	map.put("startno", startno);
    	map.put("endno", endno);
    	map.put("status","1");
    	return dao.loadListByCode("LD_EAR_CHECKSN", map);
    }
    	
    public Map grant(Map param){
    	
    	String str =  param.get("quantity").toString();
    	Integer count=Integer.valueOf(str); 
    	String startno=(String)param.get("terminal_no");
    	long end=new Long(startno).longValue() + count.longValue()-1;
    	String endno= new String(end+"");
    	Map map = new HashMap();
    	map.put("quantity", count);
    	map.put("terminal_type", "1");
    	if(startno.equals(endno)){
    		map.put("terminal_no", startno);
    	}else{
    		map.put("terminal_no", startno+"-"+endno);
    	}
    	map.put("identity_no", (String)param.get("identity_no"));
    	map.put("people_idx", (String)param.get("idx"));
    	map.put("people_name", (String)param.get("name"));
    	map.put("phone", (String)param.get("phone"));
    	Map grant=dao.add("t_material_record", map);
    	
    	for(long i=Long.parseLong(startno);i<Long.parseLong(endno)+1;i++){
    		Map earTag= new HashMap();
    		earTag.put("record_idx", grant.get("idx"));
    		earTag.put("people_idx", param.get("idx"));
    		earTag.put("people_name", param.get("name"));
    		earTag.put("terminal_type", "1");
    		earTag.put("model", param.get("model"));
    		earTag.put("identity_no", param.get("identity_no"));
    		earTag.put("phone", param.get("phone"));
    		earTag.put("configuration", param.get("configuration"));
    		earTag.put("terminal_no", i);
    		dao.add("t_terminal", earTag);
    	}
    	
    	Map mp = new HashMap();
    	mp.put("people_id", (String)param.get("idx")); 
    	mp.put("people_name", (String)param.get("name"));
    	mp.put("identity_no", (String)param.get("identity_no"));
    	mp.put("startno", startno);
    	mp.put("endno", endno);
    	mp.put("status", 2);
    	Map result=dao.excuteByCode("UP_EAR_UPDATE", mp);
    	    	
    	return grant;
    }
}
