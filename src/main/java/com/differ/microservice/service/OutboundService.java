/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 出库
 * @author zhenshan.xu
 * @version $Id: OutboundService.java, v 0.1 2017年10月21日 上午10:35:39 zhenshan.xu Exp $
 */
@Service
public class OutboundService {
	
	@Autowired
	private StepDao dao;
	
	@Autowired
	private DistrictService dis;
	
	@Autowired
	private InventoryService inventory;

	/**
	 * 查询信息
	 */
	public List findAll(Map param) {

		return dao.loadListByCode("LD_RECORDS", param);

	}

	
	/**
	 * 领用人
	 */
	public List findPeople(Map param) {
		List list= dao.loadListByCode("LD_RECIPIENT", param);
		List resultList = new ArrayList<>();
		for (Object object : list) {
			Map map = new HashMap<>();
			map = (Map) object;
			String working_area =  dis.DistrictName(map.get("district_name").toString());
			map.put("district_name", working_area);
			resultList.add(map);
		}
		return resultList;
	}
	/**
	 *出库记录录入
	 * 
	 * @param param
	 * @return
	 */
	public Map outbound(Map param) {
		
		int size = 0;
		StringBuffer product_name = new StringBuffer();
		List result = (List) param.get("product_lists");
		for (int i = 0; i < result.size(); i++) {
			Map element = (Map) result.get(i);
			String main_idx = element.get("product_name").toString();
			if (main_idx ==null || main_idx.equals("")){
				continue;
			}
				Map idx = new HashMap<>();
			idx.put("idx", main_idx);
			int quantity = (int) element.get("quantity");
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			int stock = inventory.inventoryNum(product);
			if(stock>=quantity){
				size=size+1;
			}else{
				Map map = new HashMap<>();
				//库存剩余量
				map.put("number","库存剩余 ："+stock);
				//库存不足出库数量的物资产品名称
				product_name = product_name.append(product.get("product_name").toString());
				//物资产品名称与库存量拼接在一起复制到product_name中
				product_name = product_name.append(map.get("number").toString()+",");
				continue;
			}
		}
		Map outbound = new HashMap<>();
		if(size==result.size()){
		// 保存到 t_outbound 表;
		outbound = dao.add("t_outbound", param);
		//List result = (List) param.get("product_lists");

		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
			String main_idx = element.get("product_name").toString();
			// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
			if (main_idx == null || main_idx.equals("")) {
				continue;
			}
			Map idx = new HashMap<>();
			// 赋值main_idx到idx中;
			idx.put("idx", main_idx);
			// 根据main_idx查询产品信息
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			element.put("main_idx", outbound.get("idx"));
			// 获取物资的idx
			element.put("product_idx", product.get("idx"));
			element.put("people_idx",  param.get("idx"));
			element.put("product_name", product.get("product_name"));
			Map parameter = new HashMap<>();
			parameter.put("quantity", element.get("quantity"));
			parameter.put("line_abstracts1", element.get("line_abstracts1"));
			String endNo=inventory.computations(parameter);
			element.put("line_abstracts1",endNo);
			// 保存物资信息
			Map map = dao.add("t_outbound_detail", element);
			// 计算当前结余量
			int stock = inventory.inventoryNum(product);
			element.put("surplus_num", stock);
			// 获取时间
			element.put("occur_time", param.get("occur_time"));
			// 保存状态:出库
			element.put("type", "2");
			element.put("people_idx",  param.get("people_id"));
			element.put("document_num",  param.get("document_num"));
			element.put("residential_site", param.get("district_name"));
			element.put("abstracts", param.get("abstracts"));
			dao.add("t_statistics", element);
			}
				outbound.put("code", 1);
				outbound.put("message", "操作成功");
			}else{
				outbound.put("code", 0);
				outbound.put("message", "操作失败");
				outbound.put("product_name",product_name.substring(0,product_name.length()-1));
			}
		
		return outbound;
	}
	/**
	 * 查看详情
	 * @param param
	 * @return
	 */
	public List details(Map param){
		Map idx = new HashMap<>();
		List list = new ArrayList<>();
		idx.put("main_idx", param.get("main_idx"));
		List resultList = dao.loadListByCode("LD_OUTBOUND", idx);
		List product_name = dao.loadListByCode("LD_PRODUCT_NAME", param);
		for (Object result : resultList) { 
			Map details = new HashMap<>();
			details.put("nameArr", product_name);
			details.put("value", ((Map) result).get("product_idx"));
			((Map) result).put("product_name",details);
			list.add(result);
		}
		return list;
	}
	
	
	/**
	 * 修改物资出库
	 * 
	 * @param param
	 * @return
	 */
	public Map update(Map param) {
		
		int size = 0;
		StringBuffer product_name = new StringBuffer();
		List result = (List) param.get("product_lists");
		for (int i = 0; i < result.size(); i++) {
			Map element = (Map) result.get(i);
			String main_idx = element.get("product_name").toString();
			if (main_idx ==null || main_idx.equals("")){
				continue;
			}
				Map idx = new HashMap<>();
			idx.put("idx", main_idx);
			int quantity = (int) element.get("quantity");
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			int stock = inventory.inventoryNum(product);
			if(stock>=quantity){
				size=size+1;
			}else{
				Map map = new HashMap<>();
				map.put("number","库存剩余 ："+stock);
				product_name = product_name.append(product.get("product_name").toString());
				product_name = product_name.append(map.get("number").toString()+",");
				continue;
			}
		}
		//获取main_idx 到 idx 进行修改 t_outbound
		param.put("idx", param.get("main_idx"));
		Map outbound = dao.update("t_outbound", param);
		Map mainIdx = new HashMap<>();
		//通过main_idx删除物资详情列表信息
		mainIdx.put("main_idx", param.get("main_idx"));
		dao.excuteByCode("RM_OUT_DETAIL", mainIdx);
		
		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
			String main_idx = element.get("product_name").toString();
			// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
			if (main_idx == null || main_idx.equals("")) {
				continue;
			}
			Map idx = new HashMap<>();
			//赋值main_idx到idx中;
			idx.put("idx", main_idx);
			//根据main_idx查询产品信息
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			element.put("main_idx", outbound.get("idx"));
			//获取物资的idx
			element.put("product_idx", product.get("idx"));
			element.put("product_name", product.get("product_name"));
			//保存物资信息
			Map map = dao.add("t_outbound_detail", element);
			//计算当前结余量
			int stock = inventory.inventoryNum(product);
			element.put("surplus_num", stock);
			element.put("idx", param.get("rowIdx"));
			element.put("surplus_num", stock);
			//获取时间
			element.put("occur_time",param.get("occur_time"));
			//保存状态:出库
			element.put("type", "2");
			element.put("abstracts", param.get("abstracts"));
			//修改 t_statistics
			dao.update("t_statistics", element);
		}
		
		return outbound;
	}
}
