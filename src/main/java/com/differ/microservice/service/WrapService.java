/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: NodeService.java, v 0.1 2017年7月28日 上午9:20:05 dunfang.zhou Exp $
 */
@Service
public class WrapService {
    
 private final Logger logger = LoggerFactory.getLogger(this.getClass());

    
    
    @Autowired
    private StepDao dao;
    
    /**
     * 查询全部
     */
    public List findAll(Map param){
        
        return dao.loadListByCode("LD_ALLWRAP", param);
        
    }
    
    /**
     * 通过idx查询节点
     */
    public List findByIdx(Map param){
        
        return dao.loadListByCode("LD_WRAP", param);
        
    }

    
    /**
     *删除
     */
    public Map delete(Map param){
        Map map = new HashMap<>();
        List list = dao.loadListByCode("LD_ALLWRAP", param);
        dao.remove("t_wrap", param);
        List delList = dao.loadListByCode("LD_ALLWRAP", param);
        if(list.size()!=delList.size()){
            map.put("message", "操作成功");
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("message", "操作失败");
        return map;
        
    }
    
    
  
    
}
