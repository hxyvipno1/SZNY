/**
 * 
 */
package com.differ.microservice.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;
import com.differ.microservice.core.util.DateTimeUtil;

/**
 * 查看记录，出入库单
 * 
 * @author zhenshan.xu
 * @version $Id: RecordsService.java, v 0.1 2017年7月29日 上午9:25:14 zhenshan.xu Exp
 *          $
 */
@Service
public class RecordsService {

	@Autowired
	private StepDao dao;
	@Autowired
	private InventoryService inventory;// 入库的Service
	@Autowired
	private OutboundService outboundService;// 出库Service
	@Autowired
	private ApplyForService applyFor;// 申请Service
	@Autowired
	private IssueService issueService;// 发放Service
	@Autowired
	private DeliveryService deliveryService;// 投递Service
	@Autowired
	private TransfersService transfersService;// 调拨Service
	@Autowired
	private ReturnGoodsService returngoods;// 退货Service

	@Autowired
	private DistrictService dis;

	/**
	 * 初始化发放记录信息
	 */
	public Map findAll(Map param, Map pg) throws ParseException {
		Map results = new HashMap<>();
		//自定义String 类型documentNum默认为空；
		String documentNum = "";
		String dateString = (String) param.get("document_num");
		if (dateString != null) {//判断dateString的值是否为空
			if (dateString.indexOf("-") != -1) {//判断dateString值中是否含有“-”字符
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date time = sdf.parse(dateString);//转换dateString为sdf格式的日期
				//将time转换为字符串并截取前六位字符；
				documentNum = DateTimeUtil.getTimerNoFormat(time).substring(0, 6);
			} else {
				documentNum = dateString;
			}
		}
		String working = (String) param.get("residential_site");
		if (working == null || working.equals("")) {
			if (documentNum == null || documentNum.equals("")) {
				results = dao.loadPage("LD_RECORDS", param, pg);
			} else {
				param.put("document_num", "%" + documentNum + "%");
				results = dao.loadPage("LD_RECORDS", param, pg);
			}
		} else {
			String uid_end = working.substring(1, working.length() - 1);
			String working_area = dis.DistrictName(uid_end.toString());
			param.put("residential_site", "%" + working_area + "%");
			param.put("document_num", "%" + documentNum + "%");
			results = dao.loadPage("LD_RECORDS", param, pg);
		}
		return results;
	}

	/**
	 * 查询条件查询
	 * 
	 * @param param
	 * @return
	 */
	public Map find(Map param, Map pg) {
		String product_name = (String) param.get("product_name");
		String type = (String) param.get("type");
		if (product_name == null || product_name.equals("0")) {
			param.remove("product_name");
		}
		if (type == null || type.equals("0")) {
			param.remove("type");
		}
		return dao.loadPage("LD_RECORDS", param, pg);
	}

	/**
	 * 查询详情
	 */
	public List queryByIdx(Map param) {
		List result = new ArrayList<>(); // 根据param的idx获取信息
		String type = new String(); Map idx = new HashMap<>();
		Map map = dao.loadRowByCode("LD_RECORDS", param);
		// 获取map 的type
		if ( map==null ||map.equals("")) {
			idx.put("main_idx", param.get("idx"));
			type = (String) param.get("type");
		} else {
			type = (String) map.get("type");
			idx.put("main_idx", map.get("main_idx"));
		}
		if (type.equals("1")) {// 判断为入库信息
			List resultList = inventory.details(idx);
			result = (List) resultList;
		} else if (type.equals("2")) {// 判断为出库信息
			List resultList = outboundService.details(idx);
			result = (List) resultList;
		} else if (type.equals("3")) {// 判断为发放信息
			List resultList = issueService.details(idx);
			result = (List) resultList;
		} else if (type.equals("4")) {// 判断为申请信息
			List resultList = applyFor.details(idx);
			result = (List) resultList;
		} else if (type.equals("5")) {// 判断为审核信息
			List resultList = applyFor.details(idx);
			result = (List) resultList;
		} else if (type.equals("6")) {// 判断为投递信息
			List resultList = deliveryService.details(idx);
			result = (List) resultList;
		} else if (type.equals("45")) {// 判断为审核通过信息
			List resultList = applyFor.details(idx);
			result = (List) resultList;
		} else if (type.equals("54")) {// 判断为审核未通过信息
			List resultList = applyFor.details(idx);
			result = (List) resultList;
		} else if (type.equals("7")) {// 判断为调拨信息
			List resultList = transfersService.details(idx);
			result = (List) resultList;
		} else if (type.equals("8")) {// 判断为退货信息
			List resultList = returngoods.details(idx);
			result = (List) resultList;
		}
		return result;

	}

	public Map update(Map param) {
		Map result = new HashMap<>();
		// 根据param的idx获取信息
		Map idx = new HashMap<>();
		idx.put("idx", param.get("rowIdx"));
		Map map = dao.loadRowByCode("LD_RECORDS", idx);
		// 获取map 的type
		String type = map.get("type").toString();
		param.put("main_idx", map.get("main_idx"));
		if (type.equals("1")) {// 判断为入库信息
			Map resultList = (Map) inventory.update(param);
			result = resultList;
		} else if (type.equals("2")) {// 判断为出库信息
			Map resultList = (Map) outboundService.update(param);
			result = resultList;
		} else if (type.equals("3")) {// 判断为发放信息
			Map resultList = (Map) issueService.update(param);
			result = resultList;
		} else if (type.equals("4")) {// 判断为申请信息
			Map resultList = (Map) applyFor.update(param);
			result = resultList;
		} else if (type.equals("6")) {// 判断为投递信息
			Map resultList = (Map) deliveryService.update(param);
			result = resultList;
		} else if (type.equals("7")) {// 判断为调拨信息
			Map resultList = (Map) transfersService.update(param);
			result = resultList;
		} else if (type.equals("8")) {// 判断为退货信息
			Map resultList = (Map) returngoods.update(param);
			result = resultList;
		}
		return result;

	}
	
	public Map toView(Map param,Map pg) {
		List list = new ArrayList<>();
		String Type = new String();
		Map results = dao.loadPage("LD_TOVIEW", param,pg);
		List resultList =  (List) results.get("list");
		for (int i = 0; i < resultList.size(); i++) {
			List List = new ArrayList<>();
			Map element = (Map) resultList.get(i);
			if (param.get("type").equals("1")) {
				Type=(String) param.get("type");
				List = (List) dao.loadListByCode("LD_TOVIEV_INVENTORY", element);
			} else if (param.get("type").equals("2")) {
				Type=(String) param.get("type");
				List = (List) dao.loadListByCode("LD_TOVIEV_OUTBOUND", element);
			} else if (param.get("type").equals("3")) {
				Type=(String) param.get("type");
				List = (List) dao.loadListByCode("LD_TOVIEV_ISSUE", element);
			} else if (param.get("type").equals("4")) {
				Type=(String) param.get("type");
				List = (List) dao.loadListByCode("LD_TOVIEV_APPLY", element);
			} else if (param.get("type").equals("45")) {
				Type=(String) param.get("type");
				List = (List) dao.loadListByCode("LD_TOVIEV_APPLY", element);
			} else if (param.get("type").equals("54")) {
				Type=(String) param.get("type");
				List = (List) dao.loadListByCode("LD_TOVIEV_APPLY", element);
			} else if (param.get("type").equals("6")) {
				Type=(String) param.get("type");
				List = (List) dao.loadListByCode("LD_TOVIEV_DELIVERY", element);
			} else if (param.get("type").equals("8")) {
				Type=(String) param.get("type");
				List = (List) dao.loadListByCode("LD_TOVIEV_RTV", element);
			} else if (param.get("type").equals("7")) {// 调拨
				Map map = dao.loadRowByCode("LD_TRANSFERS", element);
				String type = (String) map.get("business_types");
				if (type.equals("1")) {// 判断业务状态为调入
					Type=(String) param.get("type");
					List = dao.loadListByCode("LD_TOVIEV_CALLIN", element);
				}
				if (type.equals("2")) {// 判断业务状态为调入
					Type=(String) param.get("type");
					List = dao.loadListByCode("LD_TOVIEV_CALLOUT", element);
				}
			}
			for(int j=0;j<List.size();j++){
				Map ele = (Map) List.get(j);
				ele.put("type", Type);
				list.add(ele);
			}
			results.put("list", list);
			results.put("tcount", list.size());
		}
		return  results;
	}
	
}
