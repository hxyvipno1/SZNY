package com.differ.microservice.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

@Service
public class SubsidyService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private StepDao dao;
	
	/**
	 * 新增或更新补贴详情信息
	 * @param param
	 * @return
	 */
	public Map saveSubsidy(Map param){
		Map result = new HashMap<>();
		if(param.containsKey("idx")){		//判断是否存在idx
			result = dao.update("t_subsidy", param);
		}else{
			result = dao.add("t_subsidy", param);
		}
		return result;
		
	}
}
