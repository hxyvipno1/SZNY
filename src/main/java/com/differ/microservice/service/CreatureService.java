package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

@Service
public class CreatureService {
private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private StepDao dao;
	@Autowired
	private DistrictService dis;
	
	/**
	 * 动物个体管理列表查询
	 * @param param
	 * @return
	 */
	public Map creatureList(Map param,Map pg){
		
		Map map1 =  dao.loadPage("LD_CREATURE_LIST", param,pg);
		List list = (List) map1.get("list");
		List resultList = new ArrayList<>();
		for (Object object : list) {
			Map map = new HashMap<>();
			map = (Map) object;
			if(map.get("district_idx") != null){
				String disname =  dis.queryDistrict(map.get("district_idx").toString());
				map.put("district_name", disname);
			}
			resultList.add(map);
		}
		Map result = new HashMap<>();
		if(map1.containsKey("tcount")){
			String tcount = map1.get("tcount").toString();
			result.put("tcount", tcount);
		}
		result.put("list", resultList);
		
		return result;
	}
	
	/**
	 * 复合模式新增
	 * @param param
	 * @return
	 */
	public Map setcreatureProduct(Map param){
		Map result = new HashMap<>();
		
		String rfid = param.get("rfid").toString();//取出rfid
		Map rfidmap = new HashMap<>();
		rfidmap.put("rfid", rfid);
		Map m = (Map) dao.loadListByCode("LD_CREATURE_RFID", rfidmap).get(0);//查询rfid是否存在切唯一
		String count = m.get("c").toString();
		if(count.equals("1")){ 
			Map idxMap = (Map) dao.loadListByCode("LD_CREATURE_IDX", rfidmap).get(0);
			String creature_idx = idxMap.get("idx").toString();
			param.remove("rfid");
			for (Object obj : param.keySet()) { //取出所有子级map插入t_creatureinfo
				Map map = (Map) param.get(obj);
				map.put("creature_idx", creature_idx);
				map = dao.add("t_creature_produce", map);
				result.put(obj, map);//根据key不同put到返回map中
			}
		}
		return result;
	}
	
	public List query(Map param){
		List result = dao.loadListByCode("LD_BAR_DORM", param);
		List results = new ArrayList<>();
		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			Map pid = new HashMap<>();
			pid.put("parent_id", element.get("idx"));
			List chindren = dao.loadListByCode("LD_BAR_DORM", pid);
			element.put("chindren", chindren);
			results.add(element);
		}
		return results;
	}

}
