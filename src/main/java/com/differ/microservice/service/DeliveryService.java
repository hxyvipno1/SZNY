/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 物资投递Service
 * @author zhenshan.xu
 * @version $Id: DeliveryService.java, v 0.1 2017年10月27日 上午10:38:20 zhenshan.xu Exp $
 */
@Service
public class DeliveryService {
	@Autowired
	private StepDao dao;
	
	@Autowired
	private InventoryService inventory;
	
	
	//物资投递
	public Map send(Map param) {
		
		Map deliver = dao.add("t_delivery", param);
		List result = (List) param.get("product_lists");
		
		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
			String main_idx = element.get("product_name").toString();
			// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
			if (main_idx == null || main_idx.equals("")) {
				continue;
			}Map idx = new HashMap<>();
			// 赋值main_idx到idx中;
			idx.put("idx", main_idx);
			// 根据main_idx查询产品信息
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			element.put("main_idx", deliver.get("idx"));
			// 获取物资的idx
			element.put("product_idx", product.get("idx"));
			element.put("product_name", product.get("product_name"));
			Map parameter = new HashMap<>();
			parameter.put("quantity", element.get("quantity"));
			parameter.put("line_abstracts1", element.get("line_abstracts1"));
			String endNo=inventory.computations(parameter);
			element.put("line_abstracts1",endNo);
			// 保存物资信息
			Map map = dao.add("t_inventory_detail", element);
			// 计算当前结余量
			int stock = inventory.inventoryNum(product);
			element.put("surplus_num", stock);
			// 保存状态:出库
			element.put("type", "6");
			element.put("people_idx",  param.get("idx"));
			element.put("document_num",  param.get("document_num"));
			element.put("residential_site", param.get("residential_site"));
			element.put("abstracts", param.get("abstracts"));
			dao.add("t_statistics", element);
		}
		return deliver;
	}
	/**
	 * 查看详情
	 * @param param
	 * @return
	 */
	public List details(Map param){
		Map idx = new HashMap<>();
		
		List list = new ArrayList<>();
		idx.put("main_idx", param.get("main_idx"));
		List resultList = dao.loadListByCode("LD_DELIVER", idx);
		List product_name = dao.loadListByCode("LD_PRODUCT_NAME", param);
		for (Object result : resultList) {
			Map details = new HashMap<>();
			details.put("nameArr", product_name);
			details.put("value", ((Map) result).get("product_idx"));
			((Map) result).put("product_name",details);
			list.add(result);
		}
		return list;
	}
	
	public Map update (Map param){
		// 获取main_idx 到 idx 进行修改 t_inventory
		param.put("idx", param.get("main_idx"));
		Map deliver = dao.update("t_delivery", param);
		Map mainIdx = new HashMap<>();
		// 通过main_idx删除物资详情列表信息
		mainIdx.put("main_idx", param.get("main_idx"));
		dao.excuteByCode("RM_INV_DETAIL", mainIdx);
		List result = (List) param.get("product_lists");

		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
			String main_idx = element.get("product_name").toString();
			// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
			if (main_idx == null || main_idx.equals("")) {
				continue;
			}
			Map idx = new HashMap<>();
			// 赋值main_idx到idx中;
			idx.put("idx", main_idx);
			// 根据main_idx查询产品信息
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			element.put("main_idx", deliver.get("idx"));
			// 获取物资的idx
			element.put("product_idx", product.get("idx"));
			element.put("product_name", product.get("product_name"));
			// 保存物资信息
			Map map = dao.add("t_inventory_detail", element);
			// 计算当前结余量
			int stock = inventory.inventoryNum(product);
			element.put("surplus_num", stock);
			element.put("idx", param.get("rowIdx"));
			element.put("surplus_num", stock);
			// 获取时间
			element.put("occur_time", param.get("storage_time"));
			// 保存状态:投递
			element.put("type", "6");
			element.put("people_idx",  param.get("idx"));
			element.put("abstracts", param.get("abstracts"));
			// 修改 t_statistics
			dao.update("t_statistics", element);
		}

		return deliver;
	}
	
}
 