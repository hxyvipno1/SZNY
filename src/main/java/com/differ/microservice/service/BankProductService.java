package com.differ.microservice.service;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

@Service
public class BankProductService {
private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private StepDao dao;
	
	/**
	 * 银行产品列表
	 * @param param
	 * @return
	 */
	public List bankproductList(Map param){
		return  dao.loadListByCode("LD_BANKPRODUCT_LIST", param);
	}
	
}
