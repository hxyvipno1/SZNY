package com.differ.microservice.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;
import com.differ.microservice.core.util.DateUtil;
import com.differ.microservice.core.util.JSONParser;
import com.differ.microservice.util.TxtUtil;

@Service
public class CmmService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    private StepDao       dao;
	
	public Map thermometer(Map param){
		Map entity=new HashMap();
		param.put("type", 1);
		entity.putAll(param);		
		entity.put("json",JSONParser.obj2Json(param));
		dao.add("t_json", entity);
		return entity;
	}
	
	public Map thermfile(Map param){
		param.put("type", 2);
		dao.add("t_json", param);
		return param;
	}
	
	public Map deviceinfo(Map param){
		String sql="select max(create_time) as time, max(max) as top ,min(min) as low from t_json where serialno=:serialno and type=1 and create_time > DATE_SUB('2017-12-14 12:23:30',INTERVAL  10 MINUTE) ";
		//String sql="select max(create_time) as time, max(max) as top ,min(min) as low from t_json where serialno=:serialno and type=1 and create_time > DATE_SUB(now(),INTERVAL  10 MINUTE) ";

		Map row=dao.loadRowByMap(sql, param);
		
		sql="select  attach_idx from t_json where serialno=:serialno and type=2 and create_time > DATE_SUB('2017-12-14 12:23:30',INTERVAL  10 MINUTE) limit 0, 10";
		//sql="select attach_idx from t_json where serialno=:serialno and type=2 and create_time > DATE_SUB(now(),INTERVAL  10 MINUTE)";

		List list=dao.loadListByMap(sql, param);
		row.put("img", list);
		return row;
	}
	
	String xzsql="select d.name as text,d.idx as id ,c.idx from t_customer c left join t_district d on SUBSTRING_INDEX(c.district_idx,',',-1)=d.idx where c.info_code=:info_code ";
	public Map xztree(String info_code){
		Map rst=new HashMap();
		
		Map param=new HashMap();
		param.put("info_code", info_code);		
		List list=dao.loadListByMap(xzsql, param);
		
		String distict_id=(String)((Map)list.get(0)).get("id");
		int id=Integer.parseInt(distict_id);
		
		rst.put("xjId", id);
		list.add(0, new HashMap());
		rst.put("tree", list);
		
		return rst;
	}
	
	String tsql="select count(*) cnt from t_dictionary where pid in (90,210,100651,100582,70) ";
	String t2sql="select serial as rid,pid as rtype,name from t_dictionary where pid in (90,210,100651,100582,70) order by serial asc";
	public Map t8(String id){
		Map rst=new HashMap();
		StringBuffer rtn=new StringBuffer();
		Number sum= (Number)dao.loadRowByMap(tsql, null).get("cnt");		
		List rlist=dao.loadListByMap(t2sql, null);
		rst.put("total", sum);
		
		rst.put("length", 0);
		for(int i = 0 ; i < rlist.size() ; i++){
			Map row=(Map)rlist.get(i);
			String rType = (String)row.get("rtype");
			String rid=TxtUtil.addTxt((String)row.get("rid"),3,0,0);
			String name=TxtUtil.addTxt((String)row.get("name"),16,1,1);
			String type="";
			switch(rType){
			case "70":
				type="1";break;
			case "90":
				type="2";break;
			case "210":
				type="3";break;
			case "100651":
				type="4";break;
			case "100582":
				type="5";break;			
			}
			
			String line=TxtUtil.strTo2n(type+rid+name);
logger.debug("t8 len:"+TxtUtil.length(line));			
rst.put("length", TxtUtil.length(line));
			rtn.append(line);
		}
		
		rst.put("content", rtn.toString());
		return rst;
	}
	
	public Map t9(String id){
		
		return null;
	}
	
	String d0sql="select info_code as c0,'M' as c1, register_name as c2, '' as c3, '' as c4,breeding_stock as c5, '0' as c6, '0' as c7, '100' as c8 ,'' as c9, '' as c10,'' as c11,'' as c12,'' as c13,'' as c14 from t_customer where FIND_IN_SET( :id ,district_idx) ";
	public Map d0(String id){
		Map rst=new HashMap();
		
		Map param=new HashMap();
		param.put("id", id);		
		List rlist=dao.loadListByMap(d0sql, param);
		StringBuffer content=new StringBuffer();

		rst.put("total", rlist.size());
		rst.put("length", 0);
		rlist.forEach(item->{
			Map rw=(Map)item;
			StringBuffer row=new StringBuffer();
			row.append(TxtUtil.addTxt((String)rw.get("c0"),15,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c1"),1,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c2"),16,1,1 ))
				.append(TxtUtil.addTxt((String)rw.get("c3"),3,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c4"),4,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c5"),6,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c6"),6,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c7"),2,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c8"),3,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c9"),6,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c10"),3,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c11"),6,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c12"),3,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c13"),6,0,0 ))
				.append(TxtUtil.addTxt((String)rw.get("c14"),8,1,1 ));
logger.debug("d0 len:"+TxtUtil.length(row.toString()));
			rst.put("length", TxtUtil.length(row.toString()));
			content.append(row.toString());
		});
		rst.put("content", content);
		return rst;
	}
	
	String l1sql="select a.rfid as c0,c.register_name as c1,d.name as c2,a.animal_no as c3,a.birthday as c4,'' as c5,a.production_num as c6,'' as c7,'' as c8,'' as c9,'' as c10,'' as c11,c.telephone as c12,c.address as c13,'0' as c14,'' as c15,'' as c16,'0' as c17,c.idx as c18 from t_creatureinfo a left join t_dictionary d on a.breed=d.idx,t_customer c where a.customer_idx=c.idx and FIND_IN_SET( :id ,c.district_idx) ";
	public Map dnl1(String id){
		Map param=new HashMap();
		
		param.put("id", StringUtils.replace(id, "\"", ""));		
		List rlist=dao.loadListByMap(l1sql, param);
		StringBuffer content=new StringBuffer();
		Map rst=new HashMap();
		rst.put("total", rlist.size());
		rst.put("length", 0);
		rlist.forEach(item->{
			Map rw=(Map)item;
			StringBuffer row=new StringBuffer();
			row.append(TxtUtil.addTxt((String)rw.get("c0"),15,0,0 ))
			.append(TxtUtil.addTxt((String)rw.get("c1"),16,1,1 ))
			.append(TxtUtil.addTxt((String)rw.get("c2"),10,1,1 ))
			.append(TxtUtil.addTxt((String)rw.get("c3"),4,0,0 ))
			// .append(TxtUtil.addTxt((String)rw.get("c4"),6,0,0 ))
			.append(TxtUtil.addTxt(DateUtil.format((Date)rw.get("c4"), "yyMMdd") ,6,0,0 ))
			.append(TxtUtil.addTxt((String)rw.get("c5"),3,0,0 ))
			.append(TxtUtil.addTxt((String)rw.get("c6"),2,0,0 ))
			.append(TxtUtil.addTxt((String)rw.get("c7"),6,0,0 ))
			.append(TxtUtil.addTxt((String)rw.get("c8"),6,0,0 ))
			.append(TxtUtil.addTxt((String)rw.get("c9"),4,0,0 ))
			.append(TxtUtil.addTxt((String)rw.get("c10"),2,0,0 ))
			.append(TxtUtil.addTxt((String)rw.get("c11"),2,0,0 ))
			.append(TxtUtil.addTxt((String)rw.get("c12"),16,0,0 ))
			.append(TxtUtil.addTxt((String)rw.get("c13"),16,1,1 ))
			.append(TxtUtil.addTxt((String)rw.get("c14"),2,0,0 ))
			.append('0')
			.append("1")
			.append(TxtUtil.addTxt((String)rw.get("c16"),6,0,0 ))
			.append("1");
			rst.put("length", TxtUtil.length(row.toString()));;
logger.debug("dnl1 len:"+TxtUtil.length(row.toString()));	
			content.append(row.toString());
		});
		rst.put("content", content);
		
		return rst;
	}
	

}
