/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 入库
 * @author zhenshan.xu
 * @version $Id: InventoryService.java, v 0.1 2017年9月3日 下午2:58:33 zhenshan.xu
 *          Exp $
 */
@Service
public class InventoryService {

	@Autowired
	private StepDao dao;

/**
 * 查询物资
 * @param param
 * @return
 */
	public List query(Map param) {
		
		return dao.loadListByCode("LD_ALLMATERIALS", param);
	}
	/**
	 * 入库
	 * @param param
	 * @return
	 */
	public Map storage(Map param) {
		// 保存到 t_inventory 表;
		param.put("staff_name", param.get("people_name"));
		Map inventory = dao.add("t_inventory", param);
		List result = (List) param.get("product_lists");

		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
			String main_idx = element.get("product_name").toString();
			// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
			if (main_idx == null || main_idx.equals("")) {
				continue;
			}
			Map product = new HashMap<>();
			Map idx = new HashMap<>();
			// 赋值main_idx到idx中;
			idx.put("idx", main_idx);
			// 根据main_idx查询产品信息
			product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			element.put("main_idx", inventory.get("idx"));
			// 获取物资的idx
			element.put("product_idx", product.get("idx"));
			element.put("product_name", product.get("product_name"));
			Map parameter = new HashMap<>();
			parameter.put("quantity", element.get("quantity"));
			parameter.put("line_abstracts1", element.get("line_abstracts1"));
			String endNo = this.computations(parameter);
			element.put("line_abstracts1", endNo);
			// 保存物资信息
			Map map = dao.add("t_inventory_detail", element);
			// 计算当前结余量
			int stock = this.inventoryNum(product);
			element.put("surplus_num", stock);
			// 获取时间
			element.put("occur_time", param.get("storage_time"));
			// 保存状态:入库
			element.put("type", "1");
			element.put("people_idx", param.get("people_id"));
			element.put("document_num", param.get("document_num"));
			element.put("abstracts", param.get("abstracts"));
			dao.add("t_statistics", element);
		}
		return inventory;
	}
	
	/**
	 * 计算结束连续数字编码值
	 * @param param
	 * @return
	 */
	public String computations (Map param){
		String abstracts = new String();
		int startno = (int) (param).get("quantity");
		if (param.get("line_abstracts1").equals("")) {
			abstracts = "";
		} else {
			String coding = (String) param.get("line_abstracts1");
			//判断coding是否含有英文字符
			if (coding.matches(".*[a-zA-Z].*")) {
				abstracts = coding;
			} else {
				long line_abstracts1 = new Long(coding).longValue();
				long quantity = new Long(startno).longValue();
				long endNo = (long) (quantity + line_abstracts1) - 1;
				if (line_abstracts1 == endNo) {
					abstracts = coding;
				} else {
					abstracts = coding + "-" + endNo;
				}
			}
		}
		return abstracts;
	}
	/**
	 * 修改物资入库
	 * 
	 * @param param
	 * @return
	 */
	public Map update(Map param) {
		//获取main_idx 到 idx 进行修改 t_inventory
		param.put("idx", param.get("main_idx"));
		param.put("staff_name",param.get("people_name"));
		Map inventory = dao.update("t_inventory", param);
		Map mainIdx = new HashMap<>();
		//通过main_idx删除物资详情列表信息
		mainIdx.put("main_idx", param.get("main_idx"));
		dao.excuteByCode("RM_INV_DETAIL", mainIdx);
		List result = (List) param.get("product_lists");
		
		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
			String main_idx = element.get("product_name").toString();
			// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
			if (main_idx == null || main_idx.equals("")) {
				continue;
			}
			Map idx = new HashMap<>();
			//赋值main_idx到idx中;
			idx.put("idx", main_idx);
			//根据main_idx查询产品信息
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			element.put("main_idx", inventory.get("idx"));
			//获取物资的idx
			element.put("product_idx", product.get("idx"));
			element.put("product_name", product.get("product_name"));
			//保存物资信息
			Map map = dao.add("t_inventory_detail", element);
			//计算当前结余量
			int stock = this.inventoryNum(product);
			element.put("surplus_num", stock);
			element.put("idx", param.get("rowIdx"));
			element.put("surplus_num", stock);
			//获取时间
			element.put("occur_time", param.get("storage_time"));
			//保存状态:入库
			element.put("type", "1");
			element.put("people_idx",  param.get("idx"));
			element.put("abstracts", param.get("abstracts"));
			//修改 t_statistics
			dao.update("t_statistics", element);
		}
		
		return inventory;
	}
	/**
	 * 查看详情
	 * @param param
	 * @return
	 */
	public List details(Map param){
		Map idx = new HashMap<>();
		
		List list = new ArrayList<>();
		idx.put("main_idx", param.get("main_idx"));//截取main_idx到idx对象中
		List resultList = dao.loadListByCode("LD_INVENTORY", idx);
		List product_name = dao.loadListByCode("LD_PRODUCT_NAME", param);
		for (Object result : resultList) {
			Map details = new HashMap<>();
			details.put("nameArr", product_name);
			details.put("value", ((Map) result).get("product_idx"));
			((Map) result).put("product_name",details);
			list.add(result);
		}
		return list;
	}
	
	/**
	 * 获取当前结余
	 * @param param
	 * @return
	 */
	public int inventoryNum(Map param) {
		int stock_num =0;//声明stock_num为入库数量，默认为零；
		int out_num =0;//声明out_num为出库数量，默认为零；
		int remainder=0;//声明remainder为当前剩余数量，默认为零；
		List list=dao.loadListByCode("LD_IN_QUANTITY", param);//查询该产品物资的入库数量并累加
		for (int i = 0; i < list.size(); i++) {
			Map element = (Map) list.get(i);
			int startno=(int)(element).get("quantity");
			long quantity=new Long(startno).longValue();
			stock_num=(int) (stock_num+quantity);
		}
		List result=dao.loadListByCode("LD_OUT_QUANTITY", param);//查询该产品物资的出库数量并累加
		for (int i = 0; i < result.size(); i++) {
			Map element = (Map) result.get(i);
			int startno=(int)(element).get("quantity");
			long quantity=new Long(startno).longValue();
			out_num=(int) (out_num+quantity);
		}
 		if(stock_num == 0){
 			remainder=stock_num;
 		}else{
		 remainder= stock_num-out_num;//入库量减去出库量等于当前剩余量
 		}
		return remainder;
	} 
	
}
