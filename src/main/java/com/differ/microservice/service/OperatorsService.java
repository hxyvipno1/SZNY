/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: OperatorsService.java, v 0.1 2017年8月25日 下午5:08:38 zhenshan.xu Exp $
 */
@Service
public class OperatorsService {
	
	@Autowired
	private StepDao dao;
	
	public List findAll(Map param) {
		return dao.loadListByCode("LD_OPERATORS", param);
	}
	
	/**
     * 查询供应商名称
     * @param param
     * @return
     */
    public List queryName(Map param) {

        return dao.loadListByCode("LD_OPERATORS_NAME", param);
    }
    public List queryByIdx(Map param) {
		return dao.loadListByCode("LD_OPERATORS_IDX", param);
	}
    /**
     *删除
     */
    public Map delete(Map param){
        return del(param,"LD_OPERATORS_IDX","LD_OPERATORS_PID","t_operators");
    }
    
    public Map del(Map param,String queryByIdx,String queryByPid,String tbl){
        List beforeDelete =  dao.loadListByCode(queryByIdx, param);
        dao.remove(tbl, param);
        List afterDelete =  dao.loadListByCode(queryByIdx, param);
        Object pid = param.get("idx");
        Map childParam = new HashMap<>();
        childParam.put("pid", pid);
        List list =  dao.loadListByCode(queryByPid, childParam);
        for (Object child : list) {
            String str = child.toString();
            String[] str1 = str.split(",");
            String[] str2 = str1[0].split("=");
            String childIdx = str2[1];
            Map childNode = new HashMap<>();
            childNode.put("idx", childIdx);
            dao.remove(tbl, childNode);
            del(childNode,queryByIdx,queryByPid,tbl);
        }
        Map map = new HashMap<>();
        
        if(beforeDelete.size()>0&&afterDelete.size()==0){
            map.put("message", "操作成功");
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("message", "操作失败");
        return map;
        
    }
    
    /**
     * 运营商树拖拽
     * @param param
     * @return
     */
    public List updateTree(Map param){
    	Map tree_array = new HashMap<>();
    	List list = new ArrayList<>(); 
    	List resultList = new ArrayList<>();
    	list =  (List) param.get("tree_array");
    	for(Object m : list){
    		tree_array = (Map) m;
    		resultList.add(dao.update("t_operators",tree_array)); 
    	}
		return resultList;
    	
    }
}
