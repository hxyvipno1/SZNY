/**
 * 
 */
package com.differ.microservice.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: VeterinaryDrugsService.java, v 0.1 2017年8月26日 上午10:24:53 zhenshan.xu Exp $
 */
@Service
public class VeterinaryDrugsService {
	@Autowired
	private StepDao dao;

	/**
	 * @param param
	 * @return
	 */
	public List findAll(Map param) {
		
		return dao.loadListByCode("LD_DRUGSALL", param);
	}

	public List find(Map param) {
		return dao.loadListByCode("LD_DRUGS", param);
	}
	
	 public List query(Map map) {
	        //第一个参数：sql语句，第二个参数：Map，如果有多个参数，分别放到map里，如：map.put("user_name","");map.put("password","")
	        return dao.loadListByCode("LD_DISTRICT", map);
	    }

	/**
	 * @param param
	 * @return
	 */
	public Map delete(Map param) {
		
		return dao.excuteByCode("RM_DRUGS", param);
	}
	 
	 
}
  