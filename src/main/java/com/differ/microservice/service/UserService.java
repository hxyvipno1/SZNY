package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;
import com.differ.microservice.model.User;

/**
 * 用户service类
 */

@Service
public class UserService {
	@Autowired
	private StepDao dao;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	// 封装好的通用的Dao(也可以自己定义)
	// @Autowired
	// private StepDao dao;



	public List<User> queryByField(String user_name, String password) {
		User user = new User();
		user.setUser_name(user_name);
		user.setPassword(password);
		Map param=new HashMap();
		//param.put("user_name", value);
		dao.loadListByCode("", param);
		
//		return dao.findByField(user);
		
		return null;
	}

	// //通过ID查询列表方法
	// public List<String> queryByIdx(String idx) {
	// //第一个参数：表名，第二个参数：主键
	// Map row = dao.loadRT("t_user", idx);
	// return new ArrayList<String>(row.values());
	// }
	//
	// //list方法
	// public List<String> query() {
	// //第一个参数：sql语句，第二个参数：Map，如果有多个参数，分别放到map里，如：map.put("user_name","");map.put("password","")
	// return dao.loadListByMap("select * from t_user", null);
	// }
	//
	// public Map login(Map map) {
	// //第一个参数：sql语句，第二个参数：Map，如果有多个参数，分别放到map里，如：map.put("user_name","");map.put("password","")
	// return dao.loadRowByMap("select * from t_user",map);
	// }

}
