/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: NodeService.java, v 0.1 2017年7月28日 上午9:20:05 dunfang.zhou Exp $
 */
@Service
public class NodeListService {
    
 private final Logger logger = LoggerFactory.getLogger(this.getClass());

    
    
    @Autowired
    private StepDao dao;
    
    /**
     * 查询全部
     */
    public List findAll(){
        
        return dao.loadListByCode("LD_ALLNODE_LIST", null);
        
    }
    
    /**
     * 通过idx查询节点
     */
    public List findByIdx(Map param){
        
        return dao.loadListByCode("LD_NODE_LIST", param);
        
    }

    /**
     *通过pid查询子节点
     */
    public Map findByCode(Map param){
        
        List list = dao.loadListByCode("LD_NODELIST_CODE", param);
        Map map = new HashMap<>();
        if(list.size()>1){
            map.put("valid", false);
            return map;
        }
        String str1 = param.get("node_code").toString();
        for (Object obj : list) {
            String str2 = obj.toString();
            String[] str = str2.split("=");
            String[] ss = str[1].split("}");
            if(str1.equals(ss[0])){
                map.put("valid", false);
                return map;
            }
        }
        map.put("valid", true);
        return map;
        
    }
    /**
     *通过pid查询子节点
     */
    public List findByPid(Map param){
        
        return dao.loadListByCode("LD_CHILDNODE_LIST", param);
        
    }
    
    /**
     *删除
     */
    public Map delete(Map param){
        return del(param,"LD_NODE_LIST","LD_CHILDNODE_LIST","t_node_list");
    }
    
    public Map del(Map param,String queryByIdx,String queryByPid,String tbl){
        List beforeDelete =  dao.loadListByCode(queryByIdx, param);
        dao.remove(tbl, param);
        List afterDelete =  dao.loadListByCode(queryByIdx, param);
        Object pid = param.get("idx");
        Map childParam = new HashMap<>();
        childParam.put("pid", pid);
        List list =  dao.loadListByCode(queryByPid, childParam);
        for (Object child : list) {
            String str = child.toString();
            String[] str1 = str.split(",");
            String[] str2 = str1[0].split("=");
            String childIdx = str2[1];
            Map childNode = new HashMap<>();
            childNode.put("idx", childIdx);
            dao.remove(tbl, childNode);
            del(childNode,queryByIdx,queryByPid,tbl);
        }
        Map map = new HashMap<>();
        
        if(beforeDelete.size()>0&&afterDelete.size()==0){
            map.put("message", "操作成功");
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("message", "操作失败");
        return map;
        
    }
    
    
    /**
     * 查询附件
     * @param param
     * @return
     */
    public List findChild(Map param){
        
        return dao.loadListByCode("LD_NODE_CHILD_LIST", param);
    }
    
}
