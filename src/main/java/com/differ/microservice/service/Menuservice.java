package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;
@Service
public class Menuservice {
	
	
	
	
	   private final Logger logger = LoggerFactory.getLogger(this.getClass());
	    //封装好的通用的Dao(也可以自己定义)
	    @Autowired
	    private StepDao      dao;

	    //通过ID查询列表方法
	    public List<String> queryByIdx(String idx) {
	        //第一个参数：表名，第二个参数：主键
	        Map row = dao.loadRT("t_menu", idx);
	        return new ArrayList<String>(row.values());
	    }
	    
	    //通过父类ID查询列表方法
	    public List<String> queryParentId(Map param){
	    	List row = dao.loadListByMap("select * from t_menu where parentid ="+param.get("parentid"), param);
			return row;
	    }
	    

	    //list方法
	    public List<String> query() {
	        //第一个参数：sql语句，第二个参数：Map，如果有多个参数，分别放到map里，如：map.put("user_name");map.put("password")
	        return dao.loadListByCode("LD_MENU", null);
	    }
	    
	    public Map<String,Object> add(Map entity){
			return dao.add("t_menu", entity);
	    	
	    }

}
