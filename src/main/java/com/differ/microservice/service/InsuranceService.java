/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: InsuranceService.java, v 0.1 2017年8月20日 下午3:30:37 dunfang.zhou Exp $
 */
@Service
public class InsuranceService {

    @Autowired
    private StepDao dao;
    
    /**
     * 查询所有正常银行
     * @param param
     * @return
     */
    public List findAll(Map param) {

        return dao.loadListByCode("LD_INSURANCE", param);
    }
    
    /**
     * 查询所有正常银行
     * @param param
     * @return
     */
    public List findByIdx(Map param) {

        return dao.loadListByCode("LD_INSURANCE_IDX", param);
    }
    
    /**
     * 查询保险名称
     * @param param
     * @return
     */
    public List queryName(Map param) {

        return dao.loadListByCode("LD_INSURANCE1", param);
    }
    /**
     * @param param
     * @return
     */
    public Map delete(Map param) {
        
        return del(param,"LD_INSURANCE_IDX","LD_INSURANCE_PID","t_insurance");
    }
    
    public Map del(Map param,String queryByIdx,String queryByPid,String tbl){
        List beforeDelete =  dao.loadListByCode(queryByIdx, param);
        dao.remove(tbl, param);
        List afterDelete =  dao.loadListByCode(queryByIdx, param);
        Object pid = param.get("idx");
        Map childParam = new HashMap<>();
        childParam.put("pid", pid);
        List list =  dao.loadListByCode(queryByPid, childParam);
        for (Object child : list) {
            String str = child.toString();
            String[] str1 = str.split(",");
            String[] str2 = str1[0].split("=");
            String childIdx = str2[1];
            Map childNode = new HashMap<>();
            childNode.put("idx", childIdx);
            dao.remove(tbl, childNode);
            del(childNode,queryByIdx,queryByPid,tbl);
        }
        Map map = new HashMap<>();
        
        if(beforeDelete.size()>0&&afterDelete.size()==0){
            map.put("message", "操作成功");
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("message", "操作失败");
        return map;
        
    }
    
    /**
     * 保险树拖拽
     * @param param
     * @return
     */
    public List updateTree(Map param){
    	Map tree_array = new HashMap<>();
    	List list = new ArrayList<>(); 
    	List resultList = new ArrayList<>();
    	list =  (List) param.get("tree_array");
    	for(Object m : list){
    		tree_array = (Map) m;
    		resultList.add(dao.update("t_insurance",tree_array)); 
    	}
		return resultList;
    	
    }
}
