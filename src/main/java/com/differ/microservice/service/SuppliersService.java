/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: SuppliersService.java, v 0.1 2017年8月23日 下午7:13:00 zhenshan.xu Exp $
 */
@Service
public class SuppliersService {
	
	
	@Autowired
	private StepDao dao;
	/**
	 * 查询全部
	 */
	public List findAll() {

		return dao.loadListByCode("LD_SUPPLIERS", null);

	}
	
	
	
	/**
	 * 来往单位名称
	 * @param param
	 * @return
	 */
	 public List btype(Map param){
		List results = dao.loadListByCode("LD_SUPPLIERS", param);
			for (Object object : results) {
				 Map result = new HashMap<>();
				String list =  (String) ((Map) object).get("suppliers_name");
				if(list.equals("供应商")){
					results.remove(object);
					break;
				}
			}
	        return results;
	    }
	/**
	 * 通过idx查询节点
	 * @param param
	 * @return
	 */
	public List findByIdx(Map param) {
		return dao.loadListByCode("LD_DISTRICT_IDX", param);
	}

	/**
	 * 通过pid查询子节点
	 */
	public List findByPid(Map param) {
		return dao.loadListByCode("LD_DISTRICT_PID", param);
	}
	public List query(Map map) {
        //第一个参数：sql语句，第二个参数：Map，如果有多个参数，分别放到map里，如：map.put("user_name","");map.put("password","")
        return dao.loadListByCode("LD_DISTRICT", map);
    }
	
	public List queryMaterials(Map map) {
        //第一个参数：sql语句，第二个参数：Map，如果有多个参数，分别放到map里，如：map.put("user_name","");map.put("password","")
        return dao.loadListByCode("LD_MATERIALS", map);
    }

	/**
     * 查询供应商名称
     * @param param
     * @return
     */
    public List queryName(Map param) {
        return dao.loadListByCode("LD_SUPPLIERS_NAME", param);
    }
    
    /**
     * 根据idx供应商查看
     * @param param
     * @return
     */
    public Map queryByIdx(Map param) {
    	Map Idx =new HashMap<>();
		Map map = dao.loadRowByCode("LD_SUPPLIERS_IDX", param);
		String id=(String) map.get("idx");
		Idx.put("suppliers", id);
		List list = dao.loadListByCode("LD_PRODUCTS_NEXUS", Idx);
		if (list==null || list.size()==0) {
			map = dao.loadRowByCode("LD_SUPPLIERS_IDX", param);
		} else {
			StringBuffer materialName = new StringBuffer();
			StringBuffer materialId = new StringBuffer();
			for (int i = 0; i < list.size(); i++) {				
				Map array = (Map) list.get(i);
				String material = (String) array.get("product_idx");
				String[] element = material.split(",|，");
				for (String  material_id : element) {
					Map materialIdx = new HashMap<>();
					materialIdx.put("idx", material_id);
					Map result = dao.loadRowByCode("LD_PRODUCT_IDX", materialIdx);
					materialName = materialName.append(result.get("product_name").toString()+"、");
					materialId = materialId.append(result.get("idx").toString()+",");
				}
			}
			String name = materialName.toString();
			String Name = name.substring(0,name.length()-1);
			String idx = materialId.toString();
			String IDX = idx.substring(0,idx.length()-1);
			map.put("material_name", Name);
			map.put("material_idx", IDX);
		}
		return map;
	}
    
	public Map update(Map param) {
		Map map = dao.update("t_suppliers", param);
		String idx = (String) param.get("idx");
		Map product = new HashMap<>();
		product.put("suppliers", idx);
		Map element = dao.loadRowByCode("LD_PRODUCTS_NEXUS", product);
		if (element == null || element.equals("")) {
			String material = (String) param.get("material_idx");
			String[] params = material.split(",|，");
			for (String par : params) {
				Map parameter = new HashMap<>();
				parameter.put("product_idx", par);
				parameter.put("suppliers_idx", idx);
				dao.add("t_product_nexus", parameter);
			}
		} else {
			Map id = new HashMap<>();
			id.put("suppliers", idx);
			dao.excuteByCode("RM_PRODUCTS_NEXUS", id);
			String material = (String) param.get("material_idx");
			String[] params = material.split(",|，");
			for (String par : params) {
				element.remove("idx");
				element.put("product_idx", par);
				Map results = dao.add("t_product_nexus", element);
			}
		}

		return map;

	}
    
    /**
     *删除供应商信息
     */
    public Map delete(Map param){
        return del(param,"LD_SUPPLIERS_IDX","LD_SUPPLIERS_PID","t_suppliers");
    }
    public Map del(Map param,String queryByIdx,String queryByPid,String tbl){
        List beforeDelete =  dao.loadListByCode(queryByIdx, param);
        dao.remove(tbl, param);
        List afterDelete =  dao.loadListByCode(queryByIdx, param);
        Object pid = param.get("idx");
        Map childParam = new HashMap<>();
        childParam.put("pid", pid);
        List list =  dao.loadListByCode(queryByPid, childParam);
        for (Object child : list) {
            String str = child.toString();
            String[] str1 = str.split(",");
            String[] str2 = str1[0].split("=");
            String childIdx = str2[1];
            Map childNode = new HashMap<>();
            childNode.put("idx", childIdx);
            dao.remove(tbl, childNode);
            del(childNode,queryByIdx,queryByPid,tbl);
        }
        Map map = new HashMap<>();
        
        if(beforeDelete.size()>0&&afterDelete.size()==0){
            map.put("message", "操作成功");
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("message", "操作失败");
        return map;
        
    }
    
    /**
     * 供应商树拖拽
     * @param param
     * @return
     */
    public List updateTree(Map param){
    	Map tree_array = new HashMap<>();
    	List list = new ArrayList<>(); 
    	List resultList = new ArrayList<>();
    	list =  (List) param.get("tree_array");
    	for(Object m : list){
    		tree_array = (Map) m;
    		resultList.add(dao.update("t_suppliers",tree_array)); 
    	}
		return resultList;
    	
    }
}
