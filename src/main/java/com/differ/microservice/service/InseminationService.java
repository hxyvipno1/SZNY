/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.differ.microservice.core.dao.StepDao;

/**
 * 授精站service
 * @author dunfang.zhou
 * @version $Id: InseminationService.java, v 0.1 2017年9月1日 下午2:44:26 dunfang.zhou Exp $
 */
@Service
public class InseminationService {

private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private StepDao dao;
    
    public List queryByIdx(Map param) {
        return dao.loadListByCode("LD_INSEMINATION_IDX", param);
    }
  
    /**
     * 查询所有银行
     * @param param
     * @return
     */
    public List findAll(Map param) {
        return dao.loadListByCode("LD_ALLINSEMINATION", param);
    }

    /**
     *删除
     */
    public Map delete(Map param){
        return del(param,"LD_INSEMINATION_IDX","LD_INSEMINATION_PID","t_insemination");
    }
    
    public Map del(Map param,String queryByIdx,String queryByPid,String tbl){
        List beforeDelete =  dao.loadListByCode(queryByIdx, param);
        dao.remove(tbl, param);
        List afterDelete =  dao.loadListByCode(queryByIdx, param);
        Object pid = param.get("idx");
        Map childParam = new HashMap<>();
        childParam.put("pid", pid);
        List list =  dao.loadListByCode(queryByPid, childParam);
        for (Object child : list) {
            String str = child.toString();
            String[] str1 = str.split(",");
            String[] str2 = str1[0].split("=");
            String childIdx = str2[1];
            Map childNode = new HashMap<>();
            childNode.put("idx", childIdx);
            dao.remove(tbl, childNode);
            del(childNode,queryByIdx,queryByPid,tbl);
        }
        Map map = new HashMap<>();
        
        if(beforeDelete.size()>0&&afterDelete.size()==0){
            map.put("message", "操作成功");
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("message", "操作失败");
        return map;
        
    }
}
