package com.differ.microservice.service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.util.DateTimeUtil;
import com.differ.microservice.core.util.QueryResult;
import com.differ.microservice.dao.BusinessDao;
import com.differ.microservice.model.Business;

/**
 * 业务类型service类
 */

@Service
public class BusinessService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    // 封装好的通用的Dao(也可以自己定义)

    @Autowired
    private BusinessDao  busiDao;

    /**
     * 添加
     * 
     * @param entity 实体类
     * @return 受影响的行数
     */
   /* 
    public int add(Business entity) {
        if (null == entity)
            return 0;
        entity.setCreate_time(DateTimeUtil.getNowDateTimeStr());
        return busiDao.save(entity);
    }*/

    /**
     * 修改
     * 
     * @param entity 实体类
     * @return 受影响的行数
     */
    
    /*public int update(Business entity) {
        if (null == entity)
            return 0;
        entity.setCreate_time(DateTimeUtil.getNowDateTimeStr());
        return busiDao.save(entity);
    }*/
    
    
    /**
     * 添加
     * 
     * @param entity 实体类
     * @return 受影响的行数
     */
    
    /*public int add1(Business entity) {
        if (null == entity)
            return 0;
        entity.setCreate_time(DateTimeUtil.getNowDateTimeStr());
        return busiDao.save(entity);
    }
    */
    
    
    /**
     * 分页查询
     * 
     * @param pageIndex 页码
     * @param pageSize 每页数量
     * @return QueryResult 结果集
     *//*
    public QueryResult<Business> list(String pageIndex, String pageSize) {

        return busiDao.findByPage(Integer.parseInt(pageIndex), Integer.parseInt(pageSize));
    }*/
    /**
     * 分页查询，带条件，带排序
     * 
     * @param entity 实体类
     * @param pageIndex 页码
     * @param pageSize 每页数量
     * @return QueryResult 结果集
     */
   /* public QueryResult<Business> list(String pageIndex, String pageSize, Business entity) {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        map.put("create_time", "desc");
        return busiDao.findByPage(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), entity,
            map);
    }*/

}
