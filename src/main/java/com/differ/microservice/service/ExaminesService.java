/**
 * 
 */
package com.differ.microservice.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 审核
 * 
 * @author zhenshan.xu
 * @version $Id: ExaminesService.java, v 0.1 2017年10月30日 上午10:20:56 zhenshan.xu
 *          Exp $
 */
@Service
public class ExaminesService {

	@Autowired
	private StepDao dao;
	@Autowired
	private InventoryService inventory;

	/**
	 * @param param
	 * @return
	 */
	public Map people(Map param) {

		return dao.loadRowByCode("LD_FUNCTIONARY", param);
	}

	public List query(Map param) {

		return dao.loadListByCode("LD_APPLY", param); 
	}
	

	public Map reply(Map param) {
		// 获取main_idx 到 idx 进行修改 t_inventory
		param.put("idx", param.get("main_idx"));
		Map applyfor = dao.update("t_apply_for", param);
		Map mainIdx = new HashMap<>();
		// 通过main_idx删除物资详情列表信息
		mainIdx.put("main_idx", param.get("main_idx"));
		dao.excuteByCode("RM_APPLY_DETAIL", mainIdx);
		List result = (List) param.get("product_lists");

		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
			String main_idx = element.get("product_name").toString();
			// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
			if (main_idx == null || main_idx.equals("")) {
				continue;
			}
			Map idx = new HashMap<>();
			// 赋值main_idx到idx中;
			idx.put("idx", main_idx);
			// 根据main_idx查询产品信息
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			String unit_price = product.get("unit_price").toString();
			// 计算总金额
			int number = (int) element.get("quantity");
			long price = new Long(unit_price).longValue();
			long quantity = new Long(number).longValue();
			int money_count = (int) (price * quantity);
			element.put("unit_price", price);// 单价
			// 总金额
			element.put("money_count", money_count);
			// 申请数量
			element.put("quantity", number);
			element.put("main_idx", applyfor.get("idx"));
			// 获取物资的idx
			element.put("product_idx", product.get("idx"));
			// 获取物资的name
			element.put("product_name", product.get("product_name"));
			// 获取物资的保修期
			element.put("warranty_date", product.get("warranty_date"));
			dao.add("t_apply_detail", element);
			// 计算当前结余量
			int stock = inventory.inventoryNum(product);
			element.put("surplus_num", stock);
			element.put("idx", param.get("rowIdx"));
			element.put("surplus_num", stock);
			// 获取时间
			element.put("occur_time",param.get("occur_time"));
			String reply=param.get("results").toString();
			// 保存状态:审核
			if(reply.equals("1")) {
				element.put("type", "45");//审核通过
			}
			else if(reply.equals("2")){
				element.put("type", "54");//审核未通过
			}
			element.put("abstracts", param.get("abstracts"));
			// 修改 t_statistics
			dao.update("t_statistics", element);
		}
	
		return applyfor;
		
	}
}
