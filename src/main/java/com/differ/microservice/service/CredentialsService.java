/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: CredentialsService.java, v 0.1 2017年8月28日 下午3:06:06 dunfang.zhou Exp $
 */
@Service
public class CredentialsService {

    
    @Autowired
    private StepDao dao;
    
    public Map findAll(Map param) {
        Map paramCre = new HashMap<>();
        Map map = new HashMap<>();
        Map mapCre = new HashMap<>();
        Map mapAni = new HashMap<>();
        boolean checkAni = false;
        List aList = new ArrayList<>();
        paramCre.put("code", "CRE_TYPE");
        List creList = dao.loadListByCode("LD_DIC_CODE1", paramCre);
        String cre = creList.get(0).toString();
        String[] c = cre.split(",");
        String[] c1 = c[0].split("=");
        String pid = c1[1];
        mapCre.put("pid", pid);
        paramCre.put("code", "ANI_TYPE");
        List aniList = dao.loadListByCode("LD_DIC_CODE1", paramCre);
        String are = aniList.get(0).toString();
        String[] a = are.split(",");
        String[] a1 = a[0].split("=");
        String pidAni = a1[1];
        mapAni.put("pid", pidAni); 
        List anis = dao.loadListByCode("LD_CHILDDIC", mapAni);
        List cres = dao.loadListByCode("LD_CREDENTIALS", param);
        List cList = dao.loadListByCode("LD_CREDENTIALS", param);
        for (Object ccc : anis) {
            String[] cccc = ccc.toString().split(",");
            String aa2 = null;
            String reg = "[^\u4e00-\u9fa5]";
            aa2 = cccc[2].replaceAll(reg, "");
            for (Object obj : cList) {
                String[] bb = obj.toString().split(",");
                String[] bb1 = bb[1].split("=");
                String bb2 = bb1[1];
                if(aa2.equals(bb2)){
                    checkAni = true;
                }
            }
            if(!checkAni){
                Map mm = new HashMap<>();
                mm.put("animal_type", aa2);
                mm.put("credentials", "");
                aList.add(mm);
            }
            checkAni = false;
        }
        List cList1 = dao.loadListByCode("LD_CREDENTIALS", param);
        TreeSet  set = new TreeSet<>();
        for (Object object : aList) {
            cList1.add(object);
           
        }
        for (Object obj : cList1) {
            String[] bb = obj.toString().split(",");
            String[] bb1 = bb[1].split("=");
            String bb2 = bb1[1];
            set.add(bb2);
        }
        Iterator it = set.iterator();
        List sortList = new ArrayList<>();
        while(it.hasNext()){
            String name = (String) it.next();
            for (Object obj : cList1) {
                String[] bb = obj.toString().split(",");
                String[] bb1 = bb[1].split("=");
                String bb2 = bb1[1];
                if(bb2.equals(name)){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                    sortList.add(obj);
                }
            }
        }
        
        map.put("ths",dao.loadListByCode("LD_CHILDDIC", mapCre));
        map.put("tds",sortList);
        return map;
    }
    
    public List queryCredentials(Map param){
        return dao.loadListByCode("LD_CRES", param);
        
    }
}
