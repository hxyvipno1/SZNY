/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 物资调拨
 * 
 * @author zhenshan.xu
 * @version $Id: TransfersService.java, v 0.1 2017年11月18日 上午11:04:05 zhenshan.xu
 *          Exp $
 */
@Service
public class TransfersService {
	@Autowired
	private StepDao dao;
	@Autowired
	private InventoryService inventory;

	public Map query(Map param) {

		return param;
	}

	/**
	 * 调拨
	 * 
	 * @param param
	 * @return
	 */
	public Map storage(Map param) {
		//提取business_types转换为String类型
		String types = param.get("business_types").toString();
		int size = 0;
		StringBuffer product_name = new StringBuffer();
		List result = (List) param.get("product_lists");
		if (types.equals("2")) {

			for (int i = 0; i < result.size(); i++) {
				Map element = (Map) result.get(i);
				String main_idx = element.get("product_name").toString();
				if (main_idx == null || main_idx.equals("")) {
					continue;
				}
				Map idx = new HashMap<>();
				idx.put("idx", main_idx);
				int quantity = (int) element.get("quantity");
				Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
				int stock = inventory.inventoryNum(product);
				if (stock >= quantity) {
					size = size + 1;
				} else {
					Map map = new HashMap<>();
					map.put("number", "库存剩余 ：" + stock);
					product_name = product_name.append(product.get("product_name").toString());
					product_name = product_name.append(map.get("number").toString() + ",");
					continue;
				}
			}
		}else if (types.equals("1")){
			//判断为调入size赋值为result.size()大小;
			size = result.size();
		}
		Map transfers = new HashMap<>();
		if (size == result.size()) {
			// 保存到 t_transfers 表;
			transfers = dao.add("t_transfers", param);
			for (int i = 0; i < result.size(); i++) {
				// 获取 List 中的 String 数组元素;
				Map element = (Map) result.get(i);
				// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
				String main_idx = element.get("product_name").toString();
				// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
				if (main_idx == null || main_idx.equals("")) {
					continue;
				}
				Map idx = new HashMap<>();
				// 赋值main_idx到idx中;
				idx.put("idx", main_idx);
				// 根据main_idx查询产品信息
				Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
				element.put("main_idx", transfers.get("idx"));
				// 获取物资的idx
				element.put("product_idx", product.get("idx"));
				element.put("product_name", product.get("product_name"));
				Map parameter = new HashMap<>();
				parameter.put("quantity", element.get("quantity"));
				parameter.put("line_abstracts1", element.get("line_abstracts1"));
				String endNo=inventory.computations(parameter);
				element.put("line_abstracts1",endNo);
				// 保存物资信息1:调入
				if (types.equals("1")) {
					Map map = dao.add("t_inventory_detail", element);
				}//2:调出
				if (types.equals("2")) {
					Map map = dao.add("t_outbound_detail", element);
				}
				// 保存物资信息
				// Map map = dao.add("t_goods_details", element);
				// 计算当前结余量
				int stock = inventory.inventoryNum(product);
				element.put("surplus_num", stock);
				// 获取时间
				element.put("occur_time", param.get("storage_time"));
				// 保存状态:调拨
				element.put("type", "7");
				element.put("people_idx",  param.get("idx"));
				element.put("residential_site", param.get("district_name"));
				element.put("document_num",  param.get("document_num"));
				element.put("abstracts", param.get("abstracts"));
				dao.add("t_statistics", element);
			}
			transfers.put("code", 1);
			transfers.put("message", "操作成功");
		} else {
			transfers.put("code", 0);
			transfers.put("message", "操作失败");
			transfers.put("product_name", product_name.substring(0, product_name.length() - 1));
		}
		return transfers;
	}

	/**
	 * 修改物资调拨
	 * 
	 * @param param
	 * @return
	 */
	public Map update(Map param) {
		// 获取main_idx 到 idx 进行修改 t_inventory
		param.put("idx", param.get("main_idx"));
		Map transfers = dao.update("t_transfers", param);
		Map mainIdx = new HashMap<>();
		// 通过main_idx删除物资详情列表信息
		mainIdx.put("main_idx", param.get("main_idx"));
		Map types = (Map) param.get("business_types");
		if (types.equals("1")) {
			dao.excuteByCode("RM_INV_DETAIL", mainIdx);
		}
		if (types.equals("2")) {
			dao.excuteByCode("RM_OUT_DETAIL", mainIdx);
		}
		// dao.excuteByCode("RM_GOODS_DETAIL", mainIdx);
		List result = (List) param.get("product_lists");

		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
			String main_idx = element.get("product_name").toString();
			// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
			if (main_idx == null || main_idx.equals("")) {
				continue;
			}
			Map idx = new HashMap<>();
			// 赋值main_idx到idx中;
			idx.put("idx", main_idx);
			// 根据main_idx查询产品信息
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			element.put("main_idx", transfers.get("idx"));
			// 获取物资的idx
			element.put("product_idx", product.get("idx"));
			element.put("product_name", product.get("product_name"));
			Map parameter = new HashMap<>();
			parameter.put("quantity", element.get("quantity"));
			parameter.put("line_abstracts1", element.get("line_abstracts1"));
			String endNo=inventory.computations(parameter);
			element.put("line_abstracts1",endNo);
			// 保存物资信息
			if (types.equals("1")) {
				Map map = dao.add("t_inventory_detail", element);
			}
			if (types.equals("2")) {
				Map map = dao.add("t_outbound_detail", element);
			}
			// 保存物资信息
			// Map map = dao.add("t_goods_details", element);
			// 计算当前结余量
			int stock = inventory.inventoryNum(product);
			element.put("surplus_num", stock);
			element.put("idx", param.get("rowIdx"));
			element.put("surplus_num", stock);
			// 获取时间
			element.put("occur_time", param.get("storage_time"));
			// 保存状态:调拨
			element.put("type", "7");
			element.put("abstracts", param.get("abstracts"));
			// 修改 t_statistics
			dao.update("t_statistics", element);
		}

		return transfers;
	}

	/**
	 * 查看详情
	 * 
	 * @param param
	 * @return
	 */
	public List details(Map param) {

		List list = new ArrayList<>();
		List resultList = new ArrayList<>();
		Map map = dao.loadRowByCode("LD_TRANSFERS", param);
		String type = (String) map.get("business_types");
		if (type.equals("1")) {//判断业务状态为调入
			resultList = dao.loadListByCode("LD_TRANSFERS_IN", param);
		}
		if (type.equals("2")) {//判断业务状态为调入
			resultList = dao.loadListByCode("LD_TRANSFERS_OUT", param);
		}
		List product_name = dao.loadListByCode("LD_PRODUCT_NAME", param);
		for (Object result : resultList) {
			Map details = new HashMap<>();
			details.put("nameArr", product_name);
			details.put("value", ((Map) result).get("product_idx"));
			((Map) result).put("product_name", details);
			list.add(result);
		}
		return list;
	}

}
