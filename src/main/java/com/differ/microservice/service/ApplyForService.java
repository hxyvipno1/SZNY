/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: ApplyForService.java, v 0.1 2017年9月16日 上午10:28:38 zhenshan.xu
 *          Exp $
 */
@Service
public class ApplyForService {
	@Autowired
	private StepDao dao;
	
	@Autowired
	private InventoryService inventory;


	/**
	 * 申请人信息
	 * @param param
	 * @return
	 */
	public Map query(Map param) {

		return dao.loadRowByCode("LD_HUSBANDRY_PEOPLE", param);
	}
	
	public List examines(Map param) {

		return dao.loadListByCode("LD_APPLICATION_AUDIT", param);
	}
	/**
	 * 物资申请
	 * @param param
	 * @return
	 */
	public Map report(Map param) {
		// 保存到 t_outbound 表;
		param.put("applicant_idx",param.get("people_idx"));
		Map apply = dao.add("t_apply_for", param);
		List result = (List) param.get("product_lists");

		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
			String main_idx = element.get("product_name").toString();
			// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
			if (main_idx == null || main_idx.equals("")) {
				continue;
			}
			Map idx = new HashMap<>();
			// 赋值main_idx到idx中;
			idx.put("idx", main_idx);
			// 根据main_idx查询产品信息
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			String unit_price = product.get("unit_price").toString();
			// 计算总金额
			int number = (int) element.get("quantity");
			long price = new Long(unit_price).longValue();
			long quantity = new Long(number).longValue();
			int money_count = (int) (price * quantity);
			// 单价
			element.put("unit_price", price);
			// 总金额
			element.put("money_count", money_count);
			// 申请数量
			element.put("quantity", number);
			element.put("main_idx", apply.get("idx"));
			// 获取物资的idx
			element.put("product_idx", product.get("idx"));
			// 获取物资的name 
			element.put("product_name", product.get("product_name"));
			// 获取物资的保修期
			element.put("warranty_date", product.get("warranty_date"));
			// 保存物资信息
			Map map = dao.add("t_goods_details", element);
			// 计算当前结余量
			int stock = inventory.inventoryNum(product);
			element.put("surplus_num", stock);
			// 获取时间
			element.put("occur_time", param.get("occur_time"));
			// 保存状态:申报
			element.put("type", "4");
			element.put("people_idx",  param.get("people_idx"));
			element.put("residential_site", param.get("district_name"));
			element.put("document_num",  param.get("document_num"));
			element.put("abstracts", param.get("abstracts"));
			dao.add("t_statistics", element);
		}
		return apply;
	}
	/**
	 * 查看详情
	 * @param param
	 * @return
	 */
	public List details(Map param){
		Map idx = new HashMap<>();
		List list = new ArrayList<>();
		idx.put("main_idx", param.get("main_idx"));
		List resultList = dao.loadListByCode("LD_APPLY_FOR", idx);
		List product_name = dao.loadListByCode("LD_PRODUCT_NAME", param);
		for (Object result : resultList) {
			Map details = new HashMap<>();
			details.put("nameArr", product_name);
			details.put("value", ((Map) result).get("product_idx"));
			((Map) result).put("product_name",details);
			list.add(result);
		}
		return list;
	}
	
	/**
	 * 修改物资申报
	 * 
	 * @param param
	 * @return
	 */
	public Map update(Map param) {
		//获取main_idx 到 idx 进行修改 t_inventory
		param.put("idx", param.get("main_idx"));
		Map apply = dao.update("t_apply_for", param);
		Map mainIdx = new HashMap<>();
		//通过main_idx删除物资详情列表信息
		mainIdx.put("main_idx", param.get("main_idx"));
		dao.excuteByCode("RM_GOODS_DETAIL", mainIdx);
		List result = (List) param.get("product_lists");
		
		for (int i = 0; i < result.size(); i++) {
			// 获取 List 中的 String 数组元素;
			Map element = (Map) result.get(i);
			// 获取 element 中 product_name 元数, 赋值到 main_idx 中 ;
			String main_idx = element.get("product_name").toString();
			// 判断 main_idx 中参数为 null 或为 空字符串 ,跳出 判断 ;
			if (main_idx == null || main_idx.equals("")) {
				continue;
			}
			Map idx = new HashMap<>();
			// 赋值main_idx到idx中;
			idx.put("idx", main_idx);
			// 根据main_idx查询产品信息
			Map product = dao.loadRowByCode("LD_PRODUCT_IDX", idx);
			String unit_price = product.get("unit_price").toString();
			// 计算总金额
			int number = (int) element.get("quantity");
			long price = new Long(unit_price).longValue();
			long quantity = new Long(number).longValue();
			int money_count = (int) (price * quantity);
			// 获取单价信息
			element.put("unit_price", price);
			// 总金额
			element.put("money_count", money_count);
			// 申请数量
			element.put("quantity", number);
			element.put("main_idx", apply.get("idx"));
			// 获取物资的idx
			element.put("product_idx", product.get("idx"));
			// 获取物资的name 
			element.put("product_name", product.get("product_name"));
			// 获取物资的保修期
			element.put("warranty_date", product.get("warranty_date"));
			// 保存物资信息
			Map map = dao.add("t_goods_details", element);
			//计算当前结余量
			int stock = inventory.inventoryNum(product);
			element.put("surplus_num", stock);
			element.put("idx", param.get("rowIdx"));
			element.put("surplus_num", stock);
			//获取时间
			element.put("occur_time",param.get("storage_time"));
			//保存状态:申报
			element.put("type", "4");
			element.put("residential_site", param.get("residential_site"));
			element.put("abstracts", param.get("abstracts"));
			//修改 t_statistics
			dao.update("t_statistics", element);
		}
		
		return apply;
	}
	
}
