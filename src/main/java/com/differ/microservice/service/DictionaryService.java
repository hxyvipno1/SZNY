/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: DictionaryService.java, v 0.1 2017年7月28日 上午9:20:05 dunfang.zhou Exp $
 */
@Service
public class DictionaryService {
    
 private final Logger logger = LoggerFactory.getLogger(this.getClass());

    
    
    @Autowired
    private StepDao stepDao;
    
    /**
     * 查询全部
     */
    public List findAll(){
        
        return stepDao.loadListByCode("LD_ALLDIC", null);
        
    }
    
    /**
     * 通过idx查询节点
     */
    public List findByIdx(Map param){
        
        return stepDao.loadListByCode("LD_DIC", param);
        
    }

    /**
     *通过pid查询子节点
     */
    public List findByPid(Map param){
        
        return stepDao.loadListByCode("LD_CHILDDIC", param);
        
    }
    
 public List findByType(Map param){
        
        return stepDao.loadListByCode("LD_CHILDDIC_TYPE", param);
        
    }
    
    /**
     *校验code 是否重复
     */
    public Map findByCode(Map param){
        List list = stepDao.loadListByCode("LD_DIC_CODE", param);
        Map map = new HashMap<>();
        if(list.size()>1){
            map.put("valid", false);
            return map;
        }
        String str1 = param.get("code").toString();
        for (Object obj : list) {
            String str2 = obj.toString();
            String[] str = str2.split("=");
            String[] ss = str[1].split("}");
            if(str1.equals(ss[0])){
                map.put("valid", false);
                return map;
            }
        }
        map.put("valid", true);
        return map;
        
    }
    
    /**
     *校验name 是否重复
     */
    public Map findByName(Map param){
        List list = stepDao.loadListByCode("LD_DIC_NAME", param);
        Map map = new HashMap<>();
        if(list.size()>1){
            map.put("valid", false);
            return map;
        }
        String str1 = param.get("name").toString();
        for (Object obj : list) {
            String str2 = obj.toString();
            String[] str = str2.split("=");
            String[] ss = str[1].split("}");
            if(str1.equals(ss[0])){
                map.put("valid", false);
                return map;
            }
        }
        map.put("valid", true);
        return map;
        
    }
    
    /**
     *通过code查询节点
     */
    public List findByCode1(Map param){
        
        return stepDao.loadListByCode("LD_DIC_CODE1", param);
        
    }
    
    /**
     *删除
     */
    public Map delete(Map param){
        return del(param,"LD_DIC","LD_CHILDDIC","t_dictionary");
    }
    
    public Map del(Map param,String queryByIdx,String queryByPid,String tbl){
        List beforeDelete =  stepDao.loadListByCode(queryByIdx, param);
        stepDao.remove(tbl, param);
        List afterDelete =  stepDao.loadListByCode(queryByIdx, param);
        Object pid = param.get("idx");
        Map childParam = new HashMap<>();
        childParam.put("pid", pid);
        List list =  stepDao.loadListByCode(queryByPid, childParam);
        for (Object child : list) {
            String str = child.toString();
            String[] str1 = str.split(",");
            String[] str2 = str1[0].split("=");
            String childIdx = str2[1];
            Map childNode = new HashMap<>();
            childNode.put("idx", childIdx);
            stepDao.remove(tbl, childNode);
            del(childNode,queryByIdx,queryByPid,tbl);
        }
        Map map = new HashMap<>();
        
        if(beforeDelete.size()>0&&afterDelete.size()==0){
            map.put("message", "操作成功");
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("message", "操作失败");
        return map;
        
    }
    
    
    /**
     * 查询附件
     * @param param
     * @return
     */
    public List findChild(Map param){
        
        return stepDao.loadListByCode("LD_DIC_CHILD", param);
    }
    
}
