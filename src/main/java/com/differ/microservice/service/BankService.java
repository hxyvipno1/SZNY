/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 银行Service类
 * @author zhenshan.xu
 * @version $Id: BankService.java, v 0.1 2017年8月20日 上午8:37:28 zhenshan.xu Exp $
 */
@Service
public class BankService {
	
	@Autowired
	private StepDao dao;
	
	public List queryByIdx(Map param) {
		return dao.loadListByCode("LD_BANK", param);
	}
	 /**
     * 查询银行名称
     * @param param
     * @return
     */
    public List queryName(Map param) {

        return dao.loadListByCode("LD_BANK1", param);
    }
    /**
     * 查询银行名称
     * @param param
     * @return
     */
    public List queryNames(Map param) {

        return dao.loadListByCode("LD_ALLBANKNAME", param);
    }
    /**
     * 查询能繁母猪与生猪的头数
     * @param param
     * @return
     */
    public Map queryCount(Map param) {
        Map resultMap = new HashMap<>();
        int pig = 0, cow = 0,minnum = 0,maxnum = 0;
        List list = new ArrayList<>();
        boolean p1=false,p2=false,p3=false,p4=false;
        for(int i=0;i<=150;i=i+50){
            minnum = i;
            maxnum = i+50;
            param.remove("minnum");
            param.remove("maxnum");
            param.put("minnum", minnum);
            param.put("maxnum", maxnum);
             list = dao.loadListByCode("LD_LIVE_COUNT", param);
            if(!list.isEmpty())
            for (Object object : list) {
                Map map = (Map)object;
                if(map.get("production_type")==null){
                    cow = cow + Integer.parseInt(String.valueOf(map.get("num")));
                }else if(map.get("production_type").equals("4")){
                    cow = cow + Integer.parseInt(String.valueOf(map.get("num")));
                }else if(map.get("production_type").equals("2")){
                    if(minnum==50){
                        p2=true;
                        resultMap.put("pigLessHundred", Integer.parseInt(String.valueOf(map.get("num"))));
                    }else if(minnum==100){
                        resultMap.put("pigLessOneF", Integer.parseInt(String.valueOf(map.get("num"))));
                        p3=true;
                    }else if(minnum==0){
                        p1=true;
                        resultMap.put("pigLessFifty", Integer.parseInt(String.valueOf(map.get("num"))));
                    }
                    pig = pig + Integer.parseInt(String.valueOf(map.get("num")));
                }
            }
        }
        param.remove("minnum");
        param.remove("maxnum");
        param.put("minnum", 150);
        param.put("maxnum", 1000000);
        list = dao.loadListByCode("LD_LIVE_COUNT", param);
        if(!list.isEmpty())
            for (Object object : list) {
                Map map = (Map)object;
                if(map.get("production_type")==null){
                    cow = cow + Integer.parseInt(String.valueOf(map.get("num")));
                }else if(map.get("production_type").equals("4")){
                    cow = cow + Integer.parseInt(String.valueOf(map.get("num")));
                }else if(map.get("production_type").equals("2")){
                    resultMap.put("pigMoreOneF", Integer.parseInt(String.valueOf(map.get("num"))));
                    pig = pig + Integer.parseInt(String.valueOf(map.get("num")));
                    p4=true;
                }
            }
        if(!p1)
            resultMap.put("pigLessFifty",0); 
        if(!p2)
            resultMap.put("pigLessHundred",0);
        if(!p3)
            resultMap.put("pigLessOneF",0);
        if(!p4)
            resultMap.put("pigMoreOneF",0);
        String result = "";
        if(pig!=0){
            result = reducing(cow, pig);
        }else{
            result = cow+"/"+pig;
        }
        resultMap.put("pig", pig);
        resultMap.put("cow", cow);
        resultMap.put("per", result);
        return resultMap;
    }
    
    //约分
    public String reducing(int cow,int pig){
        int smaller = pig>cow?pig:cow;
        int maxCommonFactor = 1;
        for (int i = 1; i <= smaller; i++) {
            if(pig%i==0 && cow%i==0){
                maxCommonFactor = i;
            }
        }
        String result = cow/maxCommonFactor+"/"+pig/maxCommonFactor;
        return result;
        
    }
    
	/**
	 * 查询所有银行    
	 * @param param
	 * @return
	 */
	public List findAll(Map param) {
		return dao.loadListByCode("LD_ALLBANK", param);
	}

    /**
     *删除
     */
    public Map delete(Map param){
        return del(param,"LD_BANK_IDX","LD_BANK_PID","t_bank");
    }
    
    public Map del(Map param,String queryByIdx,String queryByPid,String tbl){
        List beforeDelete =  dao.loadListByCode(queryByIdx, param);
        dao.remove(tbl, param);
        List afterDelete =  dao.loadListByCode(queryByIdx, param);
        Object pid = param.get("idx");
        Map childParam = new HashMap<>();
        childParam.put("pid", pid);
        List list =  dao.loadListByCode(queryByPid, childParam);
        for (Object child : list) {
            String str = child.toString();
            String[] str1 = str.split(",");
            String[] str2 = str1[0].split("=");
            String childIdx = str2[1];
            Map childNode = new HashMap<>();
            childNode.put("idx", childIdx);
            dao.remove(tbl, childNode);
            del(childNode,queryByIdx,queryByPid,tbl);
        }
        Map map = new HashMap<>();
        if(beforeDelete.size()>0&&afterDelete.size()==0){
            map.put("message", "操作成功");
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("message", "操作失败");
        return map;
        
    }
    
    /**
     * 银行树拖拽
     * @param param
     * @return
     */
    public List updateTree(Map param){
    	Map tree_array = new HashMap<>();
    	List list = new ArrayList<>(); 
    	List resultList = new ArrayList<>();
    	list =  (List) param.get("tree_array");
    	for(Object m : list){
    		tree_array = (Map) m;
    		resultList.add(dao.update("t_bank",tree_array)); 
    	}
		return resultList;
    	
    }
    
}
