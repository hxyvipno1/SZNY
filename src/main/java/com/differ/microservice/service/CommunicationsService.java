/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: CommunicationsService.java, v 0.1 2017年12月4日 上午11:06:00 zhenshan.xu Exp $
 */
@Service                           
public class CommunicationsService {
	
	@Autowired
	private StepDao dao;
	@Autowired
	private DistrictService dis;
	/**
	 * 查询所有通讯人员 
	 * @param param
	 * @return
	 */
	public List findAll(Map param) {
		List details = new ArrayList<>();
		List results = dao.loadListByCode("LD_COMMUNICATIONS", param);
		for (int i = 0; i < results.size(); i++) {
			Map element = (Map) results.get(i);
			String area = dis.DistrictName(element.get("area").toString());
			element.put("area", area);
			details.add(element);
		}
		return details;
	}
}
