/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: AccountService.java, v 0.1 2017年9月16日 下午2:24:52 dunfang.zhou Exp $
 */
@Service
public class AccountService {

    @Autowired
    private StepDao dao;
    
    public List findAllFeed(Map map) {
        return dao.loadListByCode("LD_FEED", map);

    }
    
    public List findAllPurchase(Map map) {
        return dao.loadListByCode("LD_PURCHASE", map);

    }
}
