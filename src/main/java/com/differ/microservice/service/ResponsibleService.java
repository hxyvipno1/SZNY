/**
 * 
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 专管人员
 * @author zhenshan.xu
 * @version $Id: ResponsibleService.java, v 0.1 2017年9月3日 上午9:34:48 zhenshan.xu Exp $
 */
@Service
public class ResponsibleService {
	@Autowired
	private StepDao dao;	
	@Autowired
	private DistrictService dis;
	/**
	 * @param param
	 * @return
	 */
	public List findAll(Map param) {
		List list = dao.loadListByCode("LD_ADMINISTER", param);
		List resultList = new ArrayList<>();
		for (Object object : list) {
			Map map = new HashMap<>();
			map = (Map) object;
			String working_area =  dis.DistrictName(map.get("working_area").toString());
			map.put("working_area", working_area);
			resultList.add(map);
	}
	
		return resultList;
		
	}
	
	public Map add (Map param){
		
		String placeId =  (String) param.get("placeId").toString();
		placeId= placeId.toString();
		String working_area = placeId.substring(1,(placeId).length()-1);
		String[] str = working_area.split(",");
		int[] arr = new int[str.length];
		for(int i=0;i<str.length;i++){
			arr[i]=Integer.parseInt(str[i].trim());
		}
		int[] arr1 = selectSort(arr);
		String s = "";
		for (int i=0;i<arr1.length ;i++) {
			if(i==0){
				s = s+String.valueOf(arr1[i]);
			}else{
				int a = arr1[i];
				s = s+","+String.valueOf(a);
			}
		}
		param.put("working_area", s);
		return dao.add("t_people_responsible", param);
	}
	
	 //排序
    public  int[] selectSort(int[]a)
    {
        int maxIndex=0;
        int temp=0;
        if((a==null)||(a.length==0))
            return null;
        for(int i=0;i<a.length-1;i++)
        {
            maxIndex=i;//无序区的最大数据数组下标
            for(int j=i+1;j<a.length;j++)
            {
                //在无序区中找到最大数据并保存其数组下标
                if(a[j]<a[maxIndex])
                {
                    maxIndex=j;
                }
            }
            if(maxIndex!=i)
            {
                //如果不是无序区的最大值位置不是默认的第一个数据，则交换之。
                temp=a[i];
                a[i]=a[maxIndex];
                a[maxIndex]=temp;
            }
        }
        return a;
    }
	
}