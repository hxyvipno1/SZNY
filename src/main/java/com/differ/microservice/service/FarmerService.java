/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: FarmerService.java, v 0.1 2017年7月11日 上午11:26:58 dunfang.zhou Exp $
 */
@Service
public class FarmerService {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    //封装好的通用的Dao(也可以自己定义)
    @Autowired
    private StepDao      dao;

    //通过ID查询列表方法
    public List<String> queryByIdx(String idx) {
        //第一个参数：表名，第二个参数：主键
        Map row = dao.loadRT("t_customer", idx);
        return new ArrayList<String>(row.values());
    }

    //list方法
    public List<String> query() {
        //第一个参数：sql语句，第二个参数：Map，如果有多个参数，分别放到map里，如：map.put("user_name","");map.put("password","")
        return dao.loadListByCode("LD_CUSTOMER", null);
    }
    
    public Map login(Map map) {
        //第一个参数：sql语句，第二个参数：Map，如果有多个参数，分别放到map里，如：map.put("user_name","");map.put("password","")
        return dao.loadRowByMap("select * from t_customer",map);
    }

}
