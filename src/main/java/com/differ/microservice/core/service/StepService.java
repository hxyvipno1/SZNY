package com.differ.microservice.core.service;

import com.differ.microservice.core.dao.StepDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cailm on 2017-07-06.
 */

@Service
public class StepService {

    @Autowired
    private StepDao dao;

	public Map addEntity(String tbl,Map entity){
    	return dao.add(tbl, entity);
    }
    
    @Transactional
    public Map doStep(Map steps){
        Map rmp=new LinkedHashMap();
        List list = (List)steps.get("step");
        for(Object step:list){
            Map stmp = (Map)step;
            String jscode = (String)stmp.get("JSCODE");
            if(StringUtils.equals("LOAD", jscode))
                this.load(stmp,rmp);
            else
                this.run(stmp,rmp);
        }
        return rmp;
    }

    private void load(Map stmp,Map rmp){
        String code = (String)stmp.get("CODE");
        String rcode=(String)stmp.get("RCODE");
        Map data=(Map)stmp.get("DATA");
        Map page=(Map)stmp.get("PAGE");
        
        if(StringUtils.isBlank(rcode)) rcode=code;
        if(page==null){        
        	List list=dao.loadListByCode(code, data);
        	rmp.put(rcode, list);
        }else{
        	Map prmp=dao.loadPage(code,data,page);
			rmp.put(rcode, prmp);
        }
    }

    private void run(Map stmp,Map rmp){
        String code = (String)stmp.get("CODE");
        String rcode=(String)stmp.get("RCODE");
        String clazz=(String)stmp.get("CLZ");
        String scriptcode=(String)stmp.get("SCRIPT_CODE");
        if(StringUtils.isBlank(rcode)) rcode=code;

        Map data=(Map)stmp.get("DATA");

        if(StringUtils.equals("ADD", code)){
            rmp.put(rcode, dao.add(clazz,data));
        }else if(StringUtils.equals("UPDATE", code)){
            rmp.put(rcode, dao.update(clazz,data));
        }else if(StringUtils.equals("REMOVE", code)){
        	dao.remove(clazz, data);
        	rmp.put(rcode, data);
        }else if(StringUtils.equals("EXECUTE",code)){
        	dao.excuteByCode(scriptcode, data);
            rmp.put(rcode, "");
        }else{
            rmp.put(rcode, data);
        }
    }
    
    
    
    public StepDao getDao() {
		return dao;
	}

	public void setDao(StepDao dao) {
		this.dao = dao;
	}

}
