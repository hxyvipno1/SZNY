package com.differ.microservice.core.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.differ.microservice.core.dao.StepDao;
import com.differ.microservice.core.util.CacheUtil;
import com.differ.microservice.core.util.Encryption;

@Service
public class LoginService {
	@Autowired
    private StepDao dao;
	
	public Map doLogin(String code,String pwd){
		Map param=new HashMap();
		param.put("code", code);
		Map user=dao.loadRowByMap("select * from t_user where user_code= :code", param);
		if(user!=null && StringUtils.equals((String)user.get("password"), Encryption.encrypt(pwd))){
			Map cookie=new HashMap();
			cookie.put("uidx", user.get("idx"));
			cookie.put("status", 1);
			dao.add("t_cookie", cookie);
			return cookie;
		}
		return null;
	}
	
	@Transactional
	public void logout(String uid){
		Map cookie=new HashMap();
		cookie.put("idx", uid);
		cookie.put("status", -1);
		dao.update("t_cookie", cookie);
		CacheUtil.instance().removeKey("findCookie_"+uid);
	}
}
