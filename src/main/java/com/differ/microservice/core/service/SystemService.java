package com.differ.microservice.core.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.differ.microservice.core.CONSTANTS;
import com.differ.microservice.core.dao.StepDao;
import com.differ.microservice.core.util.Encryption;

@Service
public class SystemService {
	@Autowired
    private StepDao dao;
	
	@Transactional
	public Map saveuser(Map param){
		Map uinfo=new HashMap();
		Map people=new HashMap();
		uinfo.putAll(param);
		people.putAll(param);
		String idx=(String)uinfo.get("idx");
		if(StringUtils.isBlank(idx)){			
			uinfo.put("password", Encryption.encrypt(CONSTANTS.DEFUALT_PWD));
			uinfo=dao.add("t_user", uinfo);

			people.put("user_idx",uinfo.get("idx"));
			people=dao.add("t_people", people);
		}else{
			uinfo=dao.update("t_user", uinfo);
			String people_idx=(String) param.get("people_idx");
			if(StringUtils.isBlank(people_idx)){
				people.put("user_idx",uinfo.get("idx"));
				people=dao.add("t_people", people);				
			}else{
				people.put("idx",people_idx);
				people.put("user_idx",uinfo.get("idx"));
				people=dao.update("t_people", people);
			}
			
			
		}
		return uinfo;
	}
	
	@Transactional
	public Map resetpwd(Map param){
		String idx=(String)param.get("idx");
		Map u=new HashMap();
		u.put("idx", idx);
		u.put("password",  Encryption.encrypt(CONSTANTS.DEFUALT_PWD));
		dao.update("t_user", u);
		return param;
	}
	
}
