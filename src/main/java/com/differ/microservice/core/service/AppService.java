package com.differ.microservice.core.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;
import com.differ.microservice.core.util.BeanUtil;

@Service
public class AppService {
	@Autowired
    private StepDao dao;
	
	
	public Map initLoad(String uidx){
		Map param=new HashMap();
		param.put("idx", uidx);
		Map user=dao.loadRowByCode("LD_INIT_DATA", param);
		//Map user=dao.loadRT("t_user", uidx);
		return user;
	}
	
	public Map initLoad1(String uidx){
		Map param=new HashMap();
		param.put("idx", uidx);
		List menu=dao.loadListByCode("LD_INIT_ACCESS", param);
		Map user=dao.loadRowByCode("LD_INIT_DATA", param);
		user.put("menu", BeanUtil.list2tree(menu));
		return user;
	}
	
}
