package com.differ.microservice.core.util;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class CacheUtil {
	private static CacheUtil _instance=null;
	public static synchronized CacheUtil instance(){
		if(_instance==null)
			_instance=new CacheUtil();
		return _instance;
	}
	
	final CacheManager cacheManager = new CacheManager();
	final Cache cache = cacheManager.getCache("cookie");
	
	public Object getKey(String key){
		Element element = cache.get(key);
		if(element==null) 
			return null;
		
		return element.getObjectValue();
	}
	
	
	public void putKey(String key,Object value){
    	cache.put(new Element(key,value));
	}
	
	public void removeKey(String key){
		cache.remove(key);
	}
}
