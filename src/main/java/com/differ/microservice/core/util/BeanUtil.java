/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.core.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author liyuran
 * @version $Id: BeanUtil.java, v 0.1 2017年7月18日 上午9:53:57 liyuran Exp $
 */
public class BeanUtil {

    /**
     * javaBean 转 Map
     * @param object 需要转换的javabean
     * @return  转换结果map
     * @throws Exception
     */
    public static Map<String, Object> beanToMap(Object object) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        Class cls = object.getClass();
        Field[] fields = cls.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if(null==field.get(object))
                continue;
            map.put(field.getName(), field.get(object));
        }
        return map;
    }

    /**
     *
     * @param map   需要转换的map
     * @param cls   目标javaBean的类对象
     * @return  目标类object
     * @throws Exception
     */
    public static Object mapToBean(Map<String, Object> map, Class cls) throws Exception {
        Object object = cls.newInstance();
        for (String key : map.keySet()) {
            Field temFiels = cls.getDeclaredField(key);
            temFiels.setAccessible(true);
            temFiels.set(object, map.get(key));
        }
        return object;
    }
    
    public static List list2tree(List list){
    	List children=new ArrayList();
    	
    	Map<String,Map> nd=new HashMap<String,Map>();
    	list.forEach(item->{
			Map rw=(Map)item;
			String idx=(String)rw.get("idx");
			nd.put(idx, rw);
    	});
    	
//    	nd.forEach((key, row)->{
//    		String pidx=(String)row.get("pidx");
//    	});
    	list.forEach(item->{
			Map rw=(Map)item;
			String pidx=(String)rw.get("pidx");
			Map pnode=nd.get(pidx);
			if(pnode==null){
				children.add(rw);
			}else{
				if(pnode.get("children")==null)
					pnode.put("children", new ArrayList());
				((List)pnode.get("children")).add(rw);
			}			
    	});
    	
    	return children;
    }

}
