/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.core.util;

import java.util.HashMap;
import java.util.List;

/**
 * 
 * @author yue.li
 * @version $Id: ResponseMap.java, v 0.1 2017年6月22日 下午2:28:11 yue.li Exp $
 */
public class ResponseMap extends HashMap<String, Object> {

    /**  */
    private static final long serialVersionUID = -2288196668519355879L;

    private int               currPage;
    private List              list;
    private int               pageSize;
    private String            resDateTime;
    private boolean           success;
    private int               totalCount;
    private int               totalPage;

    /**
     * Getter method for property <tt>currPage</tt>.
     * 
     * @return property value of currPage
     */
    public int getCurrPage() {
        return currPage;
    }

    /**
     * Setter method for property <tt>currPage</tt>.
     * 
     * @param currPage value to be assigned to property currPage
     */
    public void setCurrPage(int currPage) {
        this.currPage = currPage;
    }

    /**
     * Getter method for property <tt>list</tt>.
     * 
     * @return property value of list
     */
    public List getList() {
        return list;
    }

    /**
     * Setter method for property <tt>list</tt>.
     * 
     * @param list value to be assigned to property list
     */
    public void setList(List list) {
        this.list = list;
    }

    /**
     * Getter method for property <tt>pageSize</tt>.
     * 
     * @return property value of pageSize
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Setter method for property <tt>pageSize</tt>.
     * 
     * @param pageSize value to be assigned to property pageSize
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Getter method for property <tt>resDateTime</tt>.
     * 
     * @return property value of resDateTime
     */
    public String getResDateTime() {
        return resDateTime;
    }

    /**
     * Setter method for property <tt>resDateTime</tt>.
     * 
     * @param resDateTime value to be assigned to property resDateTime
     */
    public void setResDateTime(String resDateTime) {
        this.resDateTime = resDateTime;
    }

    /**
     * Getter method for property <tt>success</tt>.
     * 
     * @return property value of success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Setter method for property <tt>success</tt>.
     * 
     * @param success value to be assigned to property success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Getter method for property <tt>totalCount</tt>.
     * 
     * @return property value of totalCount
     */
    public int getTotalCount() {
        return totalCount;
    }

    /**
     * Setter method for property <tt>totalCount</tt>.
     * 
     * @param totalCount value to be assigned to property totalCount
     */
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * Getter method for property <tt>totalPage</tt>.
     * 
     * @return property value of totalPage
     */
    public int getTotalPage() {
        return totalPage;
    }

    /**
     * Setter method for property <tt>totalPage</tt>.
     * 
     * @param totalPage value to be assigned to property totalPage
     */
    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

}
