/**
 * 
 */
package com.differ.microservice.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author zhenshan.xu
 * @version $Id: Validator.java, v 0.1 2017年8月1日 下午1:56:05 zhenshan.xu Exp $
 */
public class Validator {
	
    /** 
     * 功能：判断字符串是否为数字 
     * 
     * @param str 
     * @return 
     */  
    public boolean isNumeric(String str) {
    	//因为是模糊查询，所以截取字符串，将字符串第一个和最后一个 % 去掉。
    	String sb = str.substring(1,str.length()-1);
        Pattern pattern = Pattern.compile("[0-9]*");  
        Matcher isNum = pattern.matcher(sb);  
        if (isNum.matches()) {  
            return true;  
        } else {  
            return false;  
        }  
    }

}
