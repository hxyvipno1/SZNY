package com.differ.microservice.core.util;

import com.differ.microservice.core.CONSTANTS;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * Created by cailm on 2017-06-23.
 */
public class DataPreset {
    private static DataPreset instance=null;

    public static synchronized DataPreset getInstance(){
        if(instance==null)
            instance = new DataPreset();

        return instance;
    }


    public Map insertPre(Map entity){
        try{
            Object create_time=entity.get(CONSTANTS.C_CRT_TIME);
            if(create_time==null)
                entity.put(CONSTANTS.C_CRT_TIME,new Date());

            String idx=(String)entity.get(CONSTANTS.C_PK);
            if(StringUtils.isBlank(idx))
                entity.put(CONSTANTS.C_PK,crtKey());

        }catch(Exception e){
            e.printStackTrace();
        }
        return entity;
    }

    public Map updatePre(Map entity){
        try{
            Object update_time=entity.get(CONSTANTS.C_UPT_TIME);
            if(update_time==null)
                entity.put(CONSTANTS.C_UPT_TIME,new Date());


        }catch(Exception e){
            e.printStackTrace();
        }
        return entity;
    }

    public String crtKey(){
        return UUID.randomUUID().toString();
    }
}
