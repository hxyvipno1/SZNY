package com.differ.microservice.core.util;

import com.differ.microservice.core.CONSTANTS;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.MessageDigest;
import java.security.SecureRandom;

/**
 * Created by cailm on 2017-06-27.
 */
public class Encryption {
    public static String encrypt(String source) {
        String result = "";
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance(CONSTANTS.SECURITY_MD5);
            return Base64.encodeBase64String(messageDigest.digest(source .getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String des_encrypt(String data, String key) throws Exception {

        if(StringUtils.isBlank(key))key=CONSTANTS._DES_PWD;
        byte[] bt = encrypt(data.getBytes(CONSTANTS.UTF8), key.getBytes(CONSTANTS.UTF8));
        String strs = Base64.encodeBase64String(bt);
        return strs;
    }

    public static String des_decrypt(String data, String key) {
        try{
            if (data == null)return null;
            if(StringUtils.isBlank(key))key=CONSTANTS._DES_PWD;
            byte[] buf = Base64.decodeBase64(data);
            byte[] bt = decrypt(buf,key.getBytes(CONSTANTS.UTF8));
            return new String(bt);
        }catch(Exception e){
            return null;
        }
    }

    private static byte[] encrypt(byte[] data, byte[] key) throws Exception {
        SecureRandom sr = new SecureRandom();
        DESKeySpec dks = new DESKeySpec(key);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(CONSTANTS.SECURITY_DES);
        SecretKey securekey = keyFactory.generateSecret(dks);
        Cipher cipher = Cipher.getInstance(CONSTANTS.SECURITY_DES);
        cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);
        return cipher.doFinal(data);
    }

    private static byte[] decrypt(byte[] data, byte[] key) throws Exception {
        SecureRandom sr = new SecureRandom();
        DESKeySpec dks = new DESKeySpec(key);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(CONSTANTS.SECURITY_DES);
        SecretKey securekey = keyFactory.generateSecret(dks);
        Cipher cipher = Cipher.getInstance(CONSTANTS.SECURITY_DES);
        cipher.init(Cipher.DECRYPT_MODE, securekey, sr);
        return cipher.doFinal(data);
    }
}
