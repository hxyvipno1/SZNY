package com.differ.microservice.core.util;

import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by cailm on 2017-06-23.
 */
public class DateUtil {
    private static final String sdf1reg = "^\\d{2,4}\\-\\d{1,2}\\-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}$";
    private static final SimpleDateFormat sdf1 = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
    private static final String sdf2reg = "^\\d{2,4}\\-\\d{1,2}\\-\\d{1,2}$";
    private static final SimpleDateFormat sdf2 = new SimpleDateFormat( "yyyy-MM-dd");
    private static final String sdf3reg = "^\\d{2,4}\\/\\d{1,2}\\/\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}$";
    private static final SimpleDateFormat sdf3 = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss");
    private static final String sdf4reg = "^\\d{2,4}\\/\\d{1,2}\\/\\d{1,2}$";
    private static final SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy/MM/dd");
    public static final String pattern1 = "yyyy-MM-dd";
    public static final String pattern2 = "yyyy-MM-dd HH:mm:ss";
    public static final String pattern3 = "yyyy/MM/dd";
    public static final String pattern4 = "yyyy/MM/dd HH:mm:ss";

    public static Date parse(String str){
        Date date = null;
        Pattern p1 = Pattern.compile(sdf1reg);
        Matcher m1 = p1.matcher(str);
        Pattern p2 = Pattern.compile(sdf2reg);
        Matcher m2 = p2.matcher(str);
        Pattern p3 = Pattern.compile(sdf3reg);
        Matcher m3 = p3.matcher(str);
        Pattern p4 = Pattern.compile(sdf4reg);
        Matcher m4 = p4.matcher(str);
        try
        {
            if (m1.matches()) {
                date = sdf1.parse(str);
            }
            else if (m2.matches()) {
                date = sdf2.parse(str);
            }
            else if (m3.matches()) {
                date = sdf3.parse(str);
            }
            else if (m4.matches())
                date = sdf4.parse(str);
        }
        catch (ParseException e)  {
            e.printStackTrace();
        }

        return date;
    }

    public static String format(Date date){
        return format(date,pattern2);
    }

    public static String format(Date date, String pattern){
        if(date==null) return "";
        if(StringUtils.isBlank(pattern))pattern=pattern2;
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }
}
