package com.differ.microservice.core.util;

import com.differ.microservice.core.CONSTANTS;
import com.differ.microservice.core.spring.SpringUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cailm on 2017-06-22.
 */
public class SqlParser {

    public String getPath() {
        return SpringUtil.getProp("spring.application.sql_path");
    }

    private static SqlParser instance=null;
    private String encoding = CONSTANTS.UTF8;
    private boolean read=false;
    private Map sqlMap=null;

    public static synchronized SqlParser getInstance(){
        if(instance==null)
            instance=new SqlParser();

        return instance;
    }

    private String readJSONFile(String path){
        try{
            File file=new File(path);
            if(file.isFile() && file.exists()){
                InputStreamReader read = new InputStreamReader( new FileInputStream(file),encoding);
                BufferedReader bufferedReader = new BufferedReader(read);
                StringBuffer buffer = new StringBuffer();
                String lineTxt="";
                while((lineTxt = bufferedReader.readLine()) != null){
                    buffer.append(lineTxt);
                }
                read.close();

                return buffer.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    private Map parse(){
        if(sqlMap==null || true ){
            //String content=readJSONFile(getPath());
            String content=ResourceFile.gtResourceContent(getPath());
            sqlMap=JSONParser.json2Map(content);
        }
        return sqlMap;
    }

    public Map getDataSource(){
        return (Map)parse().get("datasource");
    }

    private Map gtTblMap(){
        return (Map)parse().get("table");
    }

    public List findTblInfo(String tbl){
        return (List)gtTblMap().get(tbl);
    }

    public Map loadConfByCode(String code,Map param){
        Map result=new HashMap();
        Map qry=(Map)parse().get("query");
        Map dqry=(Map)qry.get(code);
        if(dqry==null) return null;
        String sql=(String)dqry.get("sql");
        String tsql=(String)dqry.get("tsql");
        String psql=(String)dqry.get("psql");

        StringBuffer dbuffer=new StringBuffer();
        List params=(List)dqry.get("params");
        if(params!=null) {
            for (Object item : params) {
                Map item_mp=(Map)item;
                String con=(String) item_mp.get("con");
                String prm=(String)item_mp.get("param");
                String type=(String)item_mp.get("type");

                if(StringUtils.isBlank(prm)){
                    dbuffer.append(con);
                    continue;
                }

                if(!param.containsKey(prm)) continue;

                if(StringUtils.contains(con, " :? "))
                    dbuffer.append(StringUtils.replace(con, " :?" , " :"+prm+" "));
                else
                    dbuffer.append(con).append(prm);

                if(StringUtils.contains(con.toLowerCase(), " in ") || StringUtils.contains(con.toLowerCase(), " in(")){
                    List vs=null;
                    Object plist = param.get(prm);
                    if(plist instanceof String)
                        vs = JSONParser.json2List((String)plist);
                    else
                        vs = (List)plist;
                    List l = new ArrayList();
                    if( StringUtils.equals(type, CONSTANTS.TY_INT)){
                        for (int i = 0; i < vs.size(); i++)
                            l.add(new Long(vs.get(i).toString()));
                    }else
                        l.addAll(vs);
                    param.put(prm, l);
                }else if(StringUtils.equals(type, CONSTANTS.TY_INT)){
                    Object pv = param.get(prm);
                    if(pv instanceof String) pv = new Long((String)pv);
                    param.put(prm, pv);
                }else if(StringUtils.equals(type, CONSTANTS.TY_DATE)){
                    param.put(prm, DateUtil.parse((String)param.get(prm)));
                }
            }
        }
        if(!StringUtils.isBlank(sql)){
	        sql = StringUtils.replace(sql, "{1}", dbuffer.toString());
	        result.put("sql",sql);
        }

        if(!StringUtils.isBlank(psql)){
        	psql=StringUtils.replace(psql, "{1}", dbuffer.toString());
        	result.put("psql",psql);
        }
        
        if(!StringUtils.isBlank(tsql)){
        	tsql=StringUtils.replace(tsql, "{1}", dbuffer.toString());
        	result.put("tsql", tsql);
        }
        
        return result;
    }
}
