/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.core.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 指定长度。与原数字字符串 生成 新的数字字符串列表
 * @author yue.li
 * @version $Id: NumberToLenth.java, v 0.1 2017年6月21日 下午2:59:45 yue.li Exp $
 */
public class NumberCodeToLength {

    /** 
     * 批量生成IMEI 
     * @param begin  
     * @param end 
     * @return 
     */  
    public static List<String> beachIMEI(String begin,String Length){  
        List<String> imeis = new ArrayList<String>();  
        try {  
            long count = Long.parseLong(Length)-1; //为与前端页面的输入个数相同 
            Long currentCode = Long.parseLong(begin);  
            String code ;  
            for (int i = 0; i <= count; i++) {  
                code = currentCode.toString();  
                code =code+ genCode(code);  
                imeis .add(code);  
                System.out.println("code====="+code);  
                currentCode += 1;             
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        }     
          
        return imeis;  
    }  
      
    /** 
     * IMEI 校验码 
     * @param code 
     * @return 
     */  
    public static String genCode(String code){  
        int total=0,sum1=0,sum2 =0;  
        int temp=0;  
        char [] chs = code.toCharArray();  
        for (int i = 0; i < chs.length; i++) {             
            int num = chs[i] - '0';     // ascii to num  
            //System.out.println(num);  
            /*(1)将奇数位数字相加(从1开始计数)*/  
            if (i%2==0) {  
                sum1 = sum1 + num;  
            }else{  
                /*(2)将偶数位数字分别乘以2,分别计算个位数和十位数之和(从1开始计数)*/  
                temp=num * 2 ;  
                if (temp < 10) {  
                    sum2=sum2+temp;  
                }else{  
                    sum2 = sum2 + temp + 1 -10;  
                }  
            }  
        }  
        total = sum1+sum2;  
        /*如果得出的数个位是0则校验位为0,否则为10减去个位数 */  
        if (total % 10 ==0) {  
            return "0";  
        }else{  
            return (10 - (total %10))+"";  
        }  
          
    }  
}
