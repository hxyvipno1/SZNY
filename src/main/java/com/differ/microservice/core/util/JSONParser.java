package com.differ.microservice.core.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;

/**
 * Created by cailm on 2017-06-22.
 */
public class JSONParser {
    public static List json2List(String JSON){
        ObjectMapper obm=new ObjectMapper();
        List list=null;
        try {
            list=obm.readValue(JSON,List.class);
        } catch (Exception e) {
            return null;
        }
        return list;
    }

    public static Map json2Map(String JSON){
        ObjectMapper obm=new ObjectMapper();
        Map list=null;
        try {
            list=obm.readValue(JSON,Map.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return list;
    }

    public static String obj2Json(Object obj){
        ObjectMapper obm = new ObjectMapper();
        try{
            DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            obm.setDateFormat(dateFormat);
            return obm.writeValueAsString(obj);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
//    public  static void main(String[] args){
//        Map mp=new HashMap();
//        mp.put("tel","12222");
//        mp.put("content","6月22日开始订餐，今天有三种套餐");
//        mp.put("smstype",0);
//        mp.put("signature","xxxxxxxx");
//
//        //ring data="{tel:\"123433\",content:\"6月22日开始订餐，今天有三种套餐\",smstype:0,signature:\"xxxxxxxx\"}";
//        System.out.println(JSONParser.obj2Json(mp));
//    }
}
