/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.core.util;

/**
 * 省市区联动工具类
 * @author dunfang.zhou
 * @version $Id: District.java, v 0.1 2017年6月30日 下午5:05:06 dunfang.zhou Exp $
 */
public class District {
  public static String  getPlace(String place){
      
      return place.replace("省", "").replace("自治旗", "").replace("市", "")
              .replace("区", "").replace("县", "").replace("市保税区", "")
              .replace("市直属", "").replace("市开发区", "").replace("自治县", "")
              .replace("盟", "").replace("自治州", "").replace("地区", "")
              .replace("自治区", "").replace("行委", "").replace("特别行政区","")
              .replace("堂区", "").replace("旗", "");
       
    }
}
