/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.core.util;

import java.util.List;

/**
 * 
 * @author liyuran
 * @version $Id: QueryResult.java, v 0.1 2017年7月17日 下午4:04:05 liyuran Exp $
 */
public class QueryResult<T> {
    private List<T> list; // 结果集  
    private int totalRow; // 总记录数  
  
    public QueryResult() {  
    }  
  
    public QueryResult(List<T> list, int totalRow) {  
        this.list = list;  
        this.totalRow = totalRow;  
    }

    /**
     * Getter method for property <tt>list</tt>.
     * 
     * @return property value of list
     */
    public List<T> getList() {
        return list;
    }

    /**
     * Setter method for property <tt>list</tt>.
     * 
     * @param list value to be assigned to property list
     */
    public void setList(List<T> list) {
        this.list = list;
    }

    /**
     * Getter method for property <tt>totalRow</tt>.
     * 
     * @return property value of totalRow
     */
    public int getTotalRow() {
        return totalRow;
    }

    /**
     * Setter method for property <tt>totalRow</tt>.
     * 
     * @param totalRow value to be assigned to property totalRow
     */
    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
    
    
    
}
