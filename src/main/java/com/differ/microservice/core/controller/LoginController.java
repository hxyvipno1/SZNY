package com.differ.microservice.core.controller;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.differ.microservice.core.service.CacheService;
import com.differ.microservice.core.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	private LoginService service;
	
	@Autowired
	private CacheService cache;
	
	@RequestMapping("/ulogin")
	public String main(){
		return "login";
	}
	
	@RequestMapping(value = "/ulogin/do" , method = RequestMethod.POST)
	public String excute(
			@RequestParam(value = "user_name", required = true)String code,
			@RequestParam(value = "password", required = true)String pwd,
			HttpServletResponse resp
	){
		
		Map umap=service.doLogin(code, pwd);
		if(umap==null) return "/ulogin";
		Cookie cookie = new Cookie("uuid",(String)umap.get("idx")); 
		cookie.setMaxAge(60*60*24);
		cookie.setPath("/");
		resp.addCookie(cookie);
		return "redirect:/";
		//return "redirect:/loginsuccess.html";
	}
	
	@RequestMapping(value = "/ulogin/exist")
	public String exist(@CookieValue(value = "uuid",required  = true) String uid,HttpServletResponse resp){
		
		service.logout(uid);
		Cookie cookie = new Cookie("uid",""); 
		cookie.setPath("/");
		resp.addCookie(cookie);
		return "redirect:/ulogin";
	}
	
	
}
