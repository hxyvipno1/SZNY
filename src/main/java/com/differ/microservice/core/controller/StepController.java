package com.differ.microservice.core.controller;

import com.differ.microservice.core.service.StepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * Created by cailm on 2017-07-06.
 */

@RestController
public class StepController extends BaseSupport{
    @Autowired
    private StepService service;

    @RequestMapping("/step")
    @ResponseBody
    public String step(HttpServletRequest req){
        return output(service.doStep(getInputJSON(req)));
    }
}
