package com.differ.microservice.core.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.differ.microservice.core.CONSTANTS;
import com.differ.microservice.core.service.StepService;
import com.differ.microservice.core.spring.SpringUtil;
import com.differ.microservice.core.util.DateUtil;
import com.differ.microservice.core.util.JSONParser;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class FileController  extends BaseSupport{
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    private StepService service;
    
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
    public Object  upload(@RequestParam("filename") MultipartFile fileField,HttpServletRequest request){
		Map entity = new HashMap();
        try {
        	String name = fileField.getOriginalFilename();
        	String path="/"+getUserId()+"/"+DateUtil.format(new Date(), "yyMMddHHmm");
        	FileUtils.copyInputStreamToFile(fileField.getInputStream(), new File(getPath()+path, name));
        	entity.put("file_name", name);
        	entity.put("file_path", path);
        	entity.put("upload_type", "zny");
        	entity.put("file_type", fileField.getContentType());
        	entity.put("file_size", fileField.getSize());
        	service.addEntity("t_file", entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }
	
	@RequestMapping(value="/file/{idx}/view", method = RequestMethod.GET)
	public void view(@PathVariable String idx,HttpServletResponse resp) throws Exception{
		String fullPath="";String file_type="image/png";
		try{
			Map fmp=service.getDao().loadRT("t_file", idx);
			fullPath=getPath()+"/"+(String)fmp.get("file_path")+"/"+(String)fmp.get("file_name");
			file_type=(String)fmp.get("file_type");
		}catch(Exception e){
			fullPath=getPath()+"/blank.png";
		}
		FileInputStream inputStream = FileUtils.openInputStream(new File(fullPath));
		resp.setContentType(file_type);   
		OutputStream outStream = resp.getOutputStream();    
		byte[] buffer = new byte[2048];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
        inputStream.close();
        outStream.close();
	}
	
	@RequestMapping(value = "/ckbrowser")
	@ResponseBody
    public String ckbrowser(HttpServletRequest request,HttpServletResponse resp){
		List rst=new ArrayList();
		String CKEditorFuncNum = request.getParameter("CKEditorFuncNum"); 
        try {
        	//PrintWriter out = resp.getWriter();
        	Map param=new HashMap();
        	param.put("ftype", "%image%");
        	List<Map> list=service.getDao().loadListByCode("LD_ATTACH", param);
        	for(Map row: list){
        		Map r=new HashMap();
        		String url=String.format("/file/%s/view", row.get("idx"));
        		r.put("image", url);
        		r.put("thumb", url);
        		
        		rst.add(r);
        		
        	}
        	
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONParser.obj2Json(rst);
    }
	
	//CKEDITOR 文件上传
	@RequestMapping(value = "/ckupload", method = RequestMethod.POST)
    public void ckupload(@RequestParam("upload") MultipartFile fileField,HttpServletRequest request,HttpServletResponse resp){
		Map entity = new HashMap();
        try {
        	String name = fileField.getOriginalFilename();
        	String content_type=fileField.getContentType();
        	long size = fileField.getSize();
        	String script="";
        	
        	String path="/"+getUserId()+"/"+DateUtil.format(new Date(), "yyMMddHHmm");
        	FileUtils.copyInputStreamToFile(fileField.getInputStream(), new File(getPath()+path, name));
        	entity.put("file_name", name);
        	entity.put("file_path", path);
        	entity.put("file_type", content_type);
        	entity.put("file_size", size);
        	entity.put("upload_type", "ck");
        	service.addEntity("t_file", entity);
        	
        	PrintWriter out = resp.getWriter();  
        	String CKEditorFuncNum = request.getParameter("CKEditorFuncNum");  
        	String expandedName = "";  
        	if(StringUtils.containsIgnoreCase(content_type, "jpeg")){
        		expandedName = ".jpg"; 
        	}else if(StringUtils.containsIgnoreCase(content_type, "png")){
        		expandedName = ".png"; 
        	}else if(StringUtils.containsIgnoreCase(content_type, "gif")){
        		expandedName = ".gif";
        	}else if(StringUtils.containsIgnoreCase(content_type, "bmp")){
        		expandedName = ".bmp";
        	}else{
        		script="<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction( %s ,'','文件格式不正确（必须为.jpg/.gif/.bmp/.png文件）');</script>";
        		script=String.format(script, CKEditorFuncNum);
        		out.println(script);
        		return;
        	}
        	
        	if (size > 1024 * 1024 * 2) {  
        		script="<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(%s,'','文件大小不得大于2M');</script>";
        		script=String.format(script, CKEditorFuncNum);
        		out.println(script);
        		return;  
        	} 
        	script="<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(%s,'/file/%s/view','');</script>";
        	script=String.format(script, CKEditorFuncNum,entity.get("idx"));
        	out.println(script);
        	return;
        	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	
	
	 public String getPath() {
        return SpringUtil.getProp("spring.application.upload_path");
    }
}
