package com.differ.microservice.core.controller;

import com.differ.microservice.core.CONSTANTS;
import com.differ.microservice.core.dto.ResponseInfo;
import com.differ.microservice.core.spring.SpringUtil;
import com.differ.microservice.core.util.JSONParser;
import com.differ.microservice.core.util.XmlParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cailm on 2017-06-27.
 */
public class BaseSupport {

    public String getDataformat() {
        return SpringUtil.getProp("dataformat");
    }

    protected String output(Object detail) {
        ResponseInfo result = new ResponseInfo(1, ResponseInfo.SUMESSAGE, detail);
        if (!StringUtils.equalsIgnoreCase(getDataformat(), CONSTANTS.DF_XML)) {
            return result.toJson();
        } else {
            return result.toXml();
        }
    }

    protected String output(int code) {
        String msg = "";
        switch (code) {
            case 1:
                msg = ResponseInfo.SUMESSAGE;
                break;
            case 0:
                msg = ResponseInfo.EXMESSAGE;
                break;

            case -2:
                msg = ResponseInfo.ERMESSAGE;
                break;

        }
        ResponseInfo result = new ResponseInfo(code, msg);
        if (!StringUtils.equalsIgnoreCase(getDataformat(), CONSTANTS.DF_XML)) {
            return result.toJson();
        } else {
            return result.toXml();
        }
    }
    
    protected String output(int code,String msg) {
      
        ResponseInfo result = new ResponseInfo(code, msg);
        if (!StringUtils.equalsIgnoreCase(getDataformat(), CONSTANTS.DF_XML)) {
            return result.toJson();
        } else {
            return result.toXml();
        }
    }

    protected Object input(String content) {
        if (!StringUtils.equalsIgnoreCase(getDataformat(), CONSTANTS.DF_XML)) {
            return JSONParser.json2Map(content);
        } else {  
            return XmlParser.xml2Map(content);
        }
    }

    protected Map getInputJSON(HttpServletRequest request) {
        Map mp = new HashMap();
        try {
            InputStream in = request.getInputStream();
            BufferedReader bf = new BufferedReader(new InputStreamReader(in, CONSTANTS.UTF8));
            StringBuffer buffer = new StringBuffer();
            char[] buff = new char[2048];
            int bytesRead;
            while (-1 != (bytesRead = bf.read(buff, 0, buff.length))) {
                buffer.append(buff, 0, bytesRead);
            }
            ObjectMapper obm = new ObjectMapper();
            if (!StringUtils.isEmpty(buffer.toString()))
                mp = obm.readValue(buffer.toString(), Map.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mp;
    }
    
    protected String getUserId(){
    	return "123";
    }

}
