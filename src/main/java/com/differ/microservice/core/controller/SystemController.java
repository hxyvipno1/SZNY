package com.differ.microservice.core.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.differ.microservice.core.service.SystemService;

@RestController
@RequestMapping("/sys")
public class SystemController extends BaseSupport{
	@Autowired
	private SystemService service;
	
	@RequestMapping(value = "/saveuser")
	@ResponseBody
	public String saveuser(HttpServletRequest req){
		Map param=this.getInputJSON(req);
		return output(service.saveuser(param));
	}
	
	@RequestMapping(value = "/resetpwd")
	@ResponseBody
	public String resetpwd(HttpServletRequest req){
		Map param=this.getInputJSON(req);
		return output(service.resetpwd(param));
	}
}
