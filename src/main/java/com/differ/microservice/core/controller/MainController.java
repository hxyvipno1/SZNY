package com.differ.microservice.core.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.differ.microservice.core.service.AppService;
import com.differ.microservice.core.service.CacheService;

@Controller
public class MainController {
	@Autowired
	private AppService service;
	@Autowired
	private CacheService cache;
	
	@RequestMapping("/old")
	public ModelAndView main(@CookieValue(value = "uuid",required  = true) String uid ){
		Map cookie=cache.findCookie(uid);
		Map initData=service.initLoad((String)cookie.get("uidx"));
//		String type=(String)initData.get("type");
		String url="index";
//		switch(type){
//			case "1":
//				url= "index1"; break;
//			case "2":
//				url= "index2"; break;
//			case "3":
//				url= "index3"; break;
//		}
		
		return new ModelAndView(url, "initData", initData);
	}
	
	@RequestMapping("/")
	public ModelAndView main1(@CookieValue(value = "uuid",required  = true) String uid ){
		Map cookie=cache.findCookie(uid);
		Map initData=service.initLoad1((String)cookie.get("uidx"));

		return new ModelAndView("main", "initData", initData);
	}
}
