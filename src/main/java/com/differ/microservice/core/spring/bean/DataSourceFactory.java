package com.differ.microservice.core.spring.bean;

import com.differ.microservice.core.dao.DynamicDao;
import com.differ.microservice.core.spring.SpringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cailm on 2017-07-06.
 */

public class DataSourceFactory {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private String DATASOURCE_BEAN_CLASS = "org.apache.commons.dbcp.BasicDataSource";
    private String JDBC_DRIVER="com.mysql.jdbc.Driver";
    private static DataSourceFactory instance;

    private Map dblink=new HashMap();
    public static synchronized DataSourceFactory getInstance(){
        if(instance==null)
            instance=new DataSourceFactory();

        return instance;
    }
    private String getUrl(Map dbmp){
        String url="jdbc:mysql://%s:%s/%s?autoReconnect=true";
        url=String.format(url,dbmp.get("ip"),dbmp.get("port"),dbmp.get("dbname"));
        return url;
    }
    public synchronized DynamicDao crtDynamicDao(Map dbmp){
        String code=(String)dbmp.get("code");
        String db_code="db_"+code;
        String jdbct_code="jdbct_"+code;
        String dao_code="dao_"+code;

        if(dblink.containsKey(dbmp.get("idx")))
            return (DynamicDao)SpringUtil.getBean(dao_code);

        DefaultListableBeanFactory factory = (DefaultListableBeanFactory) SpringUtil.getApp().getAutowireCapableBeanFactory();

        BeanDefinitionBuilder dbcp = BeanDefinitionBuilder.rootBeanDefinition(DATASOURCE_BEAN_CLASS);
        dbcp.getBeanDefinition().setAttribute("id", db_code);
        dbcp.addPropertyValue("driverClassName", JDBC_DRIVER);
logger.info(getUrl(dbmp));
logger.info((String)dbmp.get("uname"));
logger.info((String)dbmp.get("pwd"));
        dbcp.addPropertyValue("url", getUrl(dbmp));
        dbcp.addPropertyValue("username", dbmp.get("uname"));
        dbcp.addPropertyValue("password",dbmp.get("pwd"));
        dbcp.addPropertyValue("testOnBorrow", "true");
        dbcp.addPropertyValue("validationQuery", "SELECT 1");
        dbcp.addPropertyValue("initialSize", 2);
        dbcp.addPropertyValue("maxActive", 5);
        dbcp.addPropertyValue("maxIdle", 5);
        dbcp.addPropertyValue("maxWait", 5);
        factory.registerBeanDefinition(db_code, dbcp.getBeanDefinition());

        BeanDefinitionBuilder bdt = BeanDefinitionBuilder.rootBeanDefinition("org.springframework.jdbc.core.JdbcTemplate");
        bdt.getBeanDefinition().setAttribute("id", jdbct_code);
        bdt.addConstructorArgReference(db_code);
        factory.registerBeanDefinition(jdbct_code, bdt.getBeanDefinition());

        BeanDefinitionBuilder bdao = BeanDefinitionBuilder.rootBeanDefinition("com.differ.microservice.core.dao.DynamicDao");
        bdao.getBeanDefinition().setAttribute("id", dao_code);
        bdao.addPropertyReference("jdbcTemplate", jdbct_code);
        bdao.addPropertyValue("dbname",dbmp.get("dbname"));
        factory.registerBeanDefinition(dao_code, bdao.getBeanDefinition());

        dblink.put(dbmp.get("idx"),dao_code);
        return (DynamicDao)SpringUtil.getBean(dao_code);
    }
}
