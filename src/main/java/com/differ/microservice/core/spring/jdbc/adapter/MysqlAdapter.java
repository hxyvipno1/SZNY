package com.differ.microservice.core.spring.jdbc.adapter;

import com.differ.microservice.core.spring.jdbc.JdbcAdapter;

import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by cailm on 2017-07-05.
 */

public class MysqlAdapter extends JdbcAdapter {

    public String getSql4Tbl() {
        String sql="SELECT column_name AS 'name',column_comment AS 'sname',data_type as type FROM information_schema.columns WHERE  table_name = :tname and table_schema = :schema ORDER BY ordinal_position ";
        return sql;
    }

    public String getSql4TbPK(){
        return "SELECT column_name as 'name' FROM information_schema.key_column_usage WHERE table_name = :tname and table_schema = :shcema ";
    }

	
	public Map convertPageParam(Map param, Map pg) {
		Number start=(Number)pg.get("start");
		Number limit=(Number)pg.get("end");
		param.put("start", start.intValue());
		param.put("end", limit.intValue());
		return param;
	}
}
