package com.differ.microservice.core.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.*;

/**
 * Created by cailm on 2017-06-29.
 */

public  class UxInitializingBean implements ApplicationContextInitializer,ApplicationListener {
    protected ConfigurableApplicationContext app;
    private static boolean inited=false;

    public  void initialize(ConfigurableApplicationContext context){
        app=context;
        SpringUtil.setAPP(app);
    }

    public void onApplicationEvent(ApplicationEvent applicationEvent){

    }
}
