package com.differ.microservice.core.spring.bean;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;


import java.lang.reflect.Method;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.SynthesizingMethodParameter;

import com.differ.microservice.core.util.CacheUtil;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;


@Aspect
@Configuration
public class AspectCache {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
//	@Pointcut("execution(* com.differ.microservice.core.service.CacheService.find*(..))")
    public void excudeService(){}

//    @Around("excudeService()")
    public Object twiceAsOld(ProceedingJoinPoint point){
		Object thing=null;
        try {
        	point.getArgs();
        	MethodSignature signature = (MethodSignature)point.getSignature();
        	Method method = signature.getMethod();
        	String key = method.getName();
        	for (Object value : point.getArgs()) {
        		key +=  "_" + value;
        	}
        	
        	thing = CacheUtil.instance().getKey(key);
        	if(thing==null){
        		thing = point.proceed();
                CacheUtil.instance().putKey(key, thing);
        	}
            
            return thing;
        } catch (Throwable e) {
            e.printStackTrace ();
        }
        return thing;
    }

}
