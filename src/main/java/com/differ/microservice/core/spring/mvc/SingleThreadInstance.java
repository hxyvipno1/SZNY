package com.differ.microservice.core.spring.mvc;

import java.util.HashMap;
import java.util.Map;

public class SingleThreadInstance {
	private static SingleThreadInstance instance = null;
	private ThreadLocal<Map<Object, Object>> threadLocal;
	
	public static synchronized SingleThreadInstance getInstance() {
		if (instance == null) {
            instance = new SingleThreadInstance();
        }
        return instance;
	}
	
	public ThreadLocal<Map<Object,Object>> getThreadLocal(){
		if(threadLocal==null){
			threadLocal=new ThreadLocal();
			threadLocal.set(new HashMap());
		}
		return threadLocal;
	}
	
	public void put(String key,Object value){
		Map map = (Map)this.getThreadLocal().get();
		if(map==null)
			map = new HashMap();
		map.put(key, value);
		this.getThreadLocal().set( map);
	}
	
	public Object get(String key){
		Map map = (Map)this.getThreadLocal().get();
		return map.get(key);
	}
	
	public void remove(String key){
		Map map = (Map)this.getThreadLocal().get();
		map.remove(key);
		this.getThreadLocal().set(map);
	}
	
	public void clear(){
		Map map = (Map)this.getThreadLocal().get();
		map.clear();
		this.getThreadLocal().set(map);
	}
}
