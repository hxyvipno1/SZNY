package com.differ.microservice.core.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by cailm on 2017-07-06.
 */
//@Configuration
public class UxConfigurerAdapter extends WebMvcConfigurerAdapter {

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //registry.addResourceHandler("/page/**").addResourceLocations(SpringUtil.getProp("spring.application.static_path"));
        super.addResourceHandlers(registry);
    }
}
