package com.differ.microservice.core.spring.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	 @Autowired
	 private LoginInterceptor loginInterceptor;
	 
	 public void addInterceptors(InterceptorRegistry registry) {
		 registry.addInterceptor(loginInterceptor).addPathPatterns("/*")
		 .excludePathPatterns(
				 "/ulogin/**",
				 "*.action"
		);
		 super.addInterceptors(registry);
	 }
	 
}
