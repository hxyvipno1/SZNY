package com.differ.microservice.core.spring.jdbc;

import com.differ.microservice.core.CONSTANTS;
import com.differ.microservice.core.util.DataPreset;
import com.differ.microservice.core.util.DateUtil;
import com.differ.microservice.core.util.JSONParser;
import com.differ.microservice.core.util.SqlParser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cailm  on 2017-07-05.
 */
public abstract class JdbcAdapter {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    protected NamedParameterJdbcTemplate template=null;
    protected JdbcTemplate jdbcTemplate;
    private String dbname;

    public abstract String getSql4Tbl();
    public abstract String getSql4TbPK();
    public abstract Map convertPageParam(Map param,Map pg);
    public Map executeByCode(String code,Map param){
        Map map = SqlParser.getInstance().loadConfByCode(code,param);
        String sql=(String)map.get("sql");
        return excuteSql(sql,param);
    }

    public Map excuteSql(String sql,Map param){
        SqlParameterSource ps=new MapSqlParameterSource(param);
        final Map result=new HashMap();
        getTemplate().execute(sql,ps,new PreparedStatementCallback(){
            public Object doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                boolean has_more=true;int key=0;
                ps.execute();
                while(has_more){
                    ResultSet rs = ps.getResultSet();
                    int count=0;key++;
                    if(rs==null){
                        count=ps.getUpdateCount();
                        result.put("key"+key,count);
                    }else{
                        ResultSetMetaData rm = rs.getMetaData();
                        List list=new ArrayList();
                        int clumn = rm.getColumnCount();
                        while(rs.next()){
                            count++;
                            Map row=new HashMap();
                            for (int i = 1; i <= clumn; i++) {
                                row.put(rm.getColumnName(i), rs.getString(i));
                            }
                            list.add(row);
                        }
                        result.put("key"+key,list);                        
                    }
                    
                    try{
                        has_more=ps.getMoreResults(Statement.CLOSE_ALL_RESULTS);
                    }catch(Exception e){
                        has_more=false;
                        logger.error("exception:",e);
                    }finally{}
                }
                return null;
            }

        });

        return result;
    }
    public Map loadRT(String tbl,String idx){
        String sql="select * from "+tbl+" where idx='"+idx+"' ";
        return loadRowByMap(sql,null);
    }

    public List loadListByCode(String code,Map param){
        Map map = SqlParser.getInstance().loadConfByCode(code,param);
        List rlist= this.loadListByMap((String)map.get("sql"), param);
        return rlist;
    }
    
    public Map loadPage(String code,Map param,Map pg){
    	Map result = new HashMap();
    	Map map = SqlParser.getInstance().loadConfByCode(code,param);
    	param=this.convertPageParam(param,pg);
    	List rlist= this.loadListByMap((String)map.get("psql"), param);
    	result.put("list", rlist);
    	if(pg.containsKey("tcount")){
    		Number tcount=0L;
    		Map rtmp =loadRowByMap((String)map.get("tsql"), param);
    		if(rtmp!=null) tcount=(Number)rtmp.get("tcount");
    		result.put("tcount", tcount);
    	}
    	return result;
    }

    public Map loadRowByCode(String code,Map param){
        List list=this.loadListByCode(code,param);
        if(list.isEmpty()) return null;
        return (Map)list.get(0);
    }

    public List loadListByMap(String sql,Map param){
        if(param==null) param=new HashMap();
        SqlParameterSource ps=new MapSqlParameterSource(param);
        sql=StringUtils.replace(sql, "{2}", "");
        List list = getTemplate().queryForList(sql, ps);
        return list;
    }

    public Map loadRowByMap(String sql,Map param){
        List list=this.loadListByMap(sql, param);
        if(list.isEmpty()) return null;
        return (Map)list.get(0);
    }

    public Map addEntity(String tbl, Map entity){
        int indx=0;StringBuffer cbuffer=new StringBuffer() ,  pbuffer=new StringBuffer();
        MapSqlParameterSource ps=new MapSqlParameterSource();
        entity= DataPreset.getInstance().insertPre(entity);
        String sql = "INSERT INTO %s (%s) VALUES (%s)  ";
        List columns= (List)findTblInfo(tbl).get("cols");

        for(Object k:entity.keySet()){
            String key=(String)k;
            Map detail=getDetailDefine(columns,key);
            if(detail==null) continue;
            if(indx>0){ cbuffer.append(",");pbuffer.append(",");}
            cbuffer.append(detail.get("name"));pbuffer.append(":"+key);
            indx++;

            Object v=entity.get(k);
            String type=(String)detail.get("type");
            ps=putParam(v,key,type,ps);
        }

        sql = String.format(sql, tbl,cbuffer.toString(),pbuffer.toString());
        KeyHolder keyholder=new GeneratedKeyHolder();
        getTemplate().update(sql, ps, keyholder);
        return entity;
    }

    public Map updateEntity(String tbl,Map entity){
        int indx=0;StringBuffer cbuffer=new StringBuffer() ,  pbuffer=new StringBuffer();
        String sql = "UPDATE  %s  SET  %s WHERE %s ";
        MapSqlParameterSource ps=new MapSqlParameterSource();
        entity=DataPreset.getInstance().updatePre(entity);
        List columns= (List)findTblInfo(tbl).get("cols");

        for(Object k:entity.keySet()){
            String key=(String)k;
            Map detail=getDetailDefine(columns,key);
            if(detail==null || StringUtils.equalsIgnoreCase(key, CONSTANTS.C_PK)) continue;
            if(indx>0){ cbuffer.append(",");}

            cbuffer.append(key).append("=:").append(key);
            indx++;
            Object v=entity.get(k);
            String type=(String)detail.get("type");
            ps=putParam(v,key,type,ps);
        }

        String pk=(String)entity.get(CONSTANTS.C_PK);
        if(StringUtils.isBlank(pk)){
            return null;
        }
        pbuffer.append(" ").append(CONSTANTS.C_PK).append("=:").append(CONSTANTS.C_PK).append(" ");
        ps.addValue(CONSTANTS.C_PK,pk);

        sql = String.format(sql, tbl,cbuffer.toString(),pbuffer.toString());
        getTemplate().update(sql, ps);
        return entity;
    }
    
    public void removeEntity(String tbl,Map entity){
    	String sql = "DELETE  FROM "+tbl+"  WHERE idx = :idx ";
    	MapSqlParameterSource ps=new MapSqlParameterSource();
    	ps.addValue("idx",entity.get("idx"), Types.CHAR);
    	getTemplate().update(sql, ps);
    }

    private MapSqlParameterSource putParam(Object v,String key,String type,MapSqlParameterSource ps){
        ps.addValue(key,v);
        if(StringUtils.containsIgnoreCase(type,CONSTANTS.TY_INT))
            ps.addValue(key,v, Types.INTEGER);
        else if( StringUtils.containsIgnoreCase(type,CONSTANTS.TY_DATE)) {
            if (v instanceof java.lang.String)
                ps.addValue(key, DateUtil.parse((String) v));
        }else if(StringUtils.containsIgnoreCase(type,CONSTANTS.TY_CHAR)) {
            if (v instanceof Map || v instanceof List)
                ps.addValue(key, JSONParser.obj2Json(v), Types.CHAR);
        }
        return ps;
    }

    public Map findTblInfo(String tbl){
        Map result=new HashMap();
        String sql=getSql4Tbl();
        Map pmp=new HashMap(); pmp.put("tname",tbl); pmp.put("schema",dbname);
        result.put("cols",loadListByMap(sql,pmp));
        return result;
    }

    private Map getDetailDefine(List columns,String key){
        for(Object col:columns){
            Map detail=(Map)col;
            String name=(String)detail.get("name");
            String sname=(String)detail.get("sname");
            if(StringUtils.equalsIgnoreCase(key, name) || StringUtils.equalsIgnoreCase(key,sname))
                return detail;
        }
        return null;
    }

    private NamedParameterJdbcTemplate getTemplate(){
        if(template==null)
            template=new NamedParameterJdbcTemplate(jdbcTemplate);
        return template;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

}
