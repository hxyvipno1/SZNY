package com.differ.microservice.core.dao;

import com.differ.microservice.core.CONSTANTS;
import com.differ.microservice.core.util.DataPreset;
import com.differ.microservice.core.util.DateUtil;
import com.differ.microservice.core.util.JSONParser;
import com.differ.microservice.core.util.SqlParser;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Types;
import java.util.List;
import java.util.Map;

/**
 * Created by cailm on 2017-06-22.
 */

@Component
public class JdbcDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private NamedParameterJdbcTemplate template=null;

    public  Map addEntity(String tbl,Map entity){
        int indx=0;StringBuffer cbuffer=new StringBuffer() ,  pbuffer=new StringBuffer();
        MapSqlParameterSource ps=new MapSqlParameterSource();
        entity= DataPreset.getInstance().insertPre(entity);
        String sql = "INSERT INTO %s (%s) VALUES (%s)  ";

        for(Object k:entity.keySet()){
            String key=(String)k;
            Map detail=getDetailDefine(tbl,key);
            if(detail==null) continue;
            if(indx>0){ cbuffer.append(",");pbuffer.append(",");}
            cbuffer.append(detail.get("name"));pbuffer.append(":"+key);
            indx++;

            Object v=entity.get(k);
            String type=(String)detail.get("type");
            ps=putParam(v,key,type,ps);
        }

        sql = String.format(sql, tbl,cbuffer.toString(),pbuffer.toString());
        KeyHolder keyholder=new GeneratedKeyHolder();
        getTemplate().update(sql, ps, keyholder);
        return entity;
    }

    public Map updateEntity(String tbl,Map entity){
        int indx=0;StringBuffer cbuffer=new StringBuffer() ,  pbuffer=new StringBuffer();
        String sql = "UPDATE  %s  SET  %s WHERE %s ";
        MapSqlParameterSource ps=new MapSqlParameterSource();
        entity=DataPreset.getInstance().updatePre(entity);

        for(Object k:entity.keySet()){
            String key=(String)k;
            Map detail=getDetailDefine(tbl,key);
            if(detail==null || StringUtils.equalsIgnoreCase(key, CONSTANTS.C_PK)) continue;
            if(indx>0){ cbuffer.append(",");}

            cbuffer.append(key).append("=:").append(key);
            indx++;
            Object v=entity.get(k);
            String type=(String)detail.get("type");
            ps=putParam(v,key,type,ps);
        }

        String pk=(String)entity.get(CONSTANTS.C_PK);
        if(StringUtils.isBlank(pk)){
            return null;
        }
        pbuffer.append(" ").append(CONSTANTS.C_PK).append("=:").append(CONSTANTS.C_PK).append(" ");
        ps.addValue(CONSTANTS.C_PK,pk);

        sql = String.format(sql, tbl,cbuffer.toString(),pbuffer.toString());
        getTemplate().update(sql, ps);
        return entity;
    }

    private MapSqlParameterSource putParam(Object v,String key,String type,MapSqlParameterSource ps){
        ps.addValue(key,v);
        switch (type){
            case CONSTANTS.TY_INT:
                ps.addValue(key,v, Types.INTEGER); break;
            case CONSTANTS.TY_DATE:
                if(v instanceof java.lang.String)
                    ps.addValue(key, DateUtil.parse((String)v));
                break;
            case CONSTANTS.TY_CHAR:
                if(v instanceof Map || v instanceof  List)
                    ps.addValue(key, JSONParser.obj2Json(v),Types.CHAR);
            default:
                break;
        }

        return ps;
    }

    private Map getDetailDefine(String tbl,String key){
        List columns= SqlParser.getInstance().findTblInfo(tbl);
        for(Object col:columns){
            Map detail=(Map)col;
            String name=(String)detail.get("name");
            String sname=(String)detail.get("sname");
            if(StringUtils.equalsIgnoreCase(key, name) || StringUtils.equalsIgnoreCase(key,sname))
                return detail;
        }
        return null;
    }

    private NamedParameterJdbcTemplate getTemplate(){
        if(template==null)
            template=new NamedParameterJdbcTemplate(jdbcTemplate);
        return template;
    }
}
