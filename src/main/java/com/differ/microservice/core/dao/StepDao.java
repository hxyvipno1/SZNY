package com.differ.microservice.core.dao;

import com.differ.microservice.core.spring.SpringUtil;
import com.differ.microservice.core.spring.jdbc.JdbcAdapter;
import com.differ.microservice.core.spring.jdbc.adapter.MysqlAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by cailm on 2017-07-06.
 */
@Repository
public class StepDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private JdbcAdapter adapter=null;

    private synchronized JdbcAdapter initAdapter(){
        if(adapter==null) {
            adapter = new MysqlAdapter();
            adapter.setJdbcTemplate(jdbcTemplate);
            adapter.setDbname(SpringUtil.getProp("spring.datasource.database"));
        }
        return adapter;
    }

    /**
     * 更新修改
     * @param tbl
     * @param entity
     * @return
     */
    public Map update(String tbl, Map entity){
        return initAdapter().updateEntity(tbl,entity);
    }

    /**
     * 新增
     * @param tbl
     * @param entity
     * @return
     */
    public Map add(String tbl, Map entity){
        return initAdapter().addEntity(tbl,entity);
    }
    
    /**
     * 删除
     * @param tbl
     * @param entity
     */
    public void remove(String tbl,Map entity){
    	initAdapter().removeEntity(tbl,entity);
    }

    public List loadListByMap(String sql, Map param){
        return initAdapter().loadListByMap(sql,param);
    }

    public Map loadRowByMap(String sql,Map param){
        return initAdapter().loadRowByMap(sql,param);
    }

    public List loadListByCode(String code,Map param){
        return initAdapter().loadListByCode(code,param);
    }
    /**
     * 分页查询
     * @param code
     * @param param
     * @param pg
     * @return
     */
    public Map loadPage(String code,Map param,Map pg){
    	return initAdapter().loadPage(code, param, pg);
    }

    public Map loadRowByCode(String code,Map param){
        return initAdapter().loadRowByCode(code,param);
    }

    /**
     * 根据idx查询
     * @param tbl
     * @param idx
     * @return
     */
    public Map loadRT(String tbl,String idx){
        return initAdapter().loadRT(tbl,idx);
    }
    
    public Map excuteByCode(String code,Map param){
        return initAdapter().executeByCode(code,param);
    }
}
