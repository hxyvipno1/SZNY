package com.differ.microservice.core.exception;

import com.differ.microservice.core.dto.ResponseInfo;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by cailm on 2017-06-26.
 */

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public String defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        e.printStackTrace();
        ResponseInfo resp=new ResponseInfo(0,e.getMessage());
        resp.setDetail(e.getMessage());
        return resp.output();
    }

//
//    @ExceptionHandler(value = BusinessException.class)
//    @ResponseBody
//    public String jsonErrorHandler(HttpServletRequest req, BusinessException e) throws Exception {
//        ResponseInfo resp=new ResponseInfo(0,e.getMessage());
//        resp.setDetail(e.getMessage());
//        return resp.output();
//    }
}
