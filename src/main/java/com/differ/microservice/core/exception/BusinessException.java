package com.differ.microservice.core.exception;

/**
 * Created by cailm on 2017-06-26.
 */
public class BusinessException   extends RuntimeException{
    private String msg = "";
    public BusinessException(String message) {
        super(message);
        msg=message;
    }
    public BusinessException(String message, Throwable cause) {
        super(message, cause);
        msg=message;
    }

    public BusinessException(String message, Throwable cause, boolean relogin) {
        super(message, cause);
        msg=message;
    }

    public String getMessage(){
        return this.msg;
    }

    public static void _throwExcept(String message){
        throw new BusinessException(message);
    }
}
