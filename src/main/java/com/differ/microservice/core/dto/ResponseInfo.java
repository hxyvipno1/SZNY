package com.differ.microservice.core.dto;

import com.differ.microservice.core.CONSTANTS;
import com.differ.microservice.core.util.JSONParser;
import com.differ.microservice.core.util.XmlParser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cailm on 2017-06-27.
 */
public class ResponseInfo {
    public static int    SUCCESS   = 1;
    public static int    EXCEPT    = 0;
    public static int    ERROR     = -2;

    public static String SUMESSAGE = new String("操作成功");
    public static String ERMESSAGE = new String("操作失败");
    public static String EXMESSAGE = new String("异常");

    private int          code;
    private String       message;
    private Object       detail;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getDetail() {
        return detail;
    }

    public void setDetail(Object detail) {
        this.detail = detail;
    }

    public ResponseInfo(int code, String message, Object detail) {
        this.code = code;
        this.message = message;
        this.detail = detail;
    }

    public ResponseInfo(int code, String message) {
        this(code, message, null);
    }

    public ResponseInfo(Object detail) {
        this(SUCCESS, SUMESSAGE, detail);
    }

    public ResponseInfo() {
        this(SUMESSAGE);
    }

    public String toJson() {
        Map<String,Object> rtn = new HashMap<String,Object>();
        rtn.put("code", code);
        rtn.put("message", message);
        if (null != detail && !"".equals(detail)) {
            rtn.put("detail", detail);
        }
        return JSONParser.obj2Json(rtn);
    }

    public String toJson1() {
        Map result = new HashMap();
        Map rtn = new HashMap();
        rtn.put("code", code);
        rtn.put("message", message);
        rtn.put("detail", detail);
        result.put(CONSTANTS.RESULT_NODE, rtn);
        return JSONParser.obj2Json(result);
    }

    public String toXml() {
        Map result = new HashMap();
        result.put("code", code);
        result.put("message", message);
        result.put("detail", detail);
        return XmlParser.map2XML(result, CONSTANTS.RESULT_NODE);
    }

    public String output() {
        return toJson();
    }
}
