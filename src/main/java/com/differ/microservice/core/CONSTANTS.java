package com.differ.microservice.core;

import com.differ.microservice.core.spring.SpringUtil;

/**
 * Created by cailm on 2017-06-23.
 */
public class CONSTANTS {
    public static final String UTF8="UTF-8";
    public static final String SECURITY_MD5="MD5";
    public static final String SECURITY_DES="DES";
    public static final String _DES_PWD="1qa2ws!@#$%";
    public static final String DEFUALT_PWD="123456";

    public static final String DF_JSON="json";
    public static final String DF_XML="xml";
    public static final String RESULT_NODE="response";


    public static final String TY_CHAR="char";
    public static final String TY_INT="int";
    public static final String TY_DATE="date";

    public static final String C_PK="idx";
    public static final String C_CRT_TIME="create_time";
    public static final String C_UPT_TIME="update_time";


}
