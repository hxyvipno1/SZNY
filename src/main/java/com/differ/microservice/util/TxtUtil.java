package com.differ.microservice.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class TxtUtil {
	public static List<String> split(String txt,int[] arrs){
		List<String> list=new ArrayList<String>();
		
		int lindex=0;int current=0;int len=txt.length();
		for(int i=0;i<arrs.length;i++){
			current=arrs[i];
			String sub=StringUtils.substring(txt, lindex,current);
			list.add(sub);
			lindex=current;
		}
		if(current<len){
			list.add(StringUtils.substring(txt, current, len));
		}
		
		return list;
	}
	

	public static boolean isLetter(char c)
	{
		int k = 0x80;
		return c / k == 0 ? true : false;
	}

	

	public static int length(String s)
	{
		if (s == null)
			return 0;
		char[] c = s.toCharArray();
		int len = 0;
		for (int i = 0; i < c.length; i++)
		{
			len++;
			if (!isLetter(c[i]))
			{
				len++;
			}
		}
		return len;
	}
	public static String getChispace(int len){
		String a="　";StringBuffer b=new StringBuffer();
		for(int i=0;i<len;i++){
			b.append(a);
		}
		
		return b.toString();
	}
	public static String substring(String origin, int len)
	{
		if (origin == null || origin.equals("") || len < 1)
			return "";
		byte[] strByte = new byte[len];
		if (len > length(origin))
		{
			return origin;
		}
		System.arraycopy(origin.getBytes(), 0, strByte, 0, len);
		int count = 0;
		for (int i = 0; i < len; i++)
		{
			int value = (int) strByte[i];
			if (value < 0)
			{
				count++;
			}
		}
		if (count % 2 != 0)
		{
			len = (len == 1) ? ++len : --len;
		}
		return new String(strByte, 0, len);
	}
	
	public  static String flag0 		="00000000000000000000000000000000";
	public  static String flagEngSpace	="                                ";				 
	public  static String flagChiSpace 	="　　　　　　　　　　　";
	
	public static String addTxt(String baseString ,int newLen,int addFlag,int seFlg)
	{
		if (baseString == null ) baseString = "";
		baseString = baseString.trim();
		int oldLen = length(baseString);
		String temp;
		if (oldLen >= newLen)
		{
			//baseString = substring(baseString, newLen);
			if(addFlag==1)
				baseString = StringUtils.substring(baseString, 0,newLen/2);
			return baseString;
		} else
		{
			int flagLen = newLen - oldLen;
			
			switch (addFlag)
			{
			case 0:
				temp = flagEngSpace;
				break;
			case 1:
				temp = flagChiSpace;
				break;
			case 2:
				temp = flag0;
				break;
			default:
				temp = flagEngSpace;
				break;
			}
			
			switch (seFlg)
			{
			case 0:
				baseString=substring(temp, flagLen)+baseString;
				break;
			case 1:
				//baseString=baseString + substring(temp, flagLen);
				baseString=baseString + getChispace(flagLen/2);
				break;
			case 2:
				baseString=temp.substring(0, flagLen / 4) + baseString + temp;
				baseString = substring(baseString, newLen);
				break;
			default:
				temp = flagEngSpace;
				baseString=baseString + substring(temp, flagLen);
				break;
			}
		}
		
		return baseString;
	}
	
public static String strTo2n(String str){		
		int len=str.length();
		StringBuilder buf = new StringBuilder(str);
		for( int i=0; i<len; i++ )
			buf.append(" ");
		return buf.toString();
		
	}
}
