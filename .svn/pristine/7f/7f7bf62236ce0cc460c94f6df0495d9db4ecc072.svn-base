/**
 * 
 */
package com.differ.microservice.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;
import com.differ.microservice.core.util.DateTimeUtil;

/**
 * 产品管理
 * 
 * @author zhenshan.xu
 * @version $Id: ProductManagementService.java, v 0.1 2017年10月15日 上午9:23:29
 *          zhenshan.xu Exp $
 */
@Service
public class ProductManagementService {

	@Autowired
	private StepDao dao;

	/**
	 * 查询所有产品
	 * 
	 * @param param
	 * @return
	 */
	public List findAll(Map param) {
		return dao.loadListByCode("LD_PRODUCT_DETAIL", param);
	}
	
	/**
	 * 查询所有产品
	 * 
	 * @param param
	 * @return
	 */
	public Map queryByIdx(Map param) {
		Map Idx =new HashMap<>();
		Map map = dao.loadRowByCode("LD_PRODUCT_IDX", param);
		String id=(String) map.get("idx");
		Idx.put("product", id);
		List list = dao.loadListByCode("LD_PRODUCTS_NEXUS", Idx);
		if (list==null || list.size()==0) {
			map = dao.loadRowByCode("LD_PRODUCT_IDX", param);
		} else {
			StringBuffer suppliersName = new StringBuffer();
			StringBuffer suppliersId = new StringBuffer();
			for (int i = 0; i < list.size(); i++) {
				Map array = (Map) list.get(i);
				String suppliers = (String) array.get("suppliers_idx");
				String[] element = suppliers.split(",|，");
			for (Object supplier_id : element) {
				Map supplierIdx = new HashMap<>();
				supplierIdx.put("idx", supplier_id);
				Map result = dao.loadRowByCode("LD_SUPPLIERS_IDX", supplierIdx);
				suppliersName = suppliersName.append(result.get("suppliers_name").toString()+"、");
				suppliersId = suppliersId.append(result.get("idx").toString()+",");
				}
			}
			String name = suppliersName.toString();
			String Name = name.substring(0,name.length()-1);
			String idx = suppliersId.toString();
			String IDX = idx.substring(0,idx.length()-1);
			map.put("Supplier_name", Name);
			map.put("Supplier_idx", IDX);
		}
		return map;

	}
	
	
	/**
	 * 产品单据号及发生日期 产品单据号规则：截取当前时间加上四位随机数，如（时间：2017-10-15
	 * 10:12,随机数：2587,则单据号为：17101510122587）
	 * 
	 * @param param
	 * @return
	 */
	public Map document1(Map<String, String> param) {

		Random random = new Random();
		int max = 9999;
		int min = 1000;

		int randomNum = random.nextInt(max) % (max - min + 1) + min;

		Date date = new Date();
		String documentNum = DateTimeUtil.getTimerNoFormat(date);
		param.put("document_num", documentNum + randomNum);
		param.put("create_time", DateTimeUtil.getDateFormat(date));
		return param;
	}
	/**
	 * 连续号码段
	 * 产品单据号生成规则：获取最大单据号，截取当前时间转换成字符，组合流水号前一部分，时间字符串，如：1701261009
	 * 					截取字符串最后四位，结果:0001，把String类型的0001转化为int类型的1，结果+10000 + 1把10002首位的1去掉，
	 * 					再拼成17012610090002字符串
	 * @param param
	 * @return
	 */
	public Map document(Map param) {
		String Orderno = null;
		String letter = null;
		Map documentNum = new HashMap<>();
		Map maxOrderNo = new HashMap<>();
		String type = param.get("type").toString();

		if (type.equals("1")) {// 判断为入库
			letter="RK.";
			maxOrderNo = dao.loadRowByCode("LD_IN_NUM", param);
		} else if (type.equals("2")) {// 判断为出库
			letter="CK.";
			maxOrderNo = dao.loadRowByCode("LD_OUT_NUM", param);
		} else if (type.equals("3")) {// 判断为发放
			letter="FF.";
			maxOrderNo = dao.loadRowByCode("LD_ISSUE_NUM", param);
		} else if (type.equals("4")) {// 判断为申请
			letter="SQ.";
			maxOrderNo = dao.loadRowByCode("LD_APPLY_NUM", param);
		} else if (type.equals("5")) {// 判断为投递
			letter="TD.";
			maxOrderNo = dao.loadRowByCode("LD_DELIVERY_NUM", param);
		} else if (type.equals("6")) {// 判断为调拨
			letter="DB.";
			maxOrderNo = dao.loadRowByCode("LD_TRANSFERS_NUM", param);
		} else if (type.equals("7")) {// 判断为退货
			letter="TH.";
			maxOrderNo = dao.loadRowByCode("LD_RETURN_NUM", param);
		}
		String maxOrderno = null;
		if(maxOrderNo != null){
			SimpleDateFormat format = new SimpleDateFormat("yyMMddHHmm"); // 时间字符串产生方式
			String uid_pfix = /* "NO." + */ format.format(new Date()); // 组合流水号前一部分，时间字符串，如：170126
			String pfix = uid_pfix.substring(0, 6);
			maxOrderno=maxOrderNo.get("document_num").toString();
			if (maxOrderno != null && maxOrderno.contains(pfix)) {
				String uid_end = maxOrderno.substring(13, 17); // 截取字符串最后四位，结果:0001
				int endNum = Integer.parseInt(uid_end); // 把String类型的0001转化为int类型的1
				int tmpNum = 10000 + endNum + 1; // 结果10002
				Orderno = uid_pfix + this.subStr("" + tmpNum, 1);// 把10002首位的1去掉，再拼成1701260002字符串
				documentNum.put("document_num", letter+Orderno);
			} else {
				Orderno = uid_pfix + "0001";
				documentNum.put("document_num",letter+Orderno);
			}
		}else{
			SimpleDateFormat format = new SimpleDateFormat("yyMMddHHmm"); // 时间字符串产生方式
			String uid_pfix =  format.format(new Date()); 
			Orderno = uid_pfix + "0001";
			documentNum.put("document_num",letter+Orderno);
		}
		// String batch = maxOrderno.substring(6,11);//截取字符串最后五位，结果：00001
		return documentNum;
	}

	public static String subStr(String str, int start) {
		if (str == null || str.equals("") || str.length() == 0)
			return "";
		if (start < str.length()) {
			return str.substring(start);
		} else {
			return "";
		}

	}

	/**
	 * 通过idx查询节点
	 */
	public List findByIdx(Map param) {

		return dao.loadListByCode("LD_PRODUCT_IDX", param);

	}

	/**
	 * 通过pid查询子节点
	 */
	public List findByPid(Map param) {

		return dao.loadListByCode("LD_PRODUCT_PID", param);

	}

	public List queryByModel(Map param) {

		return dao.loadListByCode("LD_PRODUCT_Model", param);

	}

	public List queryByGoods(Map param) {

		List results = dao.loadListByCode("LD_PRODUCT_NAME", param);

		return results;

	}
	/**
	 * 添加物资词典
	 * @param param
	 * @return
	 */
	public Map addProduct(Map param) {
		Map element = new HashMap<>();
		Map result = dao.add("t_products", param);
		element.put("product_idx", result.get("idx"));
		String suppliers = (String) param.get("Supplier_idx");
		String[] params = suppliers.split(",|，");
		for (String par : params) {
			element.remove("idx");
			element.put("suppliers_idx",par);
			Map results = dao.add("t_product_nexus", element);
		}
		return result;

	}
	/**
	 * 修改物资词典
	 * @param param
	 * @return
	 */
	public Map updateProduct(Map param) {
		Map map =dao.update("t_products", param);
		String idx = (String) param.get("idx"); 
		Map product = new HashMap<>();
		product.put("product", idx);
		Map element = dao.loadRowByCode("LD_PRODUCTS_NEXUS", product);
		if(element==null || element.equals("")){
			String suppliers = (String) param.get("Supplier_idx");
			String[] params = suppliers.split(",|，");
			for (String par : params) {
				Map parameter = new HashMap<>();
				parameter.put("product_idx", idx);
				parameter.put("suppliers_idx", par);
				dao.add("t_product_nexus", parameter);
			}
		}else{
			Map id = new HashMap<>();
			id.put("product", idx);
			dao.excuteByCode("RM_PRODUCTS_NEXUS", id);
			String suppliers = (String) param.get("Supplier_idx");
			String[] params = suppliers.split(",|，");
			for (String par : params) {
				element.remove("idx");
				element.put("suppliers_idx",par);
				Map results = dao.add("t_product_nexus", element);
			}
		}
		
		return map;

	}
	

	/**
	 * 删除
	 */
	public Map delete(Map param) {
		return del(param, "LD_PRODUCT_IDX", "LD_PRODUCT_PID", "t_products");
	}

	public Map del(Map param, String queryByIdx, String queryByPid, String tbl) {
		List beforeDelete = dao.loadListByCode(queryByIdx, param);
		dao.remove(tbl, param);
		List afterDelete = dao.loadListByCode(queryByIdx, param);
		Object pid = param.get("idx");
		Map childParam = new HashMap<>();
		childParam.put("pidx", pid);
		List list = dao.loadListByCode(queryByPid, childParam);
		for (Object child : list) {
			String str = child.toString();
			String[] str1 = str.split(",");
			String[] str2 = str1[0].split("=");
			String childIdx = str2[1];
			Map childNode = new HashMap<>();
			childNode.put("idx", childIdx);
			dao.remove(tbl, childNode);
			del(childNode, queryByIdx, queryByPid, tbl);
		}
		Map map = new HashMap<>();

		if (beforeDelete.size() > 0 && afterDelete.size() == 0) {
			map.put("message", "操作成功");
			map.put("code", 1);
			return map;
		}
		map.put("code", 0);
		map.put("message", "操作失败");
		return map;

	}
}
