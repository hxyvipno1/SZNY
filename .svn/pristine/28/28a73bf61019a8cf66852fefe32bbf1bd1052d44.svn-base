/**
 * xcwlkj.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */
package com.differ.microservice.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.differ.microservice.core.dao.StepDao;
import com.differ.microservice.core.spring.mvc.SingleThreadInstance;
import com.differ.microservice.core.util.Encryption;
import com.differ.microservice.core.util.Validator;

/**
 * 
 * @author dunfang.zhou
 * @version $Id: CustomerService.java, v 0.1 2017年7月25日 下午2:28:23 dunfang.zhou Exp $
 */
@Service
public class CustomerService {
private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private StepDao dao;
    
    private Encryption encryption; 
    
    Validator va = new Validator();
    /**
     * （散户/规模场列表查询）查询全部
     */
    public List findAll(Map param){
    	//判断map中是否有key为name的键值对
    	boolean flag=param.containsKey("name");
    	if(flag){
    		String str = param.get("name").toString();
    		Boolean bo = va.isNumeric(str);
    		if(bo){
    			param.put("phone", str);
    			param.remove("name");
    		}
    	}
    	
        return dao.loadListByCode("LD_CUSTOMER", param);
        
    }
    /**
     * 散户、规模场详情信息
     * @param param
     * @return
     */
    public Map findDetails(Map param){
    	
   	Map<String, List> map = new HashMap<String,List>();
   			
     map.put("details", dao.loadListByCode("LD_CUSTOMER_DETAILS1", param));	//企业工商登记信息、法人信息、基本信息、治理状况
     map.put("principal", dao.loadListByCode("LD_CUSTOMER_DETAILS2", param));	//负责人列表
     map.put("technician", dao.loadListByCode("LD_CUSTOMER_DETAILS3", param));	//技术员列表
     map.put("file", dao.loadListByCode("LD_CUSTOMER_DETAILS4", param));	//业务类型照片
         
     return map;
        
    }
    /**
     * 新增技术员信息
     * @param param
     * @return
     */
    public Map<String, Object> customerTechnicianAdd(Map param){
    	String customer_idx = param.get("customer_idx").toString();
    	Map<String, Object> entty = dao.add("t_people", param);
    	String people_idx  = entty.get("idx").toString();
    	Map<String, Object> tcp = new HashMap<String, Object>();
    	tcp.put("customer_idx", customer_idx);
    	tcp.put("people_idx", people_idx);
    	dao.add("t_customer_people", tcp);
    	return entty;
    }
    /**
     * 删除技术员信息
     * @param param
     * @return
     */
    public Map customerTechnicianDelete(Map param){
    	//判断param中是否存在key为“people_idx”的键值对
    	boolean flag=param.containsKey("people_idx");
    	if(flag){
    		return dao.excuteByCode("RM_TECHNICIAN_DELETE", param);
    	}
    	return null;
    }
    /**
     * 更新散户、规模场详情信息
     * @param param
     * @return
     */ 
    public Map<String,Object> cusomerUpdate(Map param){
    	Map<String,Object> entty = new HashMap<>();//返回的参数map
    	Map customer = (Map) param.get("customer");
    	Map people = (Map) param.get("people");
    	
    	//创建用户map 传入参数
    	Map userParam = new HashMap<>();
    	userParam.put("customer_idx", customer.get("idx"));//customerIdx插入用户表中做关联
    	userParam.put("user_code", people.get("phone"));//法人电话号做登录名
    	userParam.put("type", "3");//类型为农户端用户
    	userParam.put("password", encryption.encrypt("123456"));//密码默认为123456通过MD5加密     
    	//接收people表返回的map
    	Map resultPeople = new HashMap<>();
    	//判断people表idx是否为空 空则新增人员       否则更新人员
    	if(people.get("idx") == null || people.get("idx").equals("")){
    		//判断是散户还是规模场，若是规模场则创建用户
    		if(customer.get("scale_type").equals("1")){ 
    			entty.put("user", dao.add("t_user",userParam));
    		}
    		resultPeople = dao.add("t_people",people);
    		//更新customer表  并将people表idx插入customer表中
    		String peopleidx = resultPeople.get("idx").toString();
    		customer.put("fr_idx", peopleidx);
    		entty.put("customer", dao.update("t_customer",customer));
    	}else{
    		entty.put("customer", dao.update("t_customer",customer));
    		resultPeople = dao.update("t_people",people);
    		if(people.containsKey("user_idx")){ 
    			Map user = new HashMap<>();
    			user.put("idx", people.get("user_idx"));
    			Map map1 =  (Map) dao.loadListByCode("LD_USER_TYPE", user).get(0);
    			if(map1.get("type").equals("3")){
    				userParam.put("idx", people.get("user_idx"));
    				entty.put("user", dao.update("t_user", userParam));
    			}
    		}
    	}
    	entty.put("people", resultPeople);
    	
    	return entty;
    }
    /**
     * 新增
     * @param param
     * @return
     */
    public Map<String,Object> customerAdd(Map param){
    	//获取当前用户的idx,根据用户uidx查询当前用户所述区划并插入到t_customer中
    	String uidx = (String) param.get("user_idx");
    	Map uidxParam = new HashMap<>();
    	uidxParam.put("idx",uidx);
    	Map dis =  (Map) dao.loadListByCode("LD_USER_DISTRICT", uidxParam).get(0);
    	String district_idx = dis.get("district_idx").toString();
    	param.put("district_idx", district_idx);
    	//新增散户、规模场详情信息
    	return dao.add("t_customer", param);
    }
    /**
     * 查询user_code是否为重复
     * @param param
     * @return
     */
    public List usercodeSole(Map param){
    	return  dao.loadListByCode("LD_USER_CODE_SOLE", param);
    }
    /**
     * 删除散户、规模场中附件中间表数据
     * @param param
     * @return
     */
    public Map deleteFiel(Map param){
    	return dao.excuteByCode("RM_CUSTOMER_FILE", param);
    }
    
    /**
     * 查询各地区规模场数量
     * @param param
     * @return
     */
    public List queryCount(Map param){
        List list = new ArrayList<>();
        List areaList = dao.loadListByCode("LD_DISTRICT_COUNT1", param);
        int[] sort = new int[areaList.size()];
        int comNum = 0 ;
        int i = 0;
        int j = 0;
        int k = 0;
        int m = 0;
        DecimalFormat  df = new DecimalFormat("######0.0000");  
        for (Object object : areaList) {
            Map map = (Map)object;
            String a2 = (String) map.get("idx");
            int aa2 = Integer.parseInt(a2);
            Map idxMap = new HashMap<>();
            if(aa2<100){
                idxMap.put("district_idx", "%,"+a2+",%");
            }else{
                idxMap.put("district_idx", "%,"+a2+"%");
            }
            List count = dao.loadListByCode("LD_COUNT", idxMap);
            String[] c = count.toString().split("}");
            String[] c1 = c[0].split("=");
            String c2 = c1[1];
            comNum = comNum + Integer.parseInt(c2);
        }
        
        for (Object object : areaList) {
            Map obMap = (Map)object;
            Map resultMap = new HashMap<>();
            String a2 = (String)obMap.get("idx");
            resultMap.put("idx",a2 );
            resultMap.put("name", obMap.get("name"));
            Map idxMap = new HashMap<>();
            int aa2 = Integer.parseInt(a2);
            if(aa2<100){
                idxMap.put("district_idx", "%,"+a2+",%");
            }else{
                idxMap.put("district_idx", "%,"+a2+"%");
            }
            List count = dao.loadListByCode("LD_COUNT", idxMap);
            String[] c = count.toString().split("}");
            String[] c1 = c[0].split("=");
            String c2 = c1[1];
            resultMap.put("count", c2);
            double per = 0;
            if(comNum!=0){
                per = Double.parseDouble(c2)/(double)comNum;
            }else{
                per = 0; 
            }
            resultMap.put("per", df.format(per));
            List pigCount = dao.loadListByCode("LD_COUNT_PIG", idxMap);
            String[] pc = pigCount.toString().split("}");
            String[] pc1 = pc[0].split("=");
            String pc2 = pc1[1];
            resultMap.put("pigCount", pc2);
            list.add(resultMap);
            sort[i]=Integer.parseInt(pc2);
            i++;
        }
        
        List sortPig = new ArrayList<>();
        List sortCount = new ArrayList<>();
        List sortList = selectSort(sort, list);
       
        for (Object object : sortList) {
            Map map = (Map)object;
            String pig = (String) map.get("pigCount");
            if(pig.equals("0")){
                j++;
            }else{
                k++;
            };
        }
        int[] sort1 = new int[j];
        for (Object object : sortList) {
            Map map = (Map)object;
            String pig = (String) map.get("pigCount");
            if(pig.equals("0")){
                sort1[m] = Integer.parseInt((String)map.get("count")) ;
                m++;
                sortCount.add(object);
            }else{
                sortPig.add(object);
            };
        }
        List sortList1 = selectSort(sort1, sortCount);
        sortPig.addAll(sortList1);
        return sortPig;
    }
    //排序
    public  List selectSort(int[]a,List list)
    {
        int maxIndex=0;
        int temp=0;
        if((a==null)||(a.length==0))
            return null;
        for(int i=0;i<a.length-1;i++)
        {
            maxIndex=i;//无序区的最大数据数组下标
            for(int j=i+1;j<a.length;j++)
            {
                //在无序区中找到最大数据并保存其数组下标
                if(a[j]>a[maxIndex])
                {
                    maxIndex=j;
                }
            }
            if(maxIndex!=i)
            {
                //如果不是无序区的最大值位置不是默认的第一个数据，则交换之。
                temp=a[i];
                a[i]=a[maxIndex];
                a[maxIndex]=temp;
                Map t = (Map) list.get(i);
                list.set(i, list.get(maxIndex));
                list.set(maxIndex, t);
            }
        }
        return list;
    }
    
    
    public List queryCus(){
        return dao.loadListByCode("LD_CUS", null);
    }
    
}
